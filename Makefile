# ============================================================================
# Makefile sphtool v0.2
# 
# This file is part of sphtool:
# Gridding and visualisation package for SPH simulations
# 
# Copyright (C) 2018 Attila Juhasz
# Author: Attila Juhasz
# 
# sphtool is free software; you can redistribute it and/or modify it under
# the terms of the GNU General Public License as published by the Free Software
# Foundation; either version 2 of the License, or (at your option) any later
# version.
# 
# sphtool is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.
# ============================================================================


CXX        	= g++ 
# Location of the python installation
PY_DIR	   	= /opt/anaconda/anaconda
# Python version
PY_VERSION  = 3.6

INCDIR   	= -I/opt/local/include -I/usr/local/include
LIBDIR   	= -L/opt/local/lib -L/usr/local/lib

##############################################################################################################
CXXFLAGS = -std=c++11 -fmessage-length=0 -fopenmp -fPIC
DEBUG	 = -Wall -g
OPTIM    = -O2 -ftree-vectorize -ftree-vectorizer-verbose=5 -msse2 

SRCDIR   = ./src
INCDIR  += -I./include
OBJDIR   = ./build
CDIR     = ./

LIBS     = 

SRC 		 = $(wildcard $(SRCDIR)/*.cc)
OBJECTS  = $(addprefix $(OBJDIR)/,$(notdir $(SRC:.cc=.o)))

WRAP_DIR = ./sphtool
WRAP_MODULES = sphtool
WRAP_SOURCES = $(addprefix $(WRAP_DIR)/, $(addsuffix .cc, $(WRAP_MODULES)))
WRAP_INTERFACE_SOURCES  = $(addprefix $(WRAP_DIR)/, $(addsuffix _wrap.cxx, $(WRAP_MODULES)))
WRAP_INTERFACES  = $(addprefix $(WRAP_DIR)/, $(addsuffix .i, $(WRAP_MODULES)))
WRAP_OBJECTS = $(addprefix $(OBJDIR)/, $(WRAP_SOURCES:%.cc=%.o))
WRAP_OBJECTS += $(addprefix $(OBJDIR)/, $(WRAP_INTERFACE_SOURCES:%.cxx=%.o))
TARGET = $(addprefix $(WRAP_DIR)/, $(addprefix _, $(addsuffix .so, $(WRAP_MODULES))))



# Check the python version
PY_MAJORVERSION := $(shell echo $(PY_VERSION) | cut -f1 -d.)

# Python version dependent include paths and flags
ifeq ($(PY_MAJORVERSION),2)
PY_INCLUDE     = $(PY_DIR)/include/python$(PY_VERSION)
SWIGFLAGS      = -c++ -python
else ifeq ($(PY_MAJORVERSION),3)
PY_INCLUDE     = $(PY_DIR)/include/python$(PY_VERSION)m
SWIGFLAGS      = -c++ -python -py3
endif	

PY_LIB         = $(PY_DIR)/lib
NUMPY_INCLUDE  = $(PY_LIB)/python$(PY_VERSION)/site-packages/numpy/core/include

UNAME_S := $(shell uname -s)
ifeq ($(UNAME_S),Darwin)
SHARED_OPTIONS = -bundle -undefined dynamic_lookup
else ifeq ($(UNAME_S),Linux)
SHARED_OPTIONS = -shared
endif


all :
	mkdir -p $(OBJDIR)
	make $(TARGET)		
	
.PHONY : clean doc

doc : 
	doxygen

$(OBJDIR)/%.o : $(SRCDIR)/%.cc
	$(CXX) $(OPTIM) $(DEBUG) $(CXXFLAGS) $(INCDIR) -c $^ -o $@

$(WRAP_DIR)/%_wrap.cxx : $(WRAP_DIR)/%.i
	swig $(SWIGFLAGS) $^

$(OBJDIR)/%_wrap.o : $(WRAP_DIR)/%_wrap.cxx
	$(CXX) -O2 -fPIC -c -std=c++11 -I$(PY_INCLUDE) -I$(NUMPY_INCLUDE) $(INCDIR) $^ -o $@

$(OBJDIR)/%.o : $(WRAP_DIR)/%.cc
	$(CXX) -O2 -fPIC -c -std=c++11 -I$(PY_INCLUDE) -I$(NUMPY_INCLUDE) $(INCDIR) $^ -o $@

$(WRAP_DIR)/_%.so : $(OBJECTS) $(OBJDIR)/%.o $(OBJDIR)/%_wrap.o 
	$(CXX) $(SHARED_OPTIONS) -fopenmp $(INCDIR) -L$(PY_LIB) $(LIBDIR) $(LIBS) $^ -o $@ 


clean:
	echo $(WRAP_OBJECTS)
	rm -f $(TARGET)
	rm -rf $(OBJDIR)
	rm -f $(WRAP_OBJECTS)
	rm -f $(WRAP_INTERFACE_SOURCES)
	rm -f $(addprefix $(WRAP_DIR)/, $(addsuffix .py, $(WRAP_MODULES)))

	

