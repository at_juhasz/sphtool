// ============================================================================
// kernels.cc
//
// This file is part of sphtool:
// Gridding and visualisation package for SPH simulations
//
// Copyright (C) 2018 Attila Juhasz
// Author: Attila Juhasz
//
// sphtool is free software; you can redistribute it and/or modify it under
// the terms of the GNU General Public License as published by the Free Software
// Foundation; either version 2 of the License, or (at your option) any later
// version.
//
// sphtool is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.
// ============================================================================

#include <cmath>
#include <iostream>
#include "../include/kernels.h"
// ------------------------------------------------------------------------
//
// ------------------------------------------------------------------------
KernelBase::KernelBase() {
  n_dim_ = 0;
  norm_ = 1.0;
  n_ki_table_double_ = 0.0;
  n_integral_ = 1000;
  n_ki_table_ = 1000;
}
// ------------------------------------------------------------------------
//
// ------------------------------------------------------------------------
KernelBase::~KernelBase() {}
// ------------------------------------------------------------------------
//
// ------------------------------------------------------------------------
void KernelBase::SetNDim(const int& n_dim) {
  n_dim_ = n_dim;
  InitWIntegralTable(n_integral_, n_ki_table_);
}
// ------------------------------------------------------------------------
//
// ------------------------------------------------------------------------
int KernelBase::GetNDim() {
  return n_dim_;
}
// ------------------------------------------------------------------------
//
// ------------------------------------------------------------------------
double KernelBase::W(const double& q) {return 0.0;}

// ------------------------------------------------------------------------
//
// ------------------------------------------------------------------------
void KernelBase::InitWIntegralTable(const int& n_integral,
                                            const int& n_ki_table) {}
// ------------------------------------------------------------------------
//
// ------------------------------------------------------------------------
CubicSplineKernel::CubicSplineKernel(const int& n_dim) {
  n_dim_ = n_dim;
  norm_ = 1.0;
  if (n_dim_ == 1) {
    norm_ = 1.5;
  } else if (n_dim_ == 2) {
    norm_ = 0.7 * M_PI;
  } else if (n_dim_ == 3) {
    norm_ = M_PI;
  }

  InitWIntegralTable(n_integral_, n_ki_table_);
}
// ------------------------------------------------------------------------
//
// ------------------------------------------------------------------------
CubicSplineKernel::~CubicSplineKernel() {}

// ------------------------------------------------------------------------
//
// ------------------------------------------------------------------------
double CubicSplineKernel::W(const double& q) {
  if (q >= 2.0) {
    return 0.0;
  } else if (q >= 1.0) {
    double dummy = 2.0 - q;
    return 0.25 * dummy * dummy * dummy / norm_;
  } else {
    return (1.0 - 1.5 * q * q + 0.75 * q * q * q) / norm_;
  }
}
// ------------------------------------------------------------------------
//
// ------------------------------------------------------------------------
void CubicSplineKernel::InitWIntegralTable(const int& n_integral,
                                               const int& n_ki_table) {
  std::vector<double> d;
  std::vector<double> kernel;
  std::vector<double> y;
  double ymax;

  n_integral_ = n_integral;
  n_ki_table_ = n_ki_table;
  n_ki_table_double_ = static_cast<double>(n_ki_table_);

  ki_table_x_.resize(n_ki_table, 0.0);
  ki_table_fx_.resize(n_ki_table, 0.0);
  d.resize(n_integral);
  kernel.resize(n_integral);
  y.resize(n_integral);

  double norm1 = n_ki_table_double_ - 1.0;
  double norm2 = static_cast<double>(n_integral - 1);

  for (int i = 0; i < n_ki_table; i++) {
    ki_table_x_[i] = 2.0 * static_cast<double>(i) / norm1;
    ki_table_fx_[i] = 0.0;

    ymax = sqrt(4.0 - ki_table_x_[i] * ki_table_x_[i]);
    for (int j = 0; j < n_integral; j++) {
      y[j] = ymax * static_cast<double>(j) / norm2;
      d[j] = sqrt(ki_table_x_[i] * ki_table_x_[i] + y[j] * y[j]);
      kernel[j] = W(d[j]);
    }

    ki_table_fx_[i] = 0.0;
    for (int j = 0; j < n_integral - 1; j++)
      ki_table_fx_[i] +=
          0.5 * (y[j + 1] - y[j]) * (kernel[j] + kernel[j + 1]);

    // The integral is done for the interval [0, ymax], however, we'd need to
    // do it for [-ymax, ymax] Since the kernel is symmetric we just need to
    // multiply it by 2
    ki_table_fx_[i] *= 2.0;
  }
}


