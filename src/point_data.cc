// ============================================================================
// point_data.cc
//
// This file is part of sphtool:
// Gridding and visualisation package for SPH simulations
//
// Copyright (C) 2018 Attila Juhasz
// Author: Attila Juhasz
//
// sphtool is free software; you can redistribute it and/or modify it under
// the terms of the GNU General Public License as published by the Free Software
// Foundation; either version 2 of the License, or (at your option) any later
// version.
//
// sphtool is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.
// ============================================================================

#include <cmath>
#include "../include/point_data.h"
#include "../include/tree.h"
#include "../include/kernels.h"
// ------------------------------------------------------------------------
//
// ------------------------------------------------------------------------
PointData::PointData() {
  pid = -1;
  crd.resize(3);
  for (int i = 0; i < 3; ++i) crd[i] = 0.0;
}
// ------------------------------------------------------------------------
//
// ------------------------------------------------------------------------
PointData::~PointData() {}
// ------------------------------------------------------------------------
//
// ------------------------------------------------------------------------
PointDataSet::PointDataSet() {
  this->n_scalar_ = 0;
  this->n_vector_ = 0;
  this->n_point_ = 0;
  this->n_dim_ = 1;
  ptr_points_.resize(0);
  this->crd_system_ = "cartesian";
}
// ------------------------------------------------------------------------
//
// ------------------------------------------------------------------------
PointDataSet::PointDataSet(const int& n_point, const int& n_dim) {
  if (n_dim < 1) {
    throw std::out_of_range(
        "Number of dimensions in PointDataSet constructor is negative. The "
        "number of dimensions should be between 1 and 3.");
  } else if (n_dim > 3) {
    throw std::out_of_range(
        "Number of dimensions in PointDataSet constructor larger than 3. The "
        "number of dimensions should be between 1 and 3.");
  }
  if (n_point < 0) {
    throw std::out_of_range(
        "Number of points in PointDataSet constructor is negative. The number "
        "of points in a PointDataSet cannot be less than 0.");
  }

  this->n_scalar_ = 0;
  this->n_vector_ = 0;
  this->n_point_ = n_point;
  this->n_dim_ = n_dim;
  ptr_points_.resize(n_point);

  for (int i = 0; i < n_point; ++i) {
    ptr_points_[i] = new PointData;
    for (int idim = 0; idim < n_dim; ++idim) {
      ptr_points_[i]->crd[idim] = 0.0;
    }
    ptr_points_[i]->scalars.resize(this->n_scalar_);
    ptr_points_[i]->vectors.resize(this->n_vector_);
    ptr_points_[i]->pid = i;
  }
}
// ------------------------------------------------------------------------
//
// ------------------------------------------------------------------------
PointDataSet::~PointDataSet() {
  DeleteAllPoints();
}
// ------------------------------------------------------------------------
//
// ------------------------------------------------------------------------
PointDataSet::PointDataSet(const PointDataSet& obj) {
  this->n_dim_ = obj.n_dim_;
  this->n_point_ = obj.n_point_;
  this->n_scalar_ = obj.n_scalar_;
  this->n_vector_ = obj.n_vector_;
  this->scalar_index_ = obj.scalar_index_;
  this->vector_index_ = obj.vector_index_;
  this->ptr_points_.resize(obj.ptr_points_.size());
  for (size_t ip = 0; ip < this->ptr_points_.size(); ++ip) {
    this->ptr_points_[ip] = new PointData;
    for (int idim = 0; idim < this->n_dim_; ++idim) {
      ptr_points_[ip]->crd[idim] = obj.ptr_points_[ip]->crd[idim];
    }
    this->ptr_points_[ip]->scalars = obj.ptr_points_[ip]->scalars;
    this->ptr_points_[ip]->vectors = obj.ptr_points_[ip]->vectors;
  }
}
// ------------------------------------------------------------------------
//
// ------------------------------------------------------------------------
PointDataSet& PointDataSet::operator=(const PointDataSet& obj) {
  this->n_dim_ = obj.n_dim_;
  this->n_point_ = obj.n_point_;
  this->n_scalar_ = obj.n_scalar_;
  this->n_vector_ = obj.n_vector_;
  this->scalar_index_ = obj.scalar_index_;
  this->vector_index_ = obj.vector_index_;
  this->ptr_points_.resize(obj.ptr_points_.size());
  for (size_t ip = 0; ip < this->ptr_points_.size(); ++ip) {
    this->ptr_points_[ip] = new PointData;
    for (int idim = 0; idim < this->n_dim_; ++idim) {
      ptr_points_[ip]->crd[idim] = obj.ptr_points_[ip]->crd[idim];
    }
    this->ptr_points_[ip]->scalars = obj.ptr_points_[ip]->scalars;
    this->ptr_points_[ip]->vectors = obj.ptr_points_[ip]->vectors;
  }
  return *this;
}

void PointDataSet::DeleteAllPoints() {
  for (int i = 0; i < n_point_; ++i) {
    delete ptr_points_[i];
  }
  ptr_points_.resize(0);
}
// ------------------------------------------------------------------------
//
// ------------------------------------------------------------------------
void PointDataSet::AddPointData(const PointData& point_data) {
  if (static_cast<int>(point_data.scalars.size()) != n_scalar_) {
    throw std::length_error(
        "Point data has a different number of scalars than the rest of the "
        "points in the point set, thus it cannot be added to the point set.");
  }

  if (static_cast<int>(point_data.scalars.size()) != n_vector_) {
    throw std::length_error(
        "Point data has a different number of scalars than the rest of the "
        "points in the point set, thus it cannot be added to the point set.");
  }
  ptr_points_.resize(n_point_ + 1);
  ptr_points_[n_point_] = new PointData;
  ptr_points_[n_point_]->pid = n_point_;

  for (int i = 0; i < 3; ++i) ptr_points_[n_point_]->crd[i] = point_data.crd[i];
  ptr_points_[n_point_]->scalars.resize(point_data.scalars.size());
  for (size_t i = 0; i < point_data.scalars.size(); ++i)
    ptr_points_[n_point_]->scalars[i] = point_data.scalars[i];
  for (size_t i = 0; i < point_data.vectors.size(); ++i)
    ptr_points_[n_point_]->vectors.resize(point_data.vectors.size());

  n_point_++;
}
// ------------------------------------------------------------------------
//
// ------------------------------------------------------------------------
int PointDataSet::get_ndim() {
  return n_dim_;
}
// ------------------------------------------------------------------------
//
// ------------------------------------------------------------------------
void PointDataSet::SetNDim(const int& n_dim) {
  this->n_dim_ = n_dim;
}
// ------------------------------------------------------------------------
//
// ------------------------------------------------------------------------
int PointDataSet::get_npoint() {
  return n_point_;
}
// ------------------------------------------------------------------------
//
// ------------------------------------------------------------------------
void PointDataSet::SetNPoint(const int& n_point) {
  ptr_points_.resize(n_point);

  if (this->n_point_ < n_point) {
    for (int i = this->n_point_; i < n_point; ++i) {
      ptr_points_[i] = new PointData;
      for (int idim = 0; idim < n_dim_; ++idim) {
        ptr_points_[i]->crd[idim] = 0.0;
      }
      ptr_points_[i]->scalars.resize(this->n_scalar_);
      ptr_points_[i]->vectors.resize(this->n_vector_);
      ptr_points_[i]->pid = i;
    }
  } else {
    for (int i = 0; i < n_point; ++i) {
      ptr_points_[i]->scalars.resize(this->n_scalar_);
      ptr_points_[i]->vectors.resize(this->n_vector_);
    }
  }
  this->n_point_ = n_point;
}
// ------------------------------------------------------------------------
//
// ------------------------------------------------------------------------
int PointDataSet::get_nscalar() {
  return n_scalar_;
}
// ------------------------------------------------------------------------
//
// ------------------------------------------------------------------------
void PointDataSet::_set_nscalar(const int& n_scalar,
                              const std::vector<std::string>& var_name) {
  this->n_scalar_ = n_scalar;
  for (int ip = 0; ip < n_point_; ++ip) {
    ptr_points_[ip]->scalars.resize(this->n_scalar_, 0.0);
    for (int ivar = 0; ivar < n_scalar; ++ivar) {
      ptr_points_[ip]->scalars[ivar] = 0.0;
    }
  }
  for (int ivar = 0; ivar < n_scalar; ++ivar) {
    scalar_index_[var_name[ivar]] = ivar;
  }
}
// ------------------------------------------------------------------------
//
// ------------------------------------------------------------------------
int PointDataSet::get_nvector() {
  return n_vector_;
}
// ------------------------------------------------------------------------
//
// ------------------------------------------------------------------------
void PointDataSet::_set_nvector(const int& n_vector,
                              const std::vector<std::string>& var_name) {
  this->n_vector_ = n_vector;
  int vector_size = this->n_vector_ * this->n_dim_;
  for (int ip = 0; ip < n_point_; ++ip) {
    ptr_points_[ip]->vectors.resize(vector_size, 0.0);
    for (int ivar = 0; ivar < vector_size; ++ivar) {
      ptr_points_[ip]->vectors[ivar] = 0.0;
    }
  }
  for (int ivar = 0; ivar < n_vector; ++ivar) {
    vector_index_[var_name[ivar]] = ivar;
  }
}
// ------------------------------------------------------------------------
//
// ------------------------------------------------------------------------
void PointDataSet::set_size(const int& n_point, const int& n_dim) {
  if (n_dim < 1) {
    throw std::out_of_range(
        "PointDataSet is attempted to be resized to a negative "
        "spatial dimension. The number of spatial dimensions should be between "
        "1 and 3.");
  } else if (n_dim > 3) {
    throw std::out_of_range(
        "PointDataSet is attempted to be resized to a spatial "
        "dimension larger than 3. The number of spatial dimensions should be "
        "between 1 and 3.");
  }
  if (n_point < 0) {
    throw std::out_of_range(
        "PointDataSet is attempted to be resized to a negative number of "
        "contained points. The number of points in a PointDataSet cannot be "
        "less than 0.");
  }
  this->n_dim_ = n_dim;
  if (this->n_point_ < n_point) {
    ptr_points_.resize(n_point);
    for (int i = this->n_point_; i < n_point; ++i) {
      this->ptr_points_[i] = new PointData;
      for (int idim = 0; idim < n_dim; ++idim) {
        this->ptr_points_[i]->crd[idim] = 0.0;
      }
      this->ptr_points_[i]->scalars.resize(this->n_scalar_, 0.0);
      this->ptr_points_[i]->vectors.resize(this->n_vector_ * n_dim_, 0.0);
      this->ptr_points_[i]->pid = i;
    }
  } else {
    if (this->n_point_ > n_point) {
      for (int i = n_point; i < this->n_point_; ++i) {
        delete this->ptr_points_[i];
      }
      ptr_points_.resize(n_point);
    }
    for (int i = 0; i < n_point; ++i) {
      this->ptr_points_[i]->scalars.resize(this->n_scalar_);
      this->ptr_points_[i]->vectors.resize(this->n_vector_ * n_dim_);
    }
  }
  this->n_point_ = n_point;
}
// ------------------------------------------------------------------------
//
// ------------------------------------------------------------------------
std::vector<std::string> PointDataSet::get_all_scalar_names() {
  std::vector<std::string> vector_string_out(n_scalar_);
  for (int i = 0; i < n_scalar_; ++i) {
    vector_string_out[i] = GetScalarName(i);
  }
  return vector_string_out;
}
// ------------------------------------------------------------------------
//
// ------------------------------------------------------------------------
std::vector<std::string> PointDataSet::get_all_vector_names() {
  std::vector<std::string> vector_string_out(n_vector_);
  for (int i = 0; i < n_vector_; ++i) {
    vector_string_out[i] = GetVectorName(i);
  }
  return vector_string_out;
}
// ------------------------------------------------------------------------
//
// ------------------------------------------------------------------------
int PointDataSet::GetScalarIndex(const std::string& scalar_name) {
  int ind = -1;
  for (auto i : scalar_index_) {
    if (i.first == scalar_name)
      ind = i.second;
  }
  return ind;
}
// ------------------------------------------------------------------------
//
// ------------------------------------------------------------------------
int PointDataSet::GetVectorIndex(const std::string& vector_name) {
  int ind = -1;
  for (auto i : vector_index_) {
    if (i.first == vector_name)
      ind = i.second;
  }
  return ind;
}
// ------------------------------------------------------------------------
//
// ------------------------------------------------------------------------
std::string PointDataSet::GetScalarName(const int& scalar_index) {
  std::string scalar_name = "";
  for (auto i : this->scalar_index_) {
    if (i.second == static_cast<int>(scalar_index))
      scalar_name = i.first;
  }
  return scalar_name;
}
// ------------------------------------------------------------------------
//
// ------------------------------------------------------------------------
std::string PointDataSet::GetVectorName(const int& vector_index) {
  std::string vector_name = "";
  for (auto i : this->vector_index_) {
    if (i.second == static_cast<int>(vector_index))
      vector_name = i.first;
  }
  return vector_name;
}
// ------------------------------------------------------------------------
//
// ------------------------------------------------------------------------
void PointDataSet::_set_scalar(const std::string& var_name,
                             double* vector_double_in,
                             int dim) {
  if (dim > n_point_) {
    if (dim != n_point_) {
      throw std::length_error(
          "Incompatible dimensions. Trying to set a vector containing " +
          std::to_string(dim) + " points in a container whose capacity is " +
          std::to_string(n_point_) + ". Resize the container first.");
    }
  }

  //
  // Check if a vector already exists with the same name. If so erase the
  // vector as the variable name is a unique identifier
  //
  if (GetVectorIndex(var_name) >= 0) {
    std::cout << "A vector variable with the same name is found. Replacing "
                 "vector with scalar..."
              << std::endl;
    delete_vector(var_name);
  }

  int index = GetScalarIndex(var_name);
  if (index < 0) {
    index = this->n_scalar_;
    this->n_scalar_++;
    for (int ip = 0; ip < n_point_; ++ip) {
      ptr_points_[ip]->scalars.resize(this->n_scalar_);
    }
    scalar_index_[var_name] = index;
  }
  for (int ip = 0; ip < n_point_; ++ip) {
    ptr_points_[ip]->scalars[index] = vector_double_in[ptr_points_[ip]->pid];
  }
}
// ------------------------------------------------------------------------
//
// ------------------------------------------------------------------------
void PointDataSet::_get_scalar(const std::string& var_name,
                             double* vector_double_out,
                             int dim_out) {
  int index = GetScalarIndex(var_name);
  if (index >= 0) {
    if (dim_out != n_point_) {
      throw std::length_error(
          "Wrong size of output array in function GetScalar : " +
          std::to_string(dim_out) + " vs " + std::to_string(n_point_));
    }
    for (int ip = 0; ip < n_point_; ++ip) {
      vector_double_out[ptr_points_[ip]->pid] = ptr_points_[ip]->scalars[index];
    }
  } else {
    throw std::runtime_error("Unknown variable '" + var_name + "'");
  }
}
// ------------------------------------------------------------------------
//
// ------------------------------------------------------------------------
void PointDataSet::_set_vector(const std::string& var_name,
                               double* vector_double_in, int dim, int dim2) {
  int dim1 = dim / dim2;

  if (dim1 != n_point_) {
    throw std::length_error(
        "Incompatible dimensions. Trying to set a vector containing " +
        std::to_string(dim1) + " points in a container whose capacity is " +
        std::to_string(n_point_) + ". Resize the container first.");
  }
  if (dim2 != n_dim_) {
    throw std::length_error(
        "Incompatible dimensions. Trying to set a vector with " +
        std::to_string(dim2) + " dimension in an " + std::to_string(n_dim_) +
        " container. Resize the container first.");
  }

  if (var_name == "crd") {
    for (int ip = 0; ip < n_point_; ++ip) {
      for (int idim = 0; idim < n_dim_; ++idim) {
        ptr_points_[ip]->crd[idim] =
            vector_double_in[ptr_points_[ip]->pid * n_dim_ + idim];
      }
    }
  } else {
    //
    // Check if a scalar already exists with the same name. If so erase the
    // scalar as the variable name is a unique identifier
    //
    if (GetScalarIndex(var_name) >= 0) {
      std::cout << "A scalar variable with the same name is found. Replacing "
                   "scalar with vector..."
                << std::endl;
      delete_scalar(var_name);
    }

    int index = GetVectorIndex(var_name);
    if (index < 0) {
      index = this->n_vector_;
      this->n_vector_++;
      for (int ip = 0; ip < n_point_; ++ip) {
        ptr_points_[ip]->vectors.resize(this->n_vector_*this->n_dim_);
      }
      vector_index_[var_name] = index;
    }
    for (int ip = 0; ip < n_point_; ++ip) {
      for (int idim = 0; idim < n_dim_; ++idim) {
        ptr_points_[ip]->vectors[index * n_dim_ + idim] =
            vector_double_in[ptr_points_[ip]->pid * n_dim_ + idim];
      }
    }
  }
}
// ------------------------------------------------------------------------
//
// ------------------------------------------------------------------------
void PointDataSet::_get_vector(const std::string& var_name,
                             double* vector_double_out, int dim, int dim2) {
  int dim1 = dim / dim2;
  if (dim1 != n_point_) {
    throw std::length_error(
        "Wrong size (dimension 1) of output array in function GetVector " +
        std::to_string(dim1) + " vs " + std::to_string(n_point_));
  }
  if (dim2 != n_dim_) {
    throw std::length_error(
        "Wrong size (dimension 2) of output array in function GetVector" +
        std::to_string(dim1) + " vs " + std::to_string(n_point_));
  }

  if (var_name == "crd") {
    for (int ip = 0; ip < n_point_; ++ip) {
      for (int idim = 0; idim < n_dim_; ++idim) {
        vector_double_out[ptr_points_[ip]->pid * n_dim_ + idim] =
            ptr_points_[ip]->crd[idim];
      }
    }
  } else {
    int index = GetVectorIndex(var_name);
    if (index >= 0) {
      for (int ip = 0; ip < n_point_; ++ip) {
        for (int idim = 0; idim < n_dim_; ++idim) {
          vector_double_out[ptr_points_[ip]->pid * n_dim_ + idim] =
              ptr_points_[ip]->vectors[index * n_dim_ + idim];
        }
      }
    } else {
      throw std::runtime_error("Unknown variable '" + var_name + "'");
    }
  }
}

bool PointDataSet::is_var_scalar(const std::string& var_name) {
  int ind_var = GetScalarIndex(var_name);
  bool is_var_scalar = true;
  if (ind_var < 0) {
    ind_var = GetVectorIndex(var_name);
    if (ind_var < 0) {
      throw std::runtime_error(
          "Neither scalar nor vector variable has been found with name " +
          var_name);
    }
    is_var_scalar = false;
  }
  return is_var_scalar;
}
// ------------------------------------------------------------------------
//
// ------------------------------------------------------------------------
void PointDataSet::delete_scalar(const std::string& var_name) {
  int index = GetScalarIndex(var_name);

  if (index >= 0) {
    for (int ip = 0; ip < n_point_; ++ip) {
      ptr_points_[ip]->scalars.erase(ptr_points_[ip]->scalars.begin() + index);
    }
    scalar_index_.erase(var_name);
    for (int i = index; i < n_scalar_ - 1; ++i) {
      std::string name = GetScalarName(i + 1);
      scalar_index_[name] -= 1;
    }
    n_scalar_ = n_scalar_ - 1;
  } else {
    throw std::runtime_error("Unknown variable '" + var_name + "'");
  }
}
// ------------------------------------------------------------------------
//
// ------------------------------------------------------------------------
void PointDataSet::delete_vector(const std::string& var_name) {
  int index = GetVectorIndex(var_name);

  if (index >= 0) {
    for (int ip = 0; ip < n_point_; ++ip) {
      ptr_points_[ip]->vectors.erase(ptr_points_[ip]->vectors.begin() + index);
    }

    for (int i = index; i < n_vector_ - 1; ++i) {
      std::string name = GetScalarName(i + 1);
      vector_index_[name] -= 1;
    }
    vector_index_.erase(var_name);
    n_vector_ = n_vector_ - 1;
  } else {
    throw std::runtime_error("Unknown variable '" + var_name + "'");
  }
}
// ------------------------------------------------------------------------
//
// ------------------------------------------------------------------------
void PointDataSet::GetCoordinateBoundaries(double* crd_min, double* crd_max) {
  for (int idim = 0; idim < n_dim_; ++idim) {
    crd_min[idim] = 1e99;
    crd_max[idim] = -1e99;
  }

  for (int ip = 0; ip < n_point_; ++ip) {
    for (int idim = 0; idim < n_dim_; ++idim) {
      if (ptr_points_[ip]->crd[idim] < crd_min[idim])
        crd_min[idim] = ptr_points_[ip]->crd[idim];
      if (ptr_points_[ip]->crd[idim] > crd_max[idim])
        crd_max[idim] = ptr_points_[ip]->crd[idim];
    }
  }
}
// ------------------------------------------------------------------------
//
// ------------------------------------------------------------------------
void PointDataSet::_get_coordinate_axis_boundaries(const int& iaxis,
                                                   double* vector_double_out,
                                                   int dim_out) {
  if ((iaxis < 0) | (iaxis > 2)) {
    throw std::runtime_error("Invalid axis index " + std::to_string(iaxis) +
                             ". Axis index must be between 0 and 2");
  }
  vector_double_out[0] = 1e99;
  vector_double_out[1] = -1e99;

  for (int ip = 0; ip < n_point_; ++ip) {
    if (ptr_points_[ip]->crd[iaxis] < vector_double_out[0])
      vector_double_out[0] = ptr_points_[ip]->crd[iaxis];
    if (ptr_points_[ip]->crd[iaxis] > vector_double_out[1])
      vector_double_out[1] = ptr_points_[ip]->crd[iaxis];
  }
}
// ------------------------------------------------------------------------
//
// ------------------------------------------------------------------------
void PointDataSet::validate() {
  for (auto i : scalar_index_) {
    if (i.second < 0) {
      throw std::out_of_range("Internal indexing error. Scalar variable " +
                              i.first + " has a negative scalar index of " +
                              std::to_string(i.second));
    }
  }

  for (auto i : vector_index_) {
    if (i.second < 0) {
      throw std::out_of_range("Internal indexing error. Vector variable " +
                              i.first + " has a negative vector index of " +
                              std::to_string(i.second));
    }
  }

  if (n_point_ != static_cast<int>(ptr_points_.size() )) {
    throw std::length_error("Internal size error in point data container.");
  }

  size_t n_scalar_size_t = static_cast<size_t>(n_scalar_);
  size_t n_vector_size_t = static_cast<size_t>(n_vector_ * n_dim_);
  for (int ip = 0; ip < n_point_; ++ip) {
    if (ptr_points_[ip]->scalars.size() != n_scalar_size_t) {
      throw std::length_error("Internal size error in scalar variables.");
    }
    if (ptr_points_[ip]->vectors.size() != n_vector_size_t) {
      throw std::length_error("Internal size error in vector variables.");
    }
  }
}
// ------------------------------------------------------------------------
//
// ------------------------------------------------------------------------
void PointDataSet::GetOriginalIndexOrder(int* vector_int_out,
                                         const int& dim_out) {
  if (dim_out != n_point_) {
    throw std::length_error(
        "Wrong size of output array in function GetScalar : " +
        std::to_string(dim_out) + " vs " + std::to_string(n_point_));
  }

  for (int ip = 0; ip < n_point_; ++ip) {
    vector_int_out[ptr_points_[ip]->pid] = ip;
  }
}
// ------------------------------------------------------------------------
//
// ------------------------------------------------------------------------
void PointDataSet::_transform_to_crd_system(const std::string& crd_system) {
  if (this->crd_system_ == "cartesian") {
    if (crd_system == "spherical") {
      for (int ip=0; ip < n_point_; ++ip) {
        std::vector<double> crd_new(3, 0.0);

        double r_cyl = (ptr_points_[ip]->crd[0] * ptr_points_[ip]->crd[0]) +
                       (ptr_points_[ip]->crd[1] * ptr_points_[ip]->crd[1]);
        crd_new[0] =
            sqrt(r_cyl + (ptr_points_[ip]->crd[2] * ptr_points_[ip]->crd[2]));
        if (crd_new[0] == 0.) {
          crd_new[0] = 0.0;
          crd_new[1] = 0.0;
          crd_new[2] = 0.0;
        } else {
          r_cyl = sqrt(r_cyl);
          crd_new[1] = acos(ptr_points_[ip]->crd[2] / crd_new[0]);
          crd_new[2] = acos(ptr_points_[ip]->crd[0] / (r_cyl + 1e-15));
          if (ptr_points_[ip]->crd[1] < 0.0) {
            crd_new[2] = 2.0 * M_PI - crd_new[2];
          }
        }
        ptr_points_[ip]->crd = std::move(crd_new);
      }
    } else if (crd_system != "cartesian") {
      throw std::runtime_error(
          "Point data set is in cartesian coordinates. Transformation to " +
          crd_system +
          " system is not implemented. Only cartesian and spherical coordinate "
          "systems are implemented.");
    }
  } else if (this->crd_system_ == "spherical") {
    if (crd_system == "cartesian") {
      const double pihalf = 0.5 * M_PI;
      const double threepihalf = 1.5 * M_PI;
      const double twopi = 2.0 * M_PI;


      for (int ip=0; ip < n_point_; ++ip) {
        std::vector<double> crd_new(3, 0.0);
        double sin_theta = sin(ptr_points_[ip]->crd[1]);
        double cos_theta = cos(ptr_points_[ip]->crd[1]);

        if (ptr_points_[ip]->crd[1] < 1e-15) {
          sin_theta = 0.0;
          cos_theta = 1.0;
        } else if (std::abs(ptr_points_[ip]->crd[1] - pihalf) < 1e-15) {
          sin_theta = 1.0;
          cos_theta = 0.0;
        } else if (std::abs(ptr_points_[ip]->crd[1] - M_PI) < 1e-15) {
          sin_theta = 0.0;
          cos_theta = -1.0;
        }

        double sin_phi = sin(ptr_points_[ip]->crd[2]);
        double cos_phi = cos(ptr_points_[ip]->crd[2]);

        if (ptr_points_[ip]->crd[2] < 1e-15) {
          sin_phi = 0.0;
          cos_phi = 1.0;
        } else if (std::abs(ptr_points_[ip]->crd[2] - pihalf) < 1e-15) {
          sin_phi = 1.0;
          cos_phi = 0.0;
        } else if (std::abs(ptr_points_[ip]->crd[2] - M_PI) < 1e-15) {
          sin_phi = 0.0;
          cos_phi = -1.0;
        } else if (std::abs(ptr_points_[ip]->crd[2] - threepihalf) < 1e-15) {
          sin_phi = -1.0;
          cos_phi = 0.0;
        } else if (std::abs(ptr_points_[ip]->crd[2] - twopi) < 1e-15) {
          sin_phi = 0.0;
          cos_phi = 1.0;
        }

        crd_new[0] = ptr_points_[ip]->crd[0] * sin_theta * cos_phi;
        crd_new[1] = ptr_points_[ip]->crd[0] * sin_theta * sin_phi;
        crd_new[2] = ptr_points_[ip]->crd[0] * cos_theta;

        ptr_points_[ip]->crd = std::move(crd_new);
      }
    } else if (crd_system != "spherical") {
      throw std::runtime_error(
          "Point data set is in " + this->crd_system_ +
          " coordinates. Transformation to " + crd_system +
          " system is not implemented. Only cartesian and spherical coordinate "
          "systems can be used.");
    }
  } else {
    throw std::runtime_error(
        "Point data set is defined on an unknown coordinate system " +
        this->crd_system_ +
        ". Only cartesian and spherical coordinate systems are implemented.");
  }
}
// ------------------------------------------------------------------------
//
// ------------------------------------------------------------------------
std::string PointDataSet::_get_crd_system() { return crd_system_; }
// ------------------------------------------------------------------------
//
// ------------------------------------------------------------------------
void PointDataSet::_set_crd_system(const std::string& crd_system) {
  crd_system_ = crd_system;
}
