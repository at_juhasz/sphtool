// ============================================================================
// isphtool.cc
//
// This file is part of sphtool:
// Gridding and visualisation package for SPH simulations
//
// Copyright (C) 2018 Attila Juhasz
// Author: Attila Juhasz
//
// sphtool is free software; you can redistribute it and/or modify it under
// the terms of the GNU General Public License as published by the Free Software
// Foundation; either version 2 of the License, or (at your option) any later
// version.
//
// sphtool is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.
// ============================================================================

#include "../include/sphtool.h"

#include <cmath>
#include <random>

// ------------------------------------------------------------------------
//
// ------------------------------------------------------------------------
SPHTool::SPHTool() {
  param_int_["n_thread"] = 1;
  param_int_["max_particle_per_cell"] = 50;
  param_int_["max_tree_depth"] = 15;
  param_int_["max_particle_per_amr_grid_cell"] = 50;
  param_int_["max_amr_grid_depth"] = 5;
  param_int_["ki_table_n_point"] = 1000;
  param_int_["ki_table_n_val"] = 1000;
  param_int_["output_ascii_precision"] = 9;

  grid_type_ = "regular";
}

// ------------------------------------------------------------------------
//
// ------------------------------------------------------------------------
SPHTool::~SPHTool() {}
// ------------------------------------------------------------------------
//
// ------------------------------------------------------------------------
void SPHTool::set_param_int(const std::string& param_name,
                            const int& param_value) {
  //
  // Check if the parameter exists in the map
  //
  bool found_it = false;
  for (auto i : param_int_) {
    if (i.first == param_name) {
      found_it = true;
    }
  }
  if (!found_it) {
    throw std::runtime_error("Unknown parameter name : " + param_name);
  } else {
    param_int_[param_name] = param_value;
    if (param_name == "output_ascii_precision") {
      regular_data_.SetOutputPrecision(param_value);
      amr_data_.SetOutputPrecision(param_value);
    }
  }
}
// ------------------------------------------------------------------------
//
// ------------------------------------------------------------------------
int SPHTool::get_param_int(const std::string& param_name) {
  bool found_it = false;
  for (auto i : param_int_) {
    if (i.first == param_name) {
      return i.second;
    }
  }
  if (!found_it) {
    throw std::runtime_error("Unknown parameter name : " + param_name);
  }
  return -1;
}
// ------------------------------------------------------------------------
//
// ------------------------------------------------------------------------
std::vector<std::string> SPHTool::get_param_list() {
  std::vector<std::string> param_name_list;
  for (auto i : param_int_) {
    param_name_list.push_back(i.first);
  }
  return param_name_list;
}

// ------------------------------------------------------------------------
//
// ------------------------------------------------------------------------
void SPHTool::_interpolate(const std::string& var_name, double* xi,
                           int nxi, double* yi, int nyi,
                           double* zi, int nzi,
                           double* vector_double_out, int dim_out) {
//  InitTree(param_int_["max_particle_per_cell"], param_int_["max_tree_depth"]);

  for (int i = 0; i < dim_out; ++i) {
    vector_double_out[i] = 0.0;
  }
  InterpolateSingleVar(var_name, xi, yi, zi, nxi, param_int_["n_thread"],
                       vector_double_out);
}
// ------------------------------------------------------------------------
//
// ------------------------------------------------------------------------
void SPHTool::_set_grid(const std::string& grid_type, double* xi,
                        int nxi, double* yi, int nyi, double* zi,
                        int nzi, const std::string& crd_system) {
  //  //
  //  // Since we set a new grid, we'll reset all gridded data containers
  //  //
  //  this->amr_data_.Reset();
  //  this->regular_data_.Reset();

  this->grid_type_ = grid_type;
  if (grid_type == "regular") {
    RegularGrid reg_grid;
    reg_grid.SetCellInterface(0, xi, nxi);
    reg_grid.SetCellInterface(1, yi, nyi);
    reg_grid.SetCellInterface(2, zi, nzi);
    reg_grid.SetCrdSystem(crd_system);

    this->regular_data_.SetGrid(&reg_grid);
  } else if (grid_type == "amr") {
    //
    // Generate the root node coordinates
    //
    std::vector<double> root_crd_min(3, 0.0);
    std::vector<double> root_crd_max(3, 0.0);
    root_crd_min[0] = xi[0];
    root_crd_min[1] = yi[0];
    root_crd_min[2] = zi[0];

    root_crd_max[0] = xi[1];
    root_crd_max[1] = yi[1];
    root_crd_max[2] = zi[1];

    std::cout << "Building AMR grid ... ";
    //
    // Create a temporary copy of the point data set which will be transformed
    // to the coordinate system in which the AMR grid will be built
    //
    PointDataSet point_data_set_tmp;
    std::vector<double> crd(n_point_*n_dim_, 0.0);
    point_data_set_tmp.set_size(n_point_, n_dim_);
    _get_vector("crd", crd.data(), n_point_*n_dim_, n_dim_);
    point_data_set_tmp._set_vector("crd", crd.data(), n_point_*n_dim_, n_dim_);

    //
    // If the coordinate system of the AMR grid is not cartesian, transform the
    //  point coordinates to the coordinate system of the AMR grid
    //
    if (crd_system == "spherical") {
      point_data_set_tmp._transform_to_crd_system(crd_system);
    }
    //
    // Now build the AMR grid
    //
    AMRGrid amr_grid(&point_data_set_tmp, crd_system);
    amr_grid.BuildGrid(param_int_["max_particle_per_amr_grid_cell"],
                       param_int_["max_amr_grid_depth"], root_crd_min,
                       root_crd_max, crd_system);
    std::cout << " Done " << std::endl;
    //
    // Write some statistics about the grid
    //
    std::cout << "AMR grid depth   : " << amr_grid.GetTreeDepth() << std::endl;
    std::cout << "Number of nodes  : " << amr_grid.GetNNode() << std::endl;
    std::cout << "Number of leaves : " << amr_grid.GetNLeaf() << std::endl;

    this->amr_data_.SetGrid(&amr_grid);
  } else {
    throw std::runtime_error("Unkonwn grid type : " + grid_type +
                             ". Allowed grid types are 'amr' or 'regular'");
  }
}
// ------------------------------------------------------------------------
//
// ------------------------------------------------------------------------
void SPHTool::_regrid(const std::vector<std::string>& var_name) {
  if (this->grid_type_ == "regular") {
    std::cout << "Gridding to regular grid" << std::endl;
    ReGrid(var_name, param_int_["max_particle_per_cell"],
           param_int_["max_tree_depth"], param_int_["n_threads"],
           "regular", &regular_data_);

  } else if (this->grid_type_ == "amr") {
    std::cout << "Gridding to AMR grid" << std::endl;
    ReGrid(var_name, param_int_["max_particle_per_cell"],
           param_int_["max_tree_depth"], param_int_["n_threads"],
           "amr", &amr_data_);
  }
  std::cout << "Gridding done " << std::endl;
}
// ------------------------------------------------------------------------
//
// ------------------------------------------------------------------------
void SPHTool::_refine_amr_gradient(const int& n_trial_point,
                                   const double& threshold,
                                   const double& density_floor,
                                   const int& max_amr_grid_depth) {
  std::cout << "---------------------------------------------\n";
  std::cout << "Refining AMR Grid " << std::endl;
  //
  // Get the current AMR grid
  //
  AMRGrid amr_grid = this->amr_data_.GetGrid();
  std::string crd_system = amr_grid.GetCrdSystem();
  //
  // Update the max grid depth
  //
  amr_grid.SetMaxTreeDepth(max_amr_grid_depth);
  //
  // Set up the random number generator
  //
  std::default_random_engine generator;
  std::uniform_real_distribution<double> random_dist(0.0, 1.0);

  //
  // Get the leaf cell centers
  //
  int n_leaf = amr_grid.GetNLeaf();
  int n_node = amr_grid.GetNNode();
  int n_node_old = 0;
  int n_new_leaf = n_leaf;
  //
  // Loop until grid is converged (i.e. there is no further refinement)
  //
  int iiter = 0;
  double xc_tmp = 0.0;
  double yc_tmp = 0.0;
  double zc_tmp = 0.0;
  while (n_new_leaf > 0) {
    std::cout << std::endl;
    std::cout << "AMR refinement iteration " << iiter << std::endl;
    //
    // Get the cell interfaces
    //
    std::vector<double> xi(n_leaf * 2, 0.0);
    std::vector<double> yi(n_leaf * 2, 0.0);
    std::vector<double> zi(n_leaf * 2, 0.0);

    amr_grid.GetCellInterface(0, xi.data(), n_leaf*2);
    amr_grid.GetCellInterface(1, yi.data(), n_leaf*2);
    amr_grid.GetCellInterface(2, zi.data(), n_leaf*2);

    //
    // Get the leaf IDs
    //
    std::vector<int> leaf_node_id = amr_grid.GetLeafNodeIDs();
    //
    // Now check which leaf cells should be resolved. Thus we need to loop
    // through all leaf cells and check if they have a high enough variation
    // of density than the threshold value. We do not want to check all the
    // leaf cells all the time. Only the "new" leaf cells need to be checked
    // which have been created in the previous iterations. However, since
    // the tree is not stored linearly, new leaf cells created in the tree
    // will not be stored contiguously in any output container (e.g. the output
    // of Tree<T>.GetCellInterface()). The way to select the new cells is the
    // following. Each node in the tree has an ID, starting with 0 of the root
    // node. Thus the highest node ID in the tree is N_node-1. In each iteration
    // we store the number of nodes in the tree. Then in the following
    // iteration, we check the ID of each leaf cell in the output arrays and the
    // new cells will be those whose node ID is larger than the current
    // N_node-1.
    //
    // Get the new leaf indices
    //
    std::vector<int> new_leaf_idx(n_new_leaf, -1);
    {
      int idx = 0;
      for (int ileaf = 0; ileaf < n_leaf; ++ileaf) {
        if (leaf_node_id[ileaf] >= n_node_old) {
          new_leaf_idx[idx] = ileaf;
          idx += 1;
        }
      }
    }
    //
    // Get pointers to each leaf
    //
    std::vector<TreeNode<double>*> amr_leafs = amr_grid.GetPointerToLeafNodes();
    //
    // Min/Max. variable containers
    //
    std::vector<double> var_min(n_new_leaf, 1e99);
    std::vector<double> var_max(n_new_leaf, -1e99);
    //
    // Loop over the number of trial points
    //
    for (int i = 0; i < n_trial_point; ++i) {
      //
      // Containers for the new cell centers with random offsets
      //
      std::vector<double>xc(n_new_leaf, 0.0);
      std::vector<double>yc(n_new_leaf, 0.0);
      std::vector<double>zc(n_new_leaf, 0.0);

      //
      // Generate cell center coordinates with random offsets with respect
      //  to the original coordinates
      //
      if (crd_system == "cartesian") {
        for (int icell = 0; icell < n_new_leaf; ++icell) {
          int ci_index = new_leaf_idx[icell];
          double offset = random_dist(generator) *
                          (xi[1 + 2 * ci_index] - xi[2 * ci_index]);
          xc[icell]= xi[2 * ci_index] + offset;

          offset = random_dist(generator) *
                   (yi[1 + 2 * ci_index] - yi[2 * ci_index]);
          yc[icell] = yi[2 * ci_index] + offset;

          offset = random_dist(generator) *
                   (zi[1 + 2 * ci_index] - zi[2 * ci_index]);
          zc[icell] = zi[2 * ci_index] + offset;
        }
      } else {
        for (int icell = 0; icell < n_new_leaf; ++icell) {
          int ci_index = new_leaf_idx[icell];
          double offset = random_dist(generator) *
                          (xi[1 + 2 * ci_index] - xi[2 * ci_index]);
          xc_tmp = xi[2 * ci_index] + offset;

          offset = random_dist(generator) *
                   (yi[1 + 2 * ci_index] - yi[2 * ci_index]);
          yc_tmp = yi[2 * ci_index] + offset;

          offset = random_dist(generator) *
                   (zi[1 + 2 * ci_index] - zi[2 * ci_index]);
          zc_tmp = zi[2 * ci_index] + offset;

          xc[icell] = xc_tmp * sin(yc_tmp) * cos(zc_tmp);
          yc[icell] = xc_tmp * sin(yc_tmp) * sin(zc_tmp);
          zc[icell] = xc_tmp * cos(yc_tmp);
        }
      }
      //
      // Calculate the density at each random point within the leaf cell
      //
      std::vector<double> var(n_new_leaf, 0.0);
      _interpolate("density", xc.data(), n_new_leaf, yc.data(), n_new_leaf,
                   zc.data(), n_new_leaf, var.data(), n_new_leaf);
      //
      // Get the statistics of each cell
      //
      for (int icell = 0; icell < n_new_leaf; ++icell) {
        var[icell] = std::max(var[icell], density_floor);
        var_min[icell] = std::min(var[icell], var_min[icell]);
        var_max[icell] = std::max(var[icell], var_max[icell]);
      }
    }

    //
    // Now check which AMR grid cell needs to be resolved
    //
    for (int icell = 0; icell < n_new_leaf; ++icell) {
      double delta_var = (var_max[icell] - var_min[icell]) / var_max[icell];
      if (delta_var > threshold) {
        int dummy_node_old = amr_grid.GetNNode();
        amr_grid.SplitCell(amr_leafs[new_leaf_idx[icell]]);
        int dummy_node_new = amr_grid.GetNNode();
        if (((dummy_node_new - dummy_node_old) != 8) &
            ((dummy_node_new - dummy_node_old) != 0)) {
          std::cout << dummy_node_old << std::endl;
          std::cout << dummy_node_new << std::endl;
          throw std::runtime_error("Splitting went wrong");
        }
      }
    }
    std::cout << "Verifying AMR grid...";
    amr_grid.VerifyGrid(&amr_grid);
    std::cout << "Done" << std::endl;
    //
    // Update the counters
    //
    n_node_old = n_node;
    n_node = amr_grid.GetNNode();
    n_leaf = amr_grid.GetNLeaf();
    n_new_leaf = n_node - n_node_old;
    std::cout << "Nr of leaf cells : " << n_leaf << std::endl;
    std::cout << "New leaf cells   : " << n_new_leaf << std::endl;
    std::cout << "Tree depth       : " << amr_grid.GetTreeDepth() << std::endl;
    std::cout << "Max Tree depth   : " << amr_grid.GetMaxTreeDepth()
              << std::endl;

    iiter += 1;
  }

  this->amr_data_.SetGrid(&amr_grid);
}

// ------------------------------------------------------------------------
//
// ------------------------------------------------------------------------
void SPHTool::_get_gridded_var(const std::string& var_name,
                               double* vector_double_out, int dim_out) {
  if (this->grid_type_ == "regular") {
    bool is_var_scalar = this->regular_data_.is_var_scalar(var_name);
    if (is_var_scalar) {
      this->regular_data_._get_scalar(var_name, vector_double_out, dim_out);
    } else {
      int n_dim = this->regular_data_.get_ndim();
      this->regular_data_._get_vector(var_name, vector_double_out, dim_out,
                                    n_dim);
    }
  } else {
    bool is_var_scalar = this->regular_data_.is_var_scalar(var_name);
    if (is_var_scalar) {
      this->regular_data_._get_scalar(var_name, vector_double_out, dim_out);
    } else {
      int n_dim = this->regular_data_.get_ndim();
      this->regular_data_._get_vector(var_name, vector_double_out, dim_out,
                                    n_dim);
    }
  }
}
// ------------------------------------------------------------------------
//
// ------------------------------------------------------------------------
void SPHTool::_write_gridded_var(const std::string& file_name,
                            const std::string& format,
                            const std::string& var_name,
                            const bool& is_format_binary,
                            const double& scalar_floor) {
  if (this->grid_type_ == "regular") {
    this->regular_data_.WriteData(file_name, format, var_name, is_format_binary,
                                  scalar_floor);
  } else {
    this->amr_data_.WriteData(file_name, format, var_name, is_format_binary,
                                  scalar_floor);
  }
}
// ------------------------------------------------------------------------
//
// ------------------------------------------------------------------------
void SPHTool::_write_gridded_var_multicomp(
    const std::string& file_name, const std::string& format,
    const std::vector<std::string>& var_name, const bool& is_format_binary,
    const double& scalar_floor) {
  if (this->grid_type_ == "regular") {
    this->regular_data_.WriteData(file_name, format, var_name, is_format_binary,
                                  scalar_floor);
  } else {
    this->amr_data_.WriteData(file_name, format, var_name, is_format_binary,
                                  scalar_floor);
  }
}
// ------------------------------------------------------------------------
//
// ------------------------------------------------------------------------
int SPHTool::_get_grid_n_cell(const int& iaxis) {
  int n_cell = -1;
  if (this->grid_type_ == "regular") {
    n_cell = this->regular_data_.GetNCell(iaxis);
  } else {
    n_cell = this->amr_data_.GetNCell(iaxis);
  }
  return n_cell;
}
// ------------------------------------------------------------------------
//
// ------------------------------------------------------------------------
void SPHTool::_get_grid_cell_centers(const int& iaxis,
                                     double* vector_double_out,
                                     int dim_out) {
  // Reset the output array
  for (int i = 0; i < dim_out; ++i) {
    vector_double_out[i] = 0.0;
  }
  if (this->grid_type_ == "regular") {
    this->regular_data_.GetCellCenter(iaxis, vector_double_out, dim_out);
  } else {
    this->amr_data_.GetCellCenter(iaxis, vector_double_out, dim_out);
  }
}
// ------------------------------------------------------------------------
//
// ------------------------------------------------------------------------
void SPHTool::write_grid(const std::string& file_name,
                         const std::string& format) {
  if (this->grid_type_ == "regular") {
    this->regular_data_.WriteGrid(file_name, format);
  } else {
    this->amr_data_.WriteGrid(file_name, format);
  }
}
// ------------------------------------------------------------------------
//
// ------------------------------------------------------------------------
void SPHTool::_project_scalar(const std::string& var_name,
                              const double& image_center_x,
                              const double& image_center_y,
                              const double& image_size, const int& npix,
                              const double& incl_deg, const double& posang_deg,
                              const double& phi_deg, double* vector_double_out,
                              int dim_out) {
  //
  // Generate the pixel center grid
  //
  double pixel_size = image_size / static_cast<double>(npix);
  double xmin = image_center_x - image_size*0.5;
  double ymin = image_center_y - image_size*0.5;
  std::vector<double> xcent(npix, 0.0);
  std::vector<double> ycent(npix, 0.0);
  for (int i = 0; i < npix; ++i) {
    double idx = static_cast<double>(i) + 0.5;
    xcent[i] = xmin + idx * pixel_size;
    ycent[i] = ymin + idx * pixel_size;
  }
  //
  // Convert degrees to radianas
  //
  double incl_rad = incl_deg / 180. * M_PI;
  double posang_rad = posang_deg / 180. * M_PI;
  double phi_rad = phi_deg / 180. * M_PI;
  //
  // Check if the variable is a scalar
  //
  int ind_var = GetScalarIndex(var_name);
  //
  // If we have a scalar variable
  //
  if (ind_var >= 0) {
    //
    // Do the projection
    //
    ProjectScalarToPlane(var_name, xcent, ycent, incl_rad, posang_rad, phi_rad,
                         param_int_["n_threads"], vector_double_out);
  } else {
    throw std::runtime_error("No scalar variable has been found with name " +
                             var_name);
  }
}
// ------------------------------------------------------------------------
//
// ------------------------------------------------------------------------
void SPHTool::_get_slice(const std::string& var_name, const double& slice_xcent,
                         const double& slice_ycent, const double& slice_zcent,
                         const double& slice_size, const int& n_pixel,
                         const double& incl_deg, const double& posang_deg,
                         const double& phi_deg, double* vector_double_out,
                         int dim_out) {
  int n_pixel_x = n_pixel;
  int n_pixel_y = n_pixel;
  double delta_x = slice_size / static_cast<double>(n_pixel);
  double delta_y = delta_x;
  double incl_rad = incl_deg / 180. * M_PI;
  double posang_rad = posang_deg / 180. * M_PI;
  double phi_rad = phi_deg / 180. * M_PI;

  GetVarSlice(var_name, slice_xcent, slice_ycent, slice_zcent, n_pixel_x,
              n_pixel_y, delta_x, delta_y, incl_rad, posang_rad, phi_rad,
              param_int_["n_threads"], vector_double_out);
}
// ------------------------------------------------------------------------
//
// ------------------------------------------------------------------------
void SPHTool::_write_vtk(const std::string& file_name,
                         const std::vector<std::string>& var_name) {
  if (this->grid_type_ == "regular") {
    this->regular_data_.WriteVTK(file_name, var_name);
  } else {
    this->amr_data_.WriteVTK(file_name, var_name);
  }
}
// ------------------------------------------------------------------------
//
// ------------------------------------------------------------------------
void SPHTool::finalize() {
  validate();
  InitTree(param_int_["max_particle_per_cell"], param_int_["max_tree_depth"]);
}
