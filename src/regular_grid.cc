// ============================================================================
// regular_grid.cc
//
// This file is part of sphtool:
// Gridding and visualisation package for SPH simulations
//
// Copyright (C) 2018 Attila Juhasz
// Author: Attila Juhasz
//
// sphtool is free software; you can redistribute it and/or modify it under
// the terms of the GNU General Public License as published by the Free Software
// Foundation; either version 2 of the License, or (at your option) any later
// version.
//
// sphtool is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.
// ============================================================================

#include <cmath>
#include <stdexcept>
#include <iostream>
#include <utility>
#include <fstream>
#include <iomanip>
#include "../include/regular_grid.h"

const char *RegularGrid::grid_type_ = "regular";
// ------------------------------------------------------------------------
//
// ------------------------------------------------------------------------
RegularGrid::RegularGrid() {
  crd_system_ = "cartesian";
  n_x_ = 0;
  n_y_ = 0;
  n_z_ = 0;
  active_dim_.resize(3);
  for (int i = 0; i < 3; ++i)
    active_dim_[i] = false;
  output_ascii_precision_ = 9;
}
// ------------------------------------------------------------------------
//
// ------------------------------------------------------------------------
RegularGrid::~RegularGrid() {}
// ------------------------------------------------------------------------
//
// ------------------------------------------------------------------------
std::string RegularGrid::GetGridType() { return std::string(grid_type_); }
// ------------------------------------------------------------------------
//
// ------------------------------------------------------------------------
void RegularGrid::SetOutputPrecision(const int& precision) {
  this->output_ascii_precision_ = precision;
}
// ------------------------------------------------------------------------
//
// ------------------------------------------------------------------------
int RegularGrid::GetOutputPrecision() {
  return this->output_ascii_precision_;
}
// ------------------------------------------------------------------------
//
// ------------------------------------------------------------------------
void RegularGrid::ResetGrid() {
  this->crd_system_ = "cartesian";
  this->n_x_ = 0;
  this->n_y_ = 0;
  this->n_z_ = 0;
  this->x_.resize(0);
  this->y_.resize(0);
  this->z_.resize(0);
  this->xi_.resize(0);
  this->yi_.resize(0);
  this->zi_.resize(0);
  active_dim_.resize(3);
  for (int i = 0; i < 3; ++i)
    active_dim_[i] = false;
}
// ------------------------------------------------------------------------
//
// ------------------------------------------------------------------------
int RegularGrid::GetNCell(const int& iaxis) {
  int n_axis = -1;
  if (iaxis == 0) {
    n_axis = n_x_;
  } else if (iaxis == 1) {
    n_axis = n_y_;
  } else if (iaxis == 2) {
    n_axis = n_z_;
  } else {
    throw std::runtime_error("Unknown axis index " + std::to_string(iaxis) +
                             ". Number of cells cannot be determined.");
  }
  return n_axis;
}
// ------------------------------------------------------------------------
//
// ------------------------------------------------------------------------
int RegularGrid::GetNCell() { return n_x_ * n_y_ * n_z_; }
// ------------------------------------------------------------------------
//
// ------------------------------------------------------------------------
int RegularGrid::GetNInterface(const int& iaxis) {
  int n_axis = -1;
  if (iaxis == 0) {
    n_axis = n_x_ + 1;
  } else if (iaxis == 1) {
    n_axis = n_y_ + 1;
  } else if (iaxis == 2) {
    n_axis = n_z_ + 1;
  } else {
    throw std::runtime_error(
        "Unknown axis index " + std::to_string(iaxis) +
        ". Number of cell interfaces cannot be determined.");
  }
  return n_axis;
}
// ------------------------------------------------------------------------
//
// ------------------------------------------------------------------------
void RegularGrid::GenerateAxis(const int& iaxis, double* axis_segment_bounds,
                               const int& n_bounds, int* n_cell_in_segment,
                               const int& n_segment) {
  int n_cell_interface = 1;
  for (int iseg = 0; iseg < n_segment; ++iseg)
    n_cell_interface += n_cell_in_segment[iseg];

  //
  // If we have more than one grid cell the dimension is active
  //
  if (n_cell_interface > 2) {
    active_dim_[iaxis] = true;
  } else {
    active_dim_[iaxis] = false;
  }

  std::vector<double> axis_crd_i(n_cell_interface, 0.0);
  int icell = 0;
  if (n_segment > 1) {
    for (int iseg = 0; iseg < n_segment-1; ++iseg) {
      for (int ip = 0; ip < n_cell_in_segment[iseg]; ++ip) {
        axis_crd_i[icell] =
            axis_segment_bounds[iseg] +
            (axis_segment_bounds[iseg + 1] - axis_segment_bounds[iseg]) *
                static_cast<double>(ip) /
                static_cast<double>(n_cell_in_segment[iseg] + 1);
        icell++;
      }
    }
  }

  for (int ip = 0; ip < n_cell_in_segment[n_segment-1] + 1; ++ip) {
    axis_crd_i[icell] = axis_segment_bounds[n_segment - 1] +
        (axis_segment_bounds[n_segment] - axis_segment_bounds[n_segment - 1]) *
            static_cast<double>(ip) /
            static_cast<double>(n_cell_in_segment[n_segment - 1]);
    icell++;
  }


  if (iaxis == 0) {
    n_x_ = axis_crd_i.size() - 1;
    xi_.resize(n_x_ + 1, 0.0);
    x_.resize(n_x_, 0.0);
    for (int ix = 0; ix < n_x_ + 1; ++ix)
      xi_[ix] = axis_crd_i[ix];
    for (int ix = 0; ix < n_x_; ++ix)
      x_[ix] = 0.5 * (axis_crd_i[ix] + axis_crd_i[ix + 1]);

  } else if (iaxis == 1) {
    n_y_ = axis_crd_i.size() - 1;
    yi_.resize(n_y_ + 1, 0.0);
    y_.resize(n_y_, 0.0);
    for (int iy = 0; iy < n_y_ + 1; ++iy)
      yi_[iy] = axis_crd_i[iy];
    for (int iy = 0; iy < n_y_; ++iy)
      y_[iy] = 0.5 * (axis_crd_i[iy] + axis_crd_i[iy + 1]);

  } else if (iaxis == 2) {
    n_z_ = axis_crd_i.size() - 1;
    zi_.resize(n_z_ + 1, 0.0);
    z_.resize(n_z_, 0.0);
    for (int iz = 0; iz < n_z_ + 1; ++iz)
      zi_[iz] = axis_crd_i[iz];
    for (int iz = 0; iz < n_z_; ++iz)
      z_[iz] = 0.5 * (axis_crd_i[iz] + axis_crd_i[iz + 1]);
  } else {
    throw std::runtime_error(
        "Unknown dimension in RegularGrid::GenerateAxis() " +
        std::to_string(iaxis));
  }
}
// ------------------------------------------------------------------------
//
// ------------------------------------------------------------------------
void RegularGrid::SetCellInterface(const int& iaxis,
                                       double* vector_double_in,
                                       const int& dim) {
  if (dim < 0) {
    throw std::runtime_error(
        "Cell interfaces of a regular grid axis cannot be set to an array with "
        "negative length : " +
        std::to_string(dim));
  }

  //
  // If we have more than one grid cell the dimension is active
  //
  if (dim > 2) {
    active_dim_[iaxis] = true;
  } else {
    active_dim_[iaxis] = false;
  }

  if (iaxis == 0) {
    xi_.resize(dim);
    n_x_ = dim - 1;
    for (int i = 0; i < dim; ++i) xi_[i] = vector_double_in[i];

    x_.resize(n_x_);
    for (int i = 0; i < n_x_; ++i) x_[i] = 0.5 * (xi_[i] + xi_[i+1]);

  } else if (iaxis == 1) {
    yi_.resize(dim);
    n_y_ = dim - 1;
    for (int i = 0; i < dim; ++i) yi_[i] = vector_double_in[i];

    y_.resize(n_y_);
    for (int i = 0; i < n_y_; ++i) y_[i] = 0.5 * (yi_[i] + yi_[i+1]);

  } else if (iaxis == 2) {
    zi_.resize(dim);
    n_z_ = dim - 1;
    for (int i = 0; i < dim; ++i) zi_[i] = vector_double_in[i];

    z_.resize(n_z_);
    for (int i = 0; i < n_z_; ++i) z_[i] = 0.5 * (zi_[i] + zi_[i+1]);
  }
}
// ------------------------------------------------------------------------
//
// ------------------------------------------------------------------------
void RegularGrid::GetCellInterface(const int& iaxis,
                                       double* vector_double_out,
                                       const int& dim) {
  if (iaxis == 0) {
    for (int i = 0; i < n_x_ + 1; ++i) vector_double_out[i] = xi_[i];
  } else if (iaxis == 1) {
    for (int i = 0; i < n_y_ + 1; ++i) vector_double_out[i] = yi_[i];
  } else if (iaxis == 2) {
    for (int i = 0; i < n_z_ + 1; ++i) vector_double_out[i] = zi_[i];
  }
}
// ------------------------------------------------------------------------
//
// ------------------------------------------------------------------------
void RegularGrid::SetCellCenter(const int& iaxis, double* vector_double_in,
                                const int& dim) {
  if (dim <= 0) {
    throw std::runtime_error(
        "Cell centers of a regular grid axis should be set to an array with "
        "at least one element. Current array has : " +
        std::to_string(dim) + " element");
  }

  //
  // If we have more than one grid cell the dimension is active
  //
  if (dim > 1) {
    active_dim_[iaxis] = true;
  } else {
    active_dim_[iaxis] = false;
  }


  if (iaxis == 0) {
    n_x_ = dim;
    x_.resize(n_x_);
    xi_.resize(n_x_ + 1);
    for (int i = 0; i < n_x_; ++i) x_[i] = vector_double_in[i];

    if (dim > 1) {
      // Check if we have a logarithmic grid
      double d1 = x_[1] - x_[0];
      double d2 = x_[n_x_-1] - x_[n_x_-2];
      // If the grid is linear
      if ((d2/d1 - 1.0) < 1e-9) {
        xi_[0] = x_[0] - 0.5 * d1;
        for (int i = 1; i < n_x_ + 1; ++i) xi_[i] = xi_[i-1] + d1;

      // If the grid is logarithmic
      } else {
        double q = x_[1] / x_[0];
        xi_[0] = 2.0 * x_[0] / (1.0 + q);
        for (int i = 1; i < n_x_ + 1; ++i) xi_[i] = xi_[i-1] * q;
      }
    } else {
      if (dim == 1) {
        x_[0] = vector_double_in[0];
        if (x_[0] == 0.) {
          xi_[0] = -1.0;
          xi_[1] = 1.0;
        } else {
          xi_[0] = x_[0] * 1.1;
          xi_[0] = x_[0] * 0.9;
        }
      } else {
        throw std::runtime_error("First dimension contain no points: dim = " +
                                 std::to_string(dim));
      }
    }
  } else if (iaxis == 1) {
    n_y_ = dim;
    y_.resize(n_y_);
    yi_.resize(n_y_ + 1);
    for (int i = 0; i < n_y_; ++i) y_[i] = vector_double_in[i];

    if (dim > 1) {
      // Check if we have a logarithmic grid
      double d1 = y_[1] - y_[0];
      double d2 = y_[n_y_ - 1] - y_[n_y_ - 2];
      // If the grid is linear
      if ((d2 / d1 - 1.0) < 1e-9) {
        yi_[0] = y_[0] - 0.5 * d1;
        for (int i = 1; i < n_y_ + 1; ++i) yi_[i] = yi_[i - 1] + d1;

        // If the grid is logarithmic
      } else {
        double q = y_[1] / y_[0];
        yi_[0] = 2.0 * y_[0] / (1.0 + q);
        for (int i = 1; i < n_y_ + 1; ++i) yi_[i] = yi_[i - 1] * q;
      }
    } else {
      if (dim == 1) {
        y_[0] = vector_double_in[0];
        if (y_[0] == 0.) {
          yi_[0] = -1.0;
          yi_[1] = 1.0;
        } else {
          yi_[0] = y_[0] * 1.1;
          yi_[0] = y_[0] * 0.9;
        }
      } else {
        throw std::runtime_error("Second dimension contain no points: dim = " +
                                 std::to_string(dim));
      }
    }
  } else if (iaxis == 2) {
    n_z_ = dim;
    z_.resize(n_z_);
    zi_.resize(n_z_ + 1);
    for (int i = 0; i < n_z_; ++i) z_[i] = vector_double_in[i];

    if (dim > 1) {
      // Check if we have a logarithmic grid
      double d1 = z_[1] - z_[0];
      double d2 = z_[n_z_ - 1] - z_[n_z_ - 2];
      // If the grid is linear
      if ((d2 / d1 - 1.0) < 1e-9) {
        zi_[0] = z_[0] - 0.5 * d1;
        for (int i = 1; i < n_z_ + 1; ++i) zi_[i] = zi_[i - 1] + d1;

        // If the grid is logarithmic
      } else {
        double q = z_[1] / z_[0];
        zi_[0] = 2.0 * z_[0] / (1.0 + q);
        for (int i = 1; i < n_z_ + 1; ++i) zi_[i] = zi_[i - 1] * q;
      }
    } else {
      if (dim == 1) {
        z_[0] = vector_double_in[0];
        if (z_[0] == 0.) {
          zi_[0] = -1.0;
          zi_[1] = 1.0;
        } else {
          zi_[0] = z_[0] * 1.1;
          zi_[0] = z_[0] * 0.9;
        }
      } else {
        throw std::runtime_error("Third dimension contain no points: dim = " +
                                 std::to_string(dim));
      }
    }
  }
}
// ------------------------------------------------------------------------
//
// ------------------------------------------------------------------------
void RegularGrid::GetCellCenter(const int& iaxis, double* vector_double_out,
                                const int& dim) {
  if (iaxis == 0) {
    for (int i = 0; i < n_x_; ++i) vector_double_out[i] = x_[i];
  } else if (iaxis == 1) {
    for (int i = 0; i < n_y_; ++i) vector_double_out[i] = y_[i];
  } else if (iaxis == 2) {
    for (int i = 0; i < n_z_; ++i) vector_double_out[i] = z_[i];
  }
}
// ------------------------------------------------------------------------
//
// ------------------------------------------------------------------------
void RegularGrid::WriteToFile(const std::string& file_name,
                              const std::string& format) {
  std::cout << "Writing " << file_name << "...";

  if (format == "radmc3d") {
    std::ofstream out_stream;
    // Open the stream
    try {
      out_stream.open(file_name, std::ofstream::out | std::ofstream::trunc);
    } catch (std::exception &e) {
      std::cout << e.what() << std::endl;
    }
    // iformat
    out_stream << "1\n";  // iformat
    // Grid style (0 - Regular, 1 - OcTree, 10 - Nested)
    out_stream << "0\n";
    // Coordinate system (0-99 Cartesian, 100-199 - Spherical)
    if (crd_system_ == "cartesian") {
      out_stream << "0\n";
    } else if (crd_system_ == "spherical") {
      out_stream << "100\n";
    } else {
      throw std::runtime_error("Unknown coordiante system : " + crd_system_ +
                               " encountered when writing regular grid to "
                               "file. Only cartesian and spherical coordinate "
                               "systems are implemented");
    }
    // Gridinfo
    out_stream << "0\n";
    // Active dimensions
    for (int i = 0; i < 3; ++i) {
      out_stream << active_dim_[i] << " ";
    }
    out_stream << "\n";
    // Number of grid cells(!) in each dimension
    out_stream << n_x_ << " " << n_y_ << " " << n_z_ << "\n";
    // Set the precision
    // Now write the cell interfaces(!) in each dimension
    for (int i = 0; i < n_x_ + 1; i++) {
      out_stream << std::fixed << std::scientific
                 << std::setprecision(this->output_ascii_precision_) << xi_[i]
                 << "\n";
    }
    for (int i = 0; i < n_y_ + 1; i++) {
      out_stream << std::fixed << std::scientific
                 << std::setprecision(this->output_ascii_precision_) << yi_[i]
                 << "\n";
    }
    for (int i = 0; i < n_z_ + 1; i++) {
      out_stream << std::fixed << std::scientific
                 << std::setprecision(this->output_ascii_precision_) << zi_[i]
                 << "\n";
    }
    // Close the stream
    out_stream.close();
  } else {
    throw std::runtime_error("Unknown format " + format +
                             "\n Currently only 'radmc3d' is supported");
  }
  std::cout << "Done" << std::endl;
}
// ------------------------------------------------------------------------
//
// ------------------------------------------------------------------------
void RegularGrid::ReadFromFile(const std::string& file_name,
                              const std::string& format) {
  if (format == "radmc3d") {
    std::fstream input_stream;
    // Open the input stream
    try {
      input_stream.open(file_name, std::ifstream::in);
    } catch (std::exception &e) {
      std::cout << e.what() << std::endl;
    }
    std::cout << "Reading " << file_name << std::endl;
    int iformat;
    input_stream >> iformat;
    if (iformat != 1) {
      throw std::runtime_error(std::string("Unknown file format ID of ") +
                               std::to_string(iformat) +
                               std::string(" in ") +
                               std::string(file_name));
    }

    int grid_style = -1;
    input_stream >> grid_style;
    if (grid_style == 0) {
      int crd_system_int = -1;
      input_stream >> crd_system_int;
      if (crd_system_int < 100) {
        crd_system_ = "cartesian";
      } else if (crd_system_int < 200) {
        crd_system_ = "spherical";
      } else {
        throw std::runtime_error(
            "Unknown coordinate system (" + std::to_string(crd_system_int) +
            ") encountered when reading regular grid from file. Only cartesian "
            "(0-99) and spherical (100-199) coordinate systems are "
            "implemented.");
      }
      int grid_info;
      input_stream >> grid_info;
      active_dim_.resize(3);
      // Active dimensions
      int act_dim[3];
      input_stream >> act_dim[0] >> act_dim[1] >> act_dim[2];
      for (int i = 0; i < 3; ++i) {
        if (act_dim[i] == 0) {
          active_dim_[i] = false;
        } else {
          active_dim_[i] = true;
        }
      }
      //      input_stream >> active_dim_[0] >> active_dim_[1] >>
      //      active_dim_[2];
      // Number of grid cells(!) in each dimension
      input_stream >> n_x_ >> n_y_ >> n_z_;
      // Read the cell interfaces in the first dimension
      xi_.resize(n_x_+1);
      for (int i = 0; i < n_x_ + 1; ++i) {
        input_stream >> xi_[i];
      }
      // Generate the cell centers in the first dimension
      x_.resize(n_x_);
      for (int i = 0; i < n_x_; ++i) {
        x_[i] = 0.5 * (xi_[i] + xi_[i + 1]);
      }

      // Read the cell interfaces in the second dimension
      yi_.resize(n_y_+1);
      for (int i = 0; i < n_y_ + 1; ++i) {
        input_stream >> yi_[i];
      }
      // Generate the cell centers in the second dimension
      y_.resize(n_y_);
      for (int i = 0; i < n_y_; ++i) {
        y_[i] = 0.5 * (yi_[i] + yi_[i + 1]);
      }

      // Read the cell interfaces in the third dimension
      zi_.resize(n_z_+1);
      for (int i = 0; i < n_z_ + 1; ++i) {
        input_stream >> zi_[i];
      }
      // Generate the cell centers in the third dimension
      z_.resize(n_z_);
      for (int i = 0; i < n_z_; ++i) {
        z_[i] = 0.5 * (zi_[i] + zi_[i + 1]);
      }

    } else if (grid_style == 1) {
      throw std::runtime_error(
          "Wrong grid style. " + file_name +
          " octree AMR mesh cannot be read into a regular grid container.");
    } else if (grid_style == 10) {
      throw std::runtime_error(
          "Wrong grid style. " + file_name +
          " nested mesh cannot be read into a regular grid container.");
    } else {
      throw std::runtime_error("Unknown grid style id of " +
                               std::to_string(grid_style) +
                               ". Allowed grid styles are : 0 - regular grid, "
                               "1 - octree, 10 - nested "
                               "mesh");
    }
    input_stream.close();
  } else {
    throw std::runtime_error("Unknown format " + format +
                             "\n Currently only 'radmc3d' is supported");
  }
}
