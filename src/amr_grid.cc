// ============================================================================
// amr_grid.cpp
//
// This file is part of sphtool:
// Gridding and visualisation package for SPH simulations
//
// Copyright (C) 2018 Attila Juhasz
// Author: Attila Juhasz
//
// sphtool is free software; you can redistribute it and/or modify it under
// the terms of the GNU General Public License as published by the Free Software
// Foundation; either version 2 of the License, or (at your option) any later
// version.
//
// sphtool is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.
// ============================================================================

#include <utility>
#include <iomanip>
#include <stdexcept>

#include "../include/amr_grid.h"

const char *AMRGrid::grid_type_ = "amr";
// ------------------------------------------------------------------------
//
// ------------------------------------------------------------------------
AMRGrid::AMRGrid() {
  crd_system_ = "cartesian";
  active_dim_.resize(3);
  for (int i = 0; i < 3; ++i)
    active_dim_[i] = false;
  output_ascii_precision_ = 9;
}
// ------------------------------------------------------------------------
//
// ------------------------------------------------------------------------
AMRGrid::AMRGrid(PointDataSet* point_data_set) {
  crd_system_ = "cartesian";
  //
  // Note: hard-coded 3 dimension!
  // At this point only 3D AMR grids are used so it is not a
  // problem, however, later on one might want to generalize
  // to lower dimensions.
  //
  //
  active_dim_.resize(3);
  for (int i = 0; i < 3; ++i)
    active_dim_[i] = false;
  this->SetPointData(point_data_set);
  output_ascii_precision_ = 9;
}
// ------------------------------------------------------------------------
//
// ------------------------------------------------------------------------
AMRGrid::AMRGrid(PointDataSet* point_data_set, const std::string& crd_system) {
  if ((crd_system != "cartesian") & (crd_system != "spherical")) {
    throw std::runtime_error(
        "Unknown coordinate system encountered when setting up AMR grid : " +
        crd_system +
        ". AMR grids are implented only in cartesian and spherical "
        "coordiantes.");
  }

  crd_system_ = crd_system;
  //
  // Note: hard-coded 3 dimension!
  // At this point only 3D AMR grids are used so it is not a
  // problem, however, later on one might want to generalize
  // to lower dimensions.
  //
  //
  active_dim_.resize(3);
  for (int i = 0; i < 3; ++i)
    active_dim_[i] = false;
  n_leaf_ = 0;
  n_node_ = 0;
  this->SetPointData(point_data_set);
  output_ascii_precision_ = 9;
}
// ------------------------------------------------------------------------
//
// ------------------------------------------------------------------------
AMRGrid::~AMRGrid() {}
// ------------------------------------------------------------------------
//
// ------------------------------------------------------------------------
AMRGrid::AMRGrid(const AMRGrid& obj) {
  if (this != &obj) {
    is_leaf = obj.is_leaf;
    node_depth = obj.node_depth;
    center_crd = obj.center_crd;
    cell_half_width = obj.cell_half_width;
    CopyChildNodes(obj, this);
    point_id_first = obj.point_id_first;
    point_id_last = obj.point_id_last;
    max_n_point_per_cell_ = obj.max_n_point_per_cell_;
    max_tree_depth_ = obj.max_tree_depth_;
    tree_depth_ = obj.max_tree_depth_;
    crd_min_ = obj.crd_min_;
    crd_max_ = obj.crd_max_;
    ptr_point_data_set_ = obj.ptr_point_data_set_;
    crd_system_ = obj.crd_system_;
    active_dim_ = obj.active_dim_;
    n_leaf_ = obj.n_leaf_;
    n_node_ = obj.n_node_;
    output_ascii_precision_ = obj.output_ascii_precision_;
  }
}
// ------------------------------------------------------------------------
//
// ------------------------------------------------------------------------
AMRGrid& AMRGrid::operator=(const AMRGrid& obj) {
  if (this != &obj) {
    this->Reset();
    is_leaf = obj.is_leaf;
    node_depth = obj.node_depth;
    center_crd = obj.center_crd;
    cell_half_width = obj.cell_half_width;
    CopyChildNodes(obj, this);
    point_id_first = obj.point_id_first;
    point_id_last = obj.point_id_last;
    max_n_point_per_cell_ = obj.max_n_point_per_cell_;
    max_tree_depth_ = obj.max_tree_depth_;
    tree_depth_ = obj.max_tree_depth_;
    crd_min_ = obj.crd_min_;
    crd_max_ = obj.crd_max_;
    ptr_point_data_set_ = obj.ptr_point_data_set_;
    crd_system_ = obj.crd_system_;
    active_dim_ = obj.active_dim_;
    n_leaf_ = obj.n_leaf_;
    n_node_ = obj.n_node_;
    output_ascii_precision_ = obj.output_ascii_precision_;
  }
  return *this;
}
// ------------------------------------------------------------------------
//
// ------------------------------------------------------------------------
AMRGrid::AMRGrid(AMRGrid&& obj) {
  is_leaf = obj.is_leaf;
  node_depth = obj.node_depth;
  center_crd = std::move(obj.center_crd);
  cell_half_width = std::move(obj.cell_half_width);
  ptr_child_nodes = std::move(obj.ptr_child_nodes);
  point_id_first = obj.point_id_first;
  point_id_last = obj.point_id_last;

  max_n_point_per_cell_ = obj.max_n_point_per_cell_;
  max_tree_depth_ = obj.max_tree_depth_;
  tree_depth_ = obj.max_tree_depth_;
  crd_min_ = std::move(obj.crd_min_);
  crd_max_ = std::move(obj.crd_max_);

  ptr_point_data_set_ = obj.ptr_point_data_set_;

  crd_system_ = obj.crd_system_;
  active_dim_ = std::move(obj.active_dim_);
  n_leaf_ = obj.n_leaf_;
  n_node_ = obj.n_node_;
  output_ascii_precision_ = obj.output_ascii_precision_;

  // Reset obj
  obj.crd_system_ = "";
  for (size_t i = 0; i < obj.active_dim_.size(); ++i) {
    obj.active_dim_[i] = false;
  }
  obj.is_leaf = true;
  obj.node_depth = 0;
  obj.point_id_first = 0;
  obj.point_id_last = 0;
  obj.max_n_point_per_cell_ = 0;
  obj.max_tree_depth_ = 0;
  obj.tree_depth_ = 0;
  obj.ptr_point_data_set_ = nullptr;
  obj.n_leaf_ = 0;
  obj.n_node_ = 0;
  obj.output_ascii_precision_ = 0;
}
// ------------------------------------------------------------------------
//
// ------------------------------------------------------------------------
AMRGrid& AMRGrid::operator=(AMRGrid&& obj) {
  if (this != &obj) {
    this->Reset();
      is_leaf = obj.is_leaf;
      node_depth = obj.node_depth;
      center_crd = std::move(obj.center_crd);
      cell_half_width = std::move(obj.cell_half_width);
      ptr_child_nodes = std::move(obj.ptr_child_nodes);
      point_id_first = obj.point_id_first;
      point_id_last = obj.point_id_last;

      max_n_point_per_cell_ = obj.max_n_point_per_cell_;
      max_tree_depth_ = obj.max_tree_depth_;
      tree_depth_ = obj.max_tree_depth_;
      crd_min_ = std::move(obj.crd_min_);
      crd_max_ = std::move(obj.crd_max_);

      ptr_point_data_set_ = obj.ptr_point_data_set_;

      crd_system_ = obj.crd_system_;
      active_dim_ = std::move(obj.active_dim_);
      n_leaf_ = obj.n_leaf_;
      n_node_ = obj.n_node_;
      output_ascii_precision_ = obj.output_ascii_precision_;

      // Reset obj
      obj.crd_system_ = "";
      for (size_t i = 0; i < obj.active_dim_.size(); ++i) {
        obj.active_dim_[i] = false;
      }
      obj.is_leaf = true;
      obj.node_depth = 0;
      obj.point_id_first = 0;
      obj.point_id_last = 0;
      obj.max_n_point_per_cell_ = 0;
      obj.max_tree_depth_ = 0;
      obj.tree_depth_ = 0;
      obj.ptr_point_data_set_ = nullptr;
      obj.n_leaf_ = 0;
      obj.n_node_ = 0;
      obj.output_ascii_precision_ = 0;
  }
  return *this;
}
// ------------------------------------------------------------------------
//
// ------------------------------------------------------------------------
void AMRGrid::ResetGrid() {
  // Reset the Tree members
  this->Reset();
  // Reset the GridBase members
  this->crd_system_ = "cartesian";
  for (size_t i = 0; i < this->active_dim_.size(); ++i) {
    this->active_dim_[i] = false;
  }
  // Reset the AMRGrid members
  this->CountLeafs(*this);
  this->CountNodes(*this);
}
// ------------------------------------------------------------------------
//
// ------------------------------------------------------------------------
void AMRGrid::SetOutputPrecision(const int& precision) {
  this->output_ascii_precision_ = precision;
}
// ------------------------------------------------------------------------
//
// ------------------------------------------------------------------------
int AMRGrid::GetOutputPrecision() {
  return this->output_ascii_precision_;
}
// ------------------------------------------------------------------------
//
// ------------------------------------------------------------------------
void AMRGrid::CopyChildNodes(const TreeNode<double>& node_from,
                    TreeNode<double>* ptr_node_to) {
    ptr_node_to->is_leaf = node_from.is_leaf;
    int n_dim = static_cast<int>(node_from.center_crd.size());

    if (!node_from.is_leaf) {
      ptr_node_to->ptr_child_nodes.resize(node_from.ptr_child_nodes.size());
      for (size_t i = 0; i < node_from.ptr_child_nodes.size(); ++i) {
        ptr_node_to->ptr_child_nodes[i] = new TreeNode<double>(n_dim);
        *ptr_node_to->ptr_child_nodes[i] = *node_from.ptr_child_nodes[i];
        CopyChildNodes(*node_from.ptr_child_nodes[i],
                       ptr_node_to->ptr_child_nodes[i]);
      }
    }
}
// ------------------------------------------------------------------------
//
// ------------------------------------------------------------------------
std::string AMRGrid::GetGridType() { return std::string(grid_type_); }
// ------------------------------------------------------------------------
//
// ------------------------------------------------------------------------
void AMRGrid::BuildGrid(const int& max_n_point_per_cell,
                        const int& max_tree_depth,
                        const std::vector<double>& root_crd_min,
                        const std::vector<double>& root_crd_max,
                        const std::string& crd_system) {
  this->crd_system_ = crd_system;
  this->BuildTree(max_n_point_per_cell, max_tree_depth, root_crd_min,
                  root_crd_max);
  if (!this->is_leaf) {
    size_t n_dim = this->center_crd.size();
    for (size_t idim = 0; idim < n_dim; ++idim) {
      this->active_dim_[idim] = true;
    }
  }
  this->n_leaf_ = 0;
  CountLeafs(*this);
  this->n_node_ = 0;
  CountNodes(*this);
  this->VerifyGrid(this);
}
// ------------------------------------------------------------------------
//
// ------------------------------------------------------------------------
int AMRGrid::GetNCell(const int& iaxis) {
  return this->n_leaf_;
}
// ------------------------------------------------------------------------
//
// ------------------------------------------------------------------------
int AMRGrid::GetNCell() {
  return this->n_leaf_;
}
// ------------------------------------------------------------------------
//
// ------------------------------------------------------------------------
void AMRGrid::GetCellCenter(const int& iaxis, double* vector_double_out,
                            const int& dim) {
  int idx = 0;
  int n_dim = static_cast<int>(this->center_crd.size());
  if (iaxis >= n_dim) {
    throw std::runtime_error(
        "Attempted to fetch cell centers of dimension index " +
        std::to_string(iaxis) + " while the AMR grid has only " +
        std::to_string(n_dim) + " dimension.");
  }
  this->CollectCellCenters(iaxis, *this, vector_double_out, &idx);
}
// ------------------------------------------------------------------------
//
// ------------------------------------------------------------------------
void AMRGrid::SetCellCenter(const int& iaxis, double* vector_double_in,
                            const int& dim) {
  int idx = 0;
  int n_dim = static_cast<int>(this->center_crd.size());
  if (iaxis >= n_dim) {
    throw std::runtime_error(
        "Attempted to fetch cell centers of dimension index " +
        std::to_string(iaxis) + " while the AMR grid has only " +
        std::to_string(n_dim) + " dimension.");
  }
  this->DistributeCellCenters(iaxis, this, vector_double_in, &idx);
}
// ------------------------------------------------------------------------
//
// ------------------------------------------------------------------------
void AMRGrid::CollectCellCenters(const int& iaxis, const TreeNode<double>& node,
                                 double* cell_center_arr, int* ptr_idx) {
  if (node.is_leaf) {
    cell_center_arr[*ptr_idx] = node.center_crd[iaxis];
    (*ptr_idx) += 1;
  } else {
    for (size_t i = 0; i < node.ptr_child_nodes.size(); ++i) {
      this->CollectCellCenters(iaxis, *node.ptr_child_nodes[i], cell_center_arr,
                               ptr_idx);
    }
  }
}
// ------------------------------------------------------------------------
//
// ------------------------------------------------------------------------
void AMRGrid::DistributeCellCenters(const int& iaxis,
                                    TreeNode<double>* ptr_node,
                                    double* cell_center_arr, int* ptr_idx) {
  if (ptr_node->is_leaf) {
    ptr_node->center_crd[iaxis] = cell_center_arr[*ptr_idx];
    (*ptr_idx) += 1;
  } else {
    for (size_t i = 0; i < ptr_node->ptr_child_nodes.size(); ++i) {
      this->CollectCellCenters(iaxis, *ptr_node->ptr_child_nodes[i],
                               cell_center_arr, ptr_idx);
    }
  }
}
// ------------------------------------------------------------------------
//
// ------------------------------------------------------------------------
void AMRGrid::GetCellInterface(const int& iaxis, double* vector_double_out,
                               const int& dim) {
  int idx = 0;
  int n_dim = static_cast<int>(this->center_crd.size());
  if (iaxis >= n_dim) {
    throw std::runtime_error(
        "Attempted to fetch cell centers of dimension index " +
        std::to_string(iaxis) + " while the AMR grid has only " +
        std::to_string(n_dim) + " dimension.");
  }
  this->CollectCellIntefaces(iaxis, *this, vector_double_out, &idx);
}
// ------------------------------------------------------------------------
//
// ------------------------------------------------------------------------
void AMRGrid::CollectCellIntefaces(const int& iaxis,
                                   const TreeNode<double>& node,
                                   double* cell_interface_arr, int* ptr_idx) {
  if (node.is_leaf) {
    cell_interface_arr[0 + 2 * (*ptr_idx)] =
        node.center_crd[iaxis] - node.cell_half_width[iaxis];
    cell_interface_arr[1 + 2 * (*ptr_idx)] =
        node.center_crd[iaxis] + node.cell_half_width[iaxis];
    *ptr_idx += 1;
  } else {
    for (size_t i = 0; i < node.ptr_child_nodes.size(); ++i) {
      this->CollectCellIntefaces(iaxis, *node.ptr_child_nodes[i],
                                 cell_interface_arr, ptr_idx);
    }
  }
}
// ------------------------------------------------------------------------
//
// ------------------------------------------------------------------------
std::vector<TreeNode<double>*> AMRGrid::GetPointerToLeafNodes() {
  std::vector<TreeNode<double>*>ptr_to_leaf_nodes(n_leaf_);
  int idx = 0;
  CollectPointersToLeafNodes(this, &ptr_to_leaf_nodes, &idx);

  return ptr_to_leaf_nodes;
}
// ------------------------------------------------------------------------
//
// ------------------------------------------------------------------------
void AMRGrid::CollectPointersToLeafNodes(
    TreeNode<double>* ptr_node,
    std::vector<TreeNode<double>*>* ptr_to_tree_nodes, int* ptr_idx) {
  if (ptr_node->is_leaf) {
    (*ptr_to_tree_nodes)[*ptr_idx] = ptr_node;
    *ptr_idx += 1;
  } else {
    for (size_t i = 0; i < ptr_node->ptr_child_nodes.size(); ++i) {
      CollectPointersToLeafNodes(ptr_node->ptr_child_nodes[i],
                                 ptr_to_tree_nodes, ptr_idx);
    }
  }
}

// ------------------------------------------------------------------------
//
// ------------------------------------------------------------------------
void AMRGrid::SplitCell(TreeNode<double>* ptr_node) {
  if (!ptr_node->is_leaf) {
    throw std::runtime_error(
        "Somethign went wrong. Trying to resolve an AMR grid cell, which is "
        "not a leaf.");
  }

  int n_dim = static_cast<int>(center_crd.size());
  //
  // If there are more points than the threshold and the maximum
  // resolution level has not been reached yet split the cell
  //
  if (ptr_node->node_depth < max_tree_depth_) {
    ptr_node->is_leaf = false;
    n_leaf_ -= 1;
    if (tree_depth_ < (ptr_node->node_depth + 1)) {
      tree_depth_ = ptr_node->node_depth + 1;
    }
    //
    // 1D case
    //
    if (n_dim == 1) {
      //
      // Add child nodes
      //
      ptr_node->ptr_child_nodes.resize(2);
      for (int ichild = 0; ichild < 2; ++ichild) {
        ptr_node->ptr_child_nodes[ichild] = new TreeNode<double>(n_dim);
        ptr_node->ptr_child_nodes[ichild]->node_depth =
            ptr_node->node_depth + 1;
        for (int idim = 0; idim < n_dim; ++idim) {
          ptr_node->ptr_child_nodes[ichild]->cell_half_width[idim] =
              0.5 * cell_half_width[idim];
        }

        ptr_node->ptr_child_nodes[ichild]->node_id = n_node_ + 1;
        n_node_ += 1;
        n_leaf_ += 1;
      }

      ptr_node->ptr_child_nodes[0]->center_crd[0] =
          ptr_node->center_crd[0] - 0.5 * ptr_node->cell_half_width[0];
      ptr_node->ptr_child_nodes[1]->center_crd[0] =
          ptr_node->center_crd[0] + 0.5 * ptr_node->cell_half_width[0];



      //
      // 2D case
      //
    } else if (n_dim == 2) {
      ptr_node->ptr_child_nodes.resize(4);
      for (int ichild = 0; ichild < 4; ++ichild) {
        ptr_node->ptr_child_nodes[ichild] = new TreeNode<double>(n_dim);
        ptr_node->ptr_child_nodes[ichild]->node_depth =
            ptr_node->node_depth + 1;
        for (int idim = 0; idim < n_dim; ++idim) {
          ptr_node->ptr_child_nodes[ichild]->cell_half_width[idim] =
              0.5 * ptr_node->cell_half_width[idim];
        }
        ptr_node->ptr_child_nodes[ichild]->node_id = n_node_ + 1;
        n_node_ += 1;
        n_leaf_ += 1;
      }

      ptr_node->ptr_child_nodes[0]->center_crd[0] =
          ptr_node->center_crd[0] - 0.5 * ptr_node->cell_half_width[0];
      ptr_node->ptr_child_nodes[0]->center_crd[1] =
          ptr_node->center_crd[1] - 0.5 * ptr_node->cell_half_width[1];

      ptr_node->ptr_child_nodes[1]->center_crd[0] =
          ptr_node->center_crd[0] + 0.5 * ptr_node->cell_half_width[0];
      ptr_node->ptr_child_nodes[1]->center_crd[1] =
          ptr_node->center_crd[1] - 0.5 * ptr_node->cell_half_width[1];

      ptr_node->ptr_child_nodes[2]->center_crd[0] =
          ptr_node->center_crd[0] - 0.5 * ptr_node->cell_half_width[0];
      ptr_node->ptr_child_nodes[2]->center_crd[1] =
          ptr_node->center_crd[1] + 0.5 * ptr_node->cell_half_width[1];

      ptr_node->ptr_child_nodes[3]->center_crd[0] =
          ptr_node->center_crd[0] + 0.5 * ptr_node->cell_half_width[0];
      ptr_node->ptr_child_nodes[3]->center_crd[1] =
          ptr_node->center_crd[1] + 0.5 * ptr_node->cell_half_width[1];

      //
      // 3D case
      //
    } else if (n_dim == 3) {
      ptr_node->ptr_child_nodes.resize(8);
      for (int ichild = 0; ichild < 8; ++ichild) {
        ptr_node->ptr_child_nodes[ichild] = new TreeNode<double>(n_dim);
        ptr_node->ptr_child_nodes[ichild]->node_depth =
            ptr_node->node_depth + 1;
        for (int idim = 0; idim < n_dim; ++idim) {
          ptr_node->ptr_child_nodes[ichild]->cell_half_width[idim] =
              0.5 * ptr_node->cell_half_width[idim];
        }
        ptr_node->ptr_child_nodes[ichild]->node_id = n_node_ + 1;
        n_node_ += 1;
        n_leaf_ += 1;
      }
      ptr_node->ptr_child_nodes[0]->center_crd[0] =
          ptr_node->center_crd[0] - 0.5 * ptr_node->cell_half_width[0];
      ptr_node->ptr_child_nodes[0]->center_crd[1] =
          ptr_node->center_crd[1] - 0.5 * ptr_node->cell_half_width[1];
      ptr_node->ptr_child_nodes[0]->center_crd[2] =
          ptr_node->center_crd[2] - 0.5 * ptr_node->cell_half_width[2];

      ptr_node->ptr_child_nodes[1]->center_crd[0] =
          ptr_node->center_crd[0] + 0.5 * ptr_node->cell_half_width[0];
      ptr_node->ptr_child_nodes[1]->center_crd[1] =
          ptr_node->center_crd[1] - 0.5 * ptr_node->cell_half_width[1];
      ptr_node->ptr_child_nodes[1]->center_crd[2] =
          ptr_node->center_crd[2] - 0.5 * ptr_node->cell_half_width[2];

      ptr_node->ptr_child_nodes[2]->center_crd[0] =
          ptr_node->center_crd[0] - 0.5 * ptr_node->cell_half_width[0];
      ptr_node->ptr_child_nodes[2]->center_crd[1] =
          ptr_node->center_crd[1] + 0.5 * ptr_node->cell_half_width[1];
      ptr_node->ptr_child_nodes[2]->center_crd[2] =
          ptr_node->center_crd[2] - 0.5 * ptr_node->cell_half_width[2];

      ptr_node->ptr_child_nodes[3]->center_crd[0] =
          ptr_node->center_crd[0] + 0.5 * ptr_node->cell_half_width[0];
      ptr_node->ptr_child_nodes[3]->center_crd[1] =
          ptr_node->center_crd[1] + 0.5 * ptr_node->cell_half_width[1];
      ptr_node->ptr_child_nodes[3]->center_crd[2] =
          ptr_node->center_crd[2] - 0.5 * ptr_node->cell_half_width[2];

      ptr_node->ptr_child_nodes[4]->center_crd[0] =
          ptr_node->center_crd[0] - 0.5 * ptr_node->cell_half_width[0];
      ptr_node->ptr_child_nodes[4]->center_crd[1] =
          ptr_node->center_crd[1] - 0.5 * ptr_node->cell_half_width[1];
      ptr_node->ptr_child_nodes[4]->center_crd[2] =
          ptr_node->center_crd[2] + 0.5 * ptr_node->cell_half_width[2];

      ptr_node->ptr_child_nodes[5]->center_crd[0] =
          ptr_node->center_crd[0] + 0.5 * ptr_node->cell_half_width[0];
      ptr_node->ptr_child_nodes[5]->center_crd[1] =
          ptr_node->center_crd[1] - 0.5 * ptr_node->cell_half_width[1];
      ptr_node->ptr_child_nodes[5]->center_crd[2] =
          ptr_node->center_crd[2] + 0.5 * ptr_node->cell_half_width[2];

      ptr_node->ptr_child_nodes[6]->center_crd[0] =
          ptr_node->center_crd[0] - 0.5 * ptr_node->cell_half_width[0];
      ptr_node->ptr_child_nodes[6]->center_crd[1] =
          ptr_node->center_crd[1] + 0.5 * ptr_node->cell_half_width[1];
      ptr_node->ptr_child_nodes[6]->center_crd[2] =
          ptr_node->center_crd[2] + 0.5 * ptr_node->cell_half_width[2];

      ptr_node->ptr_child_nodes[7]->center_crd[0] =
          ptr_node->center_crd[0] + 0.5 * ptr_node->cell_half_width[0];
      ptr_node->ptr_child_nodes[7]->center_crd[1] =
          ptr_node->center_crd[1] + 0.5 * ptr_node->cell_half_width[1];
      ptr_node->ptr_child_nodes[7]->center_crd[2] =
          ptr_node->center_crd[2] + 0.5 * ptr_node->cell_half_width[2];
    }
  }
}
// ------------------------------------------------------------------------
//
// ------------------------------------------------------------------------
void AMRGrid::WriteToFile(const std::string& file_name,
                          const std::string& format) {
  std::cout << "Writing " << file_name << "...";
  if (format == "radmc3d") {
    std::ofstream out_stream;
    // Open the stream
    try {
      out_stream.open(file_name, std::ofstream::out | std::ofstream::trunc);
    } catch (std::exception &e) {
      std::cout << e.what() << std::endl;
    }
    // iformat
    out_stream << "1\n";  // iformat
    out_stream << "\n";
    // Grid style (0 - Regular, 1 - OcTree, 10 - Nested)
    out_stream << "1\n";
    // Coordinate system (0-99 Cartesian, 100-199 - Spherical)
    if (crd_system_ == "cartesian") {
      out_stream << "0\n";
    } else if (crd_system_ == "spherical") {
      out_stream << "100\n";
    } else {
      throw std::runtime_error("Unknown coordiante system : " + crd_system_ +
                               " encountered when writing AMR grid to "
                               "file. Only cartesian and spherical coordinate "
                               "systems are implemented.");
    }
    // Gridinfo
    out_stream << "0\n";
    out_stream << "\n";
    // Active dimensions
    for (int i = 0; i < 3; ++i) {
      out_stream << active_dim_[i] << " ";
    }
    out_stream << "\n";
    // Number of base grid cells in each dimension
    out_stream << "1 1 1\n";
    out_stream << "\n";
    // Max. refinement level, number of leaf cells, total number of nodes (leaf
    // + branch)
    out_stream << tree_depth_ + 1 << " " << n_leaf_ << " " << n_node_ << "\n";
    out_stream << "\n";

    for (int i = 0; i < 3; ++i) {
      out_stream << std::fixed << std::scientific << std::setprecision(16)
                 << center_crd[i] - cell_half_width[i] << "\n";
      out_stream << std::fixed << std::scientific << std::setprecision(16)
                 << center_crd[i] + cell_half_width[i] << "\n";
      out_stream << "\n";
    }
    out_stream << "\n";

    WriteCellToFile(&out_stream, *this);

    // Close the stream
    out_stream.close();
  } else {
    throw std::runtime_error("Unknown format " + format +
                             "\n Currently only 'radmc3d' is supported");
  }
  std::cout << "Done" << std::endl;
}
// ------------------------------------------------------------------------
//
// ------------------------------------------------------------------------
void AMRGrid::ReadFromFile(const std::string& file_name,
                           const std::string& format) {
  if (format == "radmc3d") {
    std::fstream input_stream;
    // Open the input stream
    try {
      input_stream.open(file_name, std::ifstream::in);
    } catch (std::exception &e) {
      std::cout << e.what() << std::endl;
    }
    std::cout << "Reading " << file_name << std::endl;
    // Reset the tree
    this->Reset();

    int iformat;
    input_stream >> iformat;

    if (iformat != 1) {
      throw std::runtime_error(std::string("Unknown file format ID of ") +
                               std::to_string(iformat) +
                               std::string(" in ") +
                               std::string(file_name));
    }

    int grid_style = -1;
    input_stream >> grid_style;
    if (grid_style == 1) {
      int crd_system_int = -1;
      input_stream >> crd_system_int;
      if (crd_system_int < 100) {
        crd_system_ = "cartesian";
      } else if (crd_system_int < 200) {
        crd_system_ = "spherical";
      } else {
        throw std::runtime_error(
            "Unknown coordinate system (" + std::to_string(crd_system_int) +
            ") encountered when reading AMR grid from file. Only cartesian "
            "(0-99) and spherical (100-199) coordinate systems are "
            "implemented.");
      }
      int grid_info;
      input_stream >> grid_info;
      active_dim_.resize(3);
      // Active dimensions
      int act_dim[3];
      input_stream >> act_dim[0] >> act_dim[1] >> act_dim[2];
      for (int i = 0; i < 3; ++i) {
        if (act_dim[i] == 0) {
          active_dim_[i] = false;
        } else {
          active_dim_[i] = true;
        }
      }
      // Number of base grid cells(!) in each dimension
      std::string dummy_str;
      input_stream >> dummy_str;
      input_stream >> dummy_str;
      input_stream >> dummy_str;
      input_stream >> tree_depth_ >> n_leaf_ >> n_node_;

      ptr_point_data_set_ = nullptr;
      max_n_point_per_cell_ = 0;
      max_tree_depth_ = tree_depth_;
      crd_min_.resize(3, 0.0);
      crd_max_.resize(3, 0.0);

      node_depth = 0;
      center_crd.resize(3, 0.0);
      cell_half_width.resize(3, 0.0);
      point_id_first = 0;
      point_id_last = 0;
      // Read the base grid / root node
      for (int i = 0; i < 3; ++i) {
        input_stream >> crd_min_[i];
        input_stream >> crd_max_[i];
        center_crd[i] = 0.5 * (crd_min_[i] + crd_max_[i]);
        cell_half_width[i] = 0.5 * (crd_max_[i] - crd_min_[i]);
      }

      ReadCellFromFile(&input_stream, this);

      this->n_leaf_ = 0;
      CountLeafs(*this);
      this->n_node_ = 0;
      CountNodes(*this);


    } else if (grid_style == 0) {
      throw std::runtime_error(
          "Wrong grid style. " + file_name +
          " Regular grid cannot be read into an octree AMR mesh container.");
    } else if (grid_style == 10) {
      throw std::runtime_error(
          "Wrong grid style. " + file_name +
          " nested mesh cannot be read into a regular grid container.");
    } else {
      throw std::runtime_error("Unknown grid style id of " +
                               std::to_string(grid_style) +
                               ". Allowed grid styles are : 0 - regular grid, "
                               "1 - octree, 10 - nested "
                               "mesh");
    }
    input_stream.close();
  } else {
    throw std::runtime_error("Unknown format " + format +
                             "\n Currently only 'radmc3d' is supported");
  }
}
// ----------------------------------------------------------------------
//
// ----------------------------------------------------------------------
void AMRGrid::WriteCellToFile(std::ofstream* ptr_out_stream,
                              const TreeNode<double>& node) {
  if (node.is_leaf) {
    *ptr_out_stream << "0\n";
  } else {
    *ptr_out_stream << "1\n";
    for (size_t i = 0; i < node.ptr_child_nodes.size(); ++i) {
      WriteCellToFile(ptr_out_stream, *node.ptr_child_nodes[i]);
    }
  }
}
// ----------------------------------------------------------------------
//
// ----------------------------------------------------------------------
void AMRGrid::ReadCellFromFile(std::fstream* input_stream,
                               TreeNode<double>* ptr_node) {
  int resolve_node = -1;
  *input_stream >> resolve_node;

  int n_dim = static_cast<int>(center_crd.size());

  if (resolve_node == 0) {
    ptr_node->is_leaf = true;
  } else {
    ptr_node->is_leaf = false;

    ptr_node->ptr_child_nodes.resize(8);
    for (size_t ichild = 0; ichild < ptr_node->ptr_child_nodes.size();
         ++ichild) {
      ptr_node->ptr_child_nodes[ichild] = new TreeNode<double>(n_dim);
      ptr_node->ptr_child_nodes[ichild]->is_leaf = true;
      ptr_node->ptr_child_nodes[ichild]->node_depth = ptr_node->node_depth + 1;
      for (int idim = 0; idim < n_dim; ++idim) {
        ptr_node->ptr_child_nodes[ichild]->cell_half_width[idim] =
            0.5 * ptr_node->cell_half_width[idim];
      }
      ptr_node->ptr_child_nodes[ichild]->point_id_first = 0;
      ptr_node->ptr_child_nodes[ichild]->point_id_last = 0;
    }

    ptr_node->ptr_child_nodes[0]->center_crd[0] =
        ptr_node->center_crd[0] - 0.5 * ptr_node->cell_half_width[0];
    ptr_node->ptr_child_nodes[0]->center_crd[1] =
        ptr_node->center_crd[1] + 0.5 * ptr_node->cell_half_width[1];
    ptr_node->ptr_child_nodes[0]->center_crd[2] =
        ptr_node->center_crd[2] + 0.5 * ptr_node->cell_half_width[2];

    ptr_node->ptr_child_nodes[1]->center_crd[0] =
        ptr_node->center_crd[0] + 0.5 * ptr_node->cell_half_width[0];
    ptr_node->ptr_child_nodes[1]->center_crd[1] =
        ptr_node->center_crd[1] + 0.5 * ptr_node->cell_half_width[1];
    ptr_node->ptr_child_nodes[1]->center_crd[2] =
        ptr_node->center_crd[2] + 0.5 * ptr_node->cell_half_width[2];

    ptr_node->ptr_child_nodes[2]->center_crd[0] =
        ptr_node->center_crd[0] - 0.5 * ptr_node->cell_half_width[0];
    ptr_node->ptr_child_nodes[2]->center_crd[1] =
        ptr_node->center_crd[1] - 0.5 * ptr_node->cell_half_width[1];
    ptr_node->ptr_child_nodes[2]->center_crd[2] =
        ptr_node->center_crd[2] + 0.5 * ptr_node->cell_half_width[2];

    ptr_node->ptr_child_nodes[3]->center_crd[0] =
        ptr_node->center_crd[0] + 0.5 * ptr_node->cell_half_width[0];
    ptr_node->ptr_child_nodes[3]->center_crd[1] =
        ptr_node->center_crd[1] - 0.5 * ptr_node->cell_half_width[1];
    ptr_node->ptr_child_nodes[3]->center_crd[2] =
        ptr_node->center_crd[2] + 0.5 * ptr_node->cell_half_width[2];

    ptr_node->ptr_child_nodes[4]->center_crd[0] =
        ptr_node->center_crd[0] - 0.5 * ptr_node->cell_half_width[0];
    ptr_node->ptr_child_nodes[4]->center_crd[1] =
        ptr_node->center_crd[1] + 0.5 * ptr_node->cell_half_width[1];
    ptr_node->ptr_child_nodes[4]->center_crd[2] =
        ptr_node->center_crd[2] - 0.5 * ptr_node->cell_half_width[2];

    ptr_node->ptr_child_nodes[5]->center_crd[0] =
        ptr_node->center_crd[0] + 0.5 * ptr_node->cell_half_width[0];
    ptr_node->ptr_child_nodes[5]->center_crd[1] =
        ptr_node->center_crd[1] + 0.5 * ptr_node->cell_half_width[1];
    ptr_node->ptr_child_nodes[5]->center_crd[2] =
        ptr_node->center_crd[2] - 0.5 * ptr_node->cell_half_width[2];

    ptr_node->ptr_child_nodes[6]->center_crd[0] =
        ptr_node->center_crd[0] - 0.5 * ptr_node->cell_half_width[0];
    ptr_node->ptr_child_nodes[6]->center_crd[1] =
        ptr_node->center_crd[1] - 0.5 * ptr_node->cell_half_width[1];
    ptr_node->ptr_child_nodes[6]->center_crd[2] =
        ptr_node->center_crd[2] - 0.5 * ptr_node->cell_half_width[2];

    ptr_node->ptr_child_nodes[7]->center_crd[0] =
        ptr_node->center_crd[0] + 0.5 * ptr_node->cell_half_width[0];
    ptr_node->ptr_child_nodes[7]->center_crd[1] =
        ptr_node->center_crd[1] - 0.5 * ptr_node->cell_half_width[1];
    ptr_node->ptr_child_nodes[7]->center_crd[2] =
        ptr_node->center_crd[2] - 0.5 * ptr_node->cell_half_width[2];

    for (size_t ichild = 0; ichild < ptr_node->ptr_child_nodes.size();
         ++ichild) {
      this->ReadCellFromFile(input_stream, ptr_node->ptr_child_nodes[ichild]);
    }
  }
}
// ------------------------------------------------------------------------
//
// ------------------------------------------------------------------------
void AMRGrid::VerifyGrid(TreeNode<double>* ptr_node) {
  if (!ptr_node->is_leaf) {
    for (size_t i = 0; i < ptr_node->ptr_child_nodes.size(); ++i) {
      VerifyGrid(ptr_node->ptr_child_nodes[i]);
    }
  } else {
    int n_dim = static_cast<int>(ptr_node->center_crd.size());
    for (int idim = 0; idim < n_dim; ++idim) {
      //
      // Check the node cell size
      //
      double q = 0.5 * (crd_max_[idim] - crd_min_[idim]) /
                 ptr_node->cell_half_width[idim];
      q = log(q)/log(2);
      if (q != ptr_node->node_depth) {
        throw std::runtime_error(
            "Tree cell size inconsistency : node depth from cell size : " +
            std::to_string(q) +
            "node depth : " + std::to_string(ptr_node->node_depth));
      }
    }
  }
}
