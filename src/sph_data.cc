// ============================================================================
// sph_data.cc
//
// This file is part of sphtool:
// Gridding and visualisation package for SPH simulations
//
// Copyright (C) 2018 Attila Juhasz
// Author: Attila Juhasz
//
// sphtool is free software; you can redistribute it and/or modify it under
// the terms of the GNU General Public License as published by the Free Software
// Foundation; either version 2 of the License, or (at your option) any later
// version.
//
// sphtool is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.
// ============================================================================

#include <cmath>
#include "omp.h"
#include "../include/sph_data.h"
#include "../include/kernels.h"
// ------------------------------------------------------------------------
//
// ------------------------------------------------------------------------
SPHData::SPHData() {
  ptr_smoothing_kernel_ = new CubicSplineKernel(3);
}
// ------------------------------------------------------------------------
//
// ------------------------------------------------------------------------
SPHData::SPHData(const int& n_point, const int& n_dim)
    : PointDataSet(n_point, n_dim) {
  ptr_smoothing_kernel_ = new CubicSplineKernel(n_dim);
}
// ------------------------------------------------------------------------
//
// ------------------------------------------------------------------------
SPHData::~SPHData() {
  if (ptr_smoothing_kernel_) {
    delete ptr_smoothing_kernel_;
  }
}
// ------------------------------------------------------------------------
//
// ------------------------------------------------------------------------
void SPHData::validate() {
  PointDataSet::validate();

  int index = -1;
  index = GetScalarIndex("density");
  if (index < 0) {
    throw std::runtime_error(
        "Scalar variable 'density' is not defined. It is required for SPH "
        "interpolation.");
  }

  index = GetScalarIndex("particle_mass");
  if (index < 0) {
    throw std::runtime_error(
        "Scalar variable 'particle_mass' is not defined. It is required for "
        "SPH interpolation.");
  }

  index = GetScalarIndex("smoothing_length");
  if (index < 0) {
    throw std::runtime_error(
        "Scalar variable 'smoothing_length' is not defined. It is required for "
        "SPH interpolation.");
  }

  // TODO(juhasz): Change the data type of PointData.crd to std::vector and
  // implement a test here to check if the crd has already been set...
}

// ------------------------------------------------------------------------
//
// ------------------------------------------------------------------------
//void SPHData::ReGridOld(const std::vector<std::string>& var_name,
//                     const int& max_particle_per_cell,
//                     const int& max_tree_depth, const int& n_thread,
//                     const std::string& grid_type,
//                     PointDataSet* ptr_grid_set) {
//  //
//  // Generate a tree over the regular grid set
//  //
//
//  Tree<double> grid_tree(ptr_grid_set);
//  std::cout << "Building a tree...";
//  grid_tree.BuildTree(max_particle_per_cell, max_tree_depth);
//  std::cout << "Done" << std::endl;
//
//  //
//  // Find the scalar indices for the smoothing length, particle mass and density
//  //  required for the SPH interpolation
//  //
//  int ind_h = GetScalarIndex("smoothing_length");
//  int ind_pmass = GetScalarIndex("particle_mass");
//  int ind_density = GetScalarIndex("density");
//
//  //
//  // Separate vectors and scalars and get their indices
//  //
//  std::vector<int> scalar_index;
//  std::vector<int> vector_index;
//  std::vector<std::string> scalar_names;
//  std::vector<std::string> vector_names;
//
//  for (size_t ivar=0; ivar < var_name.size(); ++ivar) {
//    if (is_var_scalar(var_name[ivar])) {
//      scalar_index.push_back(GetScalarIndex(var_name[ivar]));
//      scalar_names.push_back(var_name[ivar]);
//    } else {
//      vector_index.push_back(GetVectorIndex(var_name[ivar]));
//      vector_names.push_back(var_name[ivar]);
//    }
//  }
//  int n_scalar = scalar_index.size();
//  int n_vector = vector_index.size();
//  //
//  // Reset the scalars and vectors in the container
//  //
//  {
//    int n_scalar_old = ptr_grid_set->get_nscalar();
//    for (int i = 0; i < n_scalar_old; ++i) {
//      std::string scalar_name = ptr_grid_set->GetScalarName(i);
//      ptr_grid_set->delete_scalar(scalar_name);
//    }
//  }
//  {
//    int n_vector_old = ptr_grid_set->get_nvector();
//    for (int i = 0; i < n_vector_old; ++i) {
//      std::string vector_name = ptr_grid_set->GetScalarName(i);
//      ptr_grid_set->delete_vector(vector_name);
//    }
//  }
//  //
//  // Set the new variables in the container
//  //
//  ptr_grid_set->_set_nscalar(n_scalar, scalar_names);
//  ptr_grid_set->_set_nvector(n_vector, vector_names);
//  //
//  // Loop over all SPH points and find the indices of all regular grid cells
//  //  the SPH point contributes to
//  //
//#pragma omp parallel num_threads(n_thread)
//  {
//    double dist = 0.0;
//    double weight = 0.0;
//    double kernel = 0.0;
//    std::vector<int> point_id;
//
//#pragma omp for schedule(static)
//    // Loop over the SPH points
//    for (int ip = 0; ip < n_point_; ++ip) {
//      point_id.resize(0);
//
//      grid_tree.GetPointIDsWithinDistance(ptr_points_[ip]->crd.data(),
//                                     ptr_points_[ip]->scalars[ind_h] * 2.0,
//                                     &point_id);
//
//      // Loop over the overlapping cell center indices
//      for (size_t i = 0; i < point_id.size(); ++i) {
//        dist = 0.0;
//        for (int idim = 0; idim < n_dim_; ++idim) {
//          dist += (ptr_grid_set->ptr_points_[point_id[i]]->crd[idim] -
//                  ptr_points_[ip]->crd[idim]) *
//                  (ptr_grid_set->ptr_points_[point_id[i]]->crd[idim] -
//                  ptr_points_[ip]->crd[idim]);
//        }
//        dist = sqrt(dist) / ptr_points_[ip]->scalars[ind_h];
//
//        if (dist <= 2.0) {
//          // Smoothing kernel
//          kernel = ptr_smoothing_kernel_->W(dist);
//          // Dimensionless weight for the SPH interpolation
//          weight = ptr_points_[ip]->scalars[ind_pmass] /
//                   ptr_points_[ip]->scalars[ind_density] /
//                   (ptr_points_[ip]->scalars[ind_h] *
//                    ptr_points_[ip]->scalars[ind_h] *
//                    ptr_points_[ip]->scalars[ind_h]);
//
//          for (int is = 0; is < n_scalar; ++is) {
//            ptr_grid_set->ptr_points_[point_id[i]]->scalars[is] +=
//                ptr_points_[ip]->scalars[scalar_index[is]] * weight * kernel;
//          }
//          for (int iv = 0; iv < n_vector; ++iv) {
//            for (int idim = 0; idim < n_dim_; ++idim) {
//              ptr_grid_set->ptr_points_[point_id[i]]
//                  ->vectors[idim + iv * n_dim_] +=
//                  ptr_points_[ip]->vectors[idim + vector_index[iv] * n_dim_] *
//                  weight * kernel;
//            }
//          }
//        }
//      }
//    }
//  }
//}
// ------------------------------------------------------------------------
//
// ------------------------------------------------------------------------
void SPHData::ReGrid(const std::vector<std::string>& var_name,
                     const int& max_particle_per_cell,
                     const int& max_tree_depth, const int& n_thread,
                     const std::string& grid_type,
                     PointDataSet* ptr_grid_set) {
  int n_point = ptr_grid_set->get_npoint();
  int n_dim   = ptr_grid_set->get_ndim();
  std::vector<double> xc(n_point, 0.0);
  std::vector<double> yc(n_point, 0.0);
  std::vector<double> zc(n_point, 0.0);
  {
    std::vector<double> crd(n_point * n_dim, 0.0);
    ptr_grid_set->_get_vector("crd", crd.data(), n_point * n_dim, n_dim);
    for (int ip = 0; ip < n_point; ++ip) {
      xc[ip] = crd[0 + n_dim * ip];
      yc[ip] = crd[1 + n_dim * ip];
      zc[ip] = crd[2 + n_dim * ip];
    }
  }

  for (size_t ivar = 0; ivar < var_name.size(); ++ivar) {
    if (is_var_scalar(var_name[ivar])) {
      std::vector<double> gridded_var(n_point, 0.0);
      InterpolateSingleVar(var_name[ivar], xc.data(), yc.data(), zc.data(),
                           n_point, n_thread, gridded_var.data());
      ptr_grid_set->_set_scalar(var_name[ivar], gridded_var.data(), n_point);
    } else {
      std::vector<double> gridded_var(n_point * n_dim, 0.0);
      InterpolateSingleVar(var_name[ivar], xc.data(), yc.data(), zc.data(),
                           n_point, n_thread, gridded_var.data());
      ptr_grid_set->_set_vector(var_name[ivar], gridded_var.data(),
                                n_point * n_dim, n_dim);
    }
  }
}
// ------------------------------------------------------------------------
//
// ------------------------------------------------------------------------
void SPHData::InitTree(const int& max_particle_per_cell,
                       const int& max_tree_depth) {
  std::cout << "Building a tree...";
  tree_.SetPointData(this);
  tree_.BuildTree(max_particle_per_cell, max_tree_depth);
  int ind_h = GetScalarIndex("smoothing_length");
  tree_.CalculateBBoxCoordinates(&tree_, ind_h);
  std::cout << "Done" << std::endl;
}
// ------------------------------------------------------------------------
//
// ------------------------------------------------------------------------
void SPHData::InterpolateSingleVar(std::string var_name, double* xi,
                                   double* yi, double* zi,
                                   const int& n_point_int, const int& n_thread,
                                   double* var_out) {
  //
  // Find the scalar indices for the smoothing length, particle mass and density
  //  required for the SPH interpolation
  //
  int ind_h = GetScalarIndex("smoothing_length");
  int ind_pmass = GetScalarIndex("particle_mass");
  int ind_density = GetScalarIndex("density");
  int var_index = GetScalarIndex(var_name);
  bool is_scalar = true;
  if (var_index < 0) {
    var_index = GetVectorIndex(var_name);
    is_scalar = false;
  }
  std::vector<double> crd(3, 0.0);

#pragma omp parallel for firstprivate(crd) schedule(static) \
    num_threads(n_thread)
  for (int ip = 0; ip < n_point_int; ++ip) {
    crd[0] = xi[ip];
    crd[1] = yi[ip];
    crd[2] = zi[ip];
    if (is_scalar) {
      var_out[ip] = 0.0;
    } else {
      for (int idim = 0; idim < n_dim_; ++idim) {
        var_out[idim + ip * n_dim_] = 0.0;
      }
    }
    //
    // Get the overlapping particle indices
    //
    std::vector<int> bind;
    tree_.GetOverlappingParticleIndices(crd, &this->tree_, &bind);
    //
    // Loop through particles and get their contribution
    //
    for (size_t ipart = 0; ipart < bind.size(); ++ipart) {
      double dist = 0.0;
      double weight = 0.0;
      double kernel = 0.0;

      int pid = bind[ipart];
      for (int idim = 0; idim < n_dim_; idim++) {
        dist += ((crd[idim] - ptr_points_[pid]->crd[idim]) *
                 (crd[idim] - ptr_points_[pid]->crd[idim]));
      }
      dist = sqrt(dist) / ptr_points_[pid]->scalars[ind_h];
      if (dist <= 2.0) {
        // Smoothing kernel
        kernel = ptr_smoothing_kernel_->W(dist);
        // Dimensionless weight for the SPH interpolation
        weight = ptr_points_[pid]->scalars[ind_pmass] /
                 ptr_points_[pid]->scalars[ind_density] /
                 (ptr_points_[pid]->scalars[ind_h] *
                  ptr_points_[pid]->scalars[ind_h] *
                  ptr_points_[pid]->scalars[ind_h]);

        if (is_scalar) {
          var_out[ip] += ptr_points_[pid]->scalars[var_index] * weight * kernel;
        } else {
          for (int idim = 0; idim < n_dim_; ++idim) {
            var_out[idim + ip * n_dim_] +=
                ptr_points_[pid]->vectors[idim + var_index * n_dim_] * weight *
                kernel;
          }
        }
      }
    }
  }
}
// ------------------------------------------------------------------------
//
// ------------------------------------------------------------------------
void SPHData::ProjectScalarToPlane(const std::string& scalar_name,
                                   const std::vector<double>& xcent,
                                   const std::vector<double>& ycent,
                                   const double& incl_rad,
                                   const double& posang_rad,
                                   const double& phi_rad, const int& n_thread,
                                   double* projected_scalar) {
  size_t npix = xcent.size();
  size_t npix2 = npix * npix;
  //
  // Initialize the projected scalar array with zeros
  //
  for (size_t i = 0; i < npix2; ++i) {
    projected_scalar[i] = 0.0;
  }
  //
  // Find the scalar indices for the smoothing length, particle mass and density
  //  required for the SPH interpolation
  //
  int ind_h = GetScalarIndex("smoothing_length");
  int ind_pmass = GetScalarIndex("particle_mass");
  int ind_density = GetScalarIndex("density");
  int ind_var = GetScalarIndex(scalar_name);

  if (ind_var >= 0) {
    // Number of pixels (assuming square image!!)
    int n_pixel = static_cast<int>(xcent.size());
    // Image size (distance between the centers of the first and the last
    // pixels)
    double image_size = xcent[n_pixel - 1] - xcent[0];
    // Center of the leftmost pixel horizontally
    double image_xmin = xcent[0];
    // Center of the bottom pixel vertically
    double image_ymin = ycent[0];
    // Half of the pixel size (i.e. pixel center to pixel edge)
    double pixel_half_width = (xcent[1] - xcent[0]) * 0.5;

    // Precompute some trigonometric functions
    double cos_phi = std::cos(phi_rad);
    double sin_phi = std::sin(phi_rad);
    double cos_incl = std::cos(incl_rad);
    double sin_incl = std::sin(incl_rad);
    double cos_posang = std::cos(posang_rad);
    double sin_posang = std::sin(posang_rad);

#pragma omp parallel for schedule(static) num_threads(n_thread)
    // Loop through all SPH particles
    for (int ip = 0; ip < n_point_; ++ip) {
      //
      // Rotate the points with phi around the z axis
      //
      double rot_x =
          ptr_points_[ip]->crd[0] * cos_phi + ptr_points_[ip]->crd[1] * sin_phi;
      double rot_y = -ptr_points_[ip]->crd[0] * sin_phi +
                     ptr_points_[ip]->crd[1] * cos_phi;
      double rot_z = ptr_points_[ip]->crd[2];

      //
      // Rotate the points with Incl around the X axis
      // Note that the Z coordinate transformation was neglected because we
      // won't need it. We are only interested in the X and Y coordinates in the
      // final rotated point distribution
      //
      double dummy_x = rot_x * cos_incl + rot_z * sin_incl;
      double dummy_y = rot_y;
      //
      // Rotate the points with Posang around the Z axis
      //
      rot_x = dummy_x * cos_posang + dummy_y * sin_posang;
      rot_y = -dummy_x * sin_posang + dummy_y * cos_posang;
      //
      // Get the indices of the pixel the current particle falls into
      //
      int ix = static_cast<int>(
          std::round((rot_x - image_xmin) / (image_size)*n_pixel));
      int iy = static_cast<int>(
          std::round((rot_y - image_ymin) / (image_size)*n_pixel));
      //
      // Now check which pixels it will contribute to
      //
      double dummy_h =
          std::max(ptr_points_[ip]->scalars[ind_h], pixel_half_width);
      int two_h_per_pixel =
          static_cast<int>(std::ceil(2.0 * dummy_h / (2.0 * pixel_half_width)));
      //
      // Get the indices of the pixels the particle may have a contribution to
      //
      int ix_min = std::max(ix - two_h_per_pixel, 0);
      int ix_max = std::min(ix + two_h_per_pixel + 1, n_pixel);
      int iy_min = std::max(iy - two_h_per_pixel, 0);
      int iy_max = std::min(iy + two_h_per_pixel + 1, n_pixel);

      double weighted_scalar = ptr_points_[ip]->scalars[ind_var] *
                               ptr_points_[ip]->scalars[ind_pmass] /
                               ptr_points_[ip]->scalars[ind_density] /
                               (dummy_h * dummy_h);

      for (int ix = ix_min; ix < ix_max; ++ix) {
        for (int iy = iy_min; iy < iy_max; ++iy) {
          //
          // Get the distance between the ray and the particle
          //
          double distance_ray_to_particle =
              std::sqrt((rot_x - xcent[ix]) * (rot_x - xcent[ix]) +
                        (rot_y - ycent[iy]) * (rot_y - ycent[iy])) /
              dummy_h;
          //
          // If the particle is within 2h from the ray add its contribution to
          // the pixel
          //

          if (distance_ray_to_particle <= 2.0) {
#pragma omp atomic
            projected_scalar[ix + iy * n_pixel] +=
                weighted_scalar *
                ptr_smoothing_kernel_->GetWIntegral(distance_ray_to_particle);
          }
        }
      }
    }
  }
}
// ------------------------------------------------------------------------
//
// ------------------------------------------------------------------------
void SPHData::GetVarSlice(const std::string& var_name,
                         const double& slice_xcent,
                         const double& slice_ycent,
                         const double& slice_zcent,
                         const int& n_pixel_x,
                         const int& n_pixel_y,
                         const double& delta_x,
                         const double& delta_y,
                         const double& incl_rad,
                         const double& posang_rad,
                         const double& phi_rad,
                         const int& n_thread,
                         double* var_slice) {
  //
  // Check if we need to calculate a scalar or a vector slice
  //
  int ind_var = -1;
  bool is_scalar = is_var_scalar(var_name);
  if (is_var_scalar(var_name)) {
    ind_var = GetScalarIndex(var_name);
  } else {
    ind_var = GetVectorIndex(var_name);
  }

  size_t slice_size = static_cast<size_t>(n_pixel_x * n_pixel_y);
  if (!is_scalar) {
    slice_size *= 2;
  }
  //
  // Initialize the slice array with zeros
  //
  for (size_t i = 0; i < slice_size; ++i) {
    var_slice[i] = 0.0;
  }
  //
  // Find the scalar indices for the smoothing length, particle mass and density
  //  required for the SPH interpolation
  //
  int ind_h = GetScalarIndex("smoothing_length");
  int ind_pmass = GetScalarIndex("particle_mass");
  int ind_density = GetScalarIndex("density");

  //
  // Generate the pixel grid
  // The pixels are always in the xy plane of a Cartesian coordinate system,
  // centered on the origin. Instead of shifting and rotating the pixel center
  // coordinates (i.e. moving the camera) it is rather the SPH particle set
  // that is shifted with slice_xcent, slice_ycent, slice_zcent and rotated
  // with incl_rad, posang_rad, and phi_rad. It is then easier to select which
  // pixels a given SPH particle contributes to.
  //
  double slice_size_x = static_cast<size_t>(n_pixel_x) * delta_x;
  double slice_x_min = -0.5 * slice_size_x;
  std::vector<double> pixel_x(n_pixel_x, 0.0);
  for (int i = 0; i < n_pixel_x; ++i) {
    pixel_x[i] = slice_x_min + static_cast<double>(i) * delta_x;
  }

  double slice_size_y = static_cast<size_t>(n_pixel_y) * delta_y;
  double slice_y_min = -0.5 * slice_size_y;
  std::vector<double> pixel_y(n_pixel_y, 0.0);
  for (int i = 0; i < n_pixel_y; ++i) {
    pixel_y[i] = slice_y_min + static_cast<double>(i) * delta_y;
  }
  double pixel_half_width = std::max(delta_x, delta_y) * 0.5;
  //
  // Pre-compute some trigonometric functions
  //
  double cos_phi = std::cos(-phi_rad);
  double sin_phi = std::sin(-phi_rad);
  double cos_incl = std::cos(-incl_rad);
  double sin_incl = std::sin(-incl_rad);
  double cos_posang = std::cos(posang_rad);
  double sin_posang = std::sin(posang_rad);

#pragma omp parallel for schedule(static) num_threads(n_thread)
  // Loop through all SPH particles
  for (int ip = 0; ip < n_point_; ++ip) {
    //
    // Rotate the points with phi around the z axis and shift them with
    // slice_xcent, slice_ycent, slice_zcent
    //
    double rot_x = (ptr_points_[ip]->crd[0] - slice_xcent) * cos_phi +
                   (ptr_points_[ip]->crd[1] - slice_ycent) * sin_phi;
    double rot_y = -(ptr_points_[ip]->crd[0] - slice_xcent) * sin_phi +
                   (ptr_points_[ip]->crd[1] - slice_ycent) * cos_phi;
    double rot_z = ptr_points_[ip]->crd[2] - slice_zcent;

    //
    // Rotate the points with Incl around the X axis
    //
    double dummy_x = rot_x * cos_incl + rot_z * sin_incl;
    double dummy_y = rot_y;
    double dummy_z = -rot_x * sin_incl + rot_z * cos_incl;
    //
    // Rotate the points with Posang around the Z axis
    //
    rot_x = dummy_x * cos_posang + dummy_y * sin_posang;
    rot_y = -dummy_x * sin_posang + dummy_y * cos_posang;
    rot_z = dummy_z;
    //
    // Check if the particle is within 2h distance from the xy-plane
    //
    if (abs(rot_z) / ptr_points_[ip]->scalars[ind_h] <= 2.0) {
      //
      // Get the indices of the pixel the current particle falls into
      //
      int ix = static_cast<int>(
          std::round((rot_x - slice_x_min) / (slice_size_x)*n_pixel_x));
      int iy = static_cast<int>(
          std::round((rot_y - slice_y_min) / (slice_size_y)*n_pixel_y));

      int two_h_per_pixel = static_cast<int>(std::ceil(
          2.0 * ptr_points_[ip]->scalars[ind_h] / (2.0 * pixel_half_width)));
      //
      // Get the indices of the pixels the particle may have a contribution to
      //
      int ix_min = std::max(ix - two_h_per_pixel, 0);
      int ix_max = std::min(ix + two_h_per_pixel + 1, n_pixel_x);
      int iy_min = std::max(iy - two_h_per_pixel, 0);
      int iy_max = std::min(iy + two_h_per_pixel + 1, n_pixel_y);

      //
      // Dimensionless weight for the SPH interpolation
      //
      double weight = ptr_points_[ip]->scalars[ind_pmass] /
                      ptr_points_[ip]->scalars[ind_density] /
                     (ptr_points_[ip]->scalars[ind_h] *
                      ptr_points_[ip]->scalars[ind_h] *
                      ptr_points_[ip]->scalars[ind_h]);
      //
      // Now check if we have a scalar or a vector as vectors should also be
      // rotated
      //
      if (is_scalar) {
        for (int ix = ix_min; ix < ix_max; ++ix) {
          for (int iy = iy_min; iy < iy_max; ++iy) {
            double dist = ((rot_x - pixel_x[ix]) * (rot_x - pixel_x[ix]) +
                           (rot_y - pixel_y[iy]) * (rot_y - pixel_y[iy]) +
                           (rot_z * rot_z));
            dist = std::sqrt(dist) / ptr_points_[ip]->scalars[ind_h];

            if (dist <= 2.0) {
              double kernel = ptr_smoothing_kernel_->W(dist);
#pragma omp atomic
                var_slice[ix + iy * n_pixel_x] +=
                    ptr_points_[ip]->scalars[ind_var] * weight * kernel;
            }
          }
        }
      } else {
        //
        // Rotate the vector
        //
        std::vector<double> vector_rot(3, 0.0);
        std::vector<double> vector_tmp(3, 0.0);

        //
        // Rotate the points with phi around the z axis and shift them with
        // slice_xcent, slice_ycent, slice_zcent
        //
        vector_rot[0] =
            ptr_points_[ip]->vectors[0 + ind_var * n_dim_] * cos_phi +
            ptr_points_[ip]->vectors[0 + ind_var * n_dim_] * sin_phi;
        vector_rot[1] =
            -ptr_points_[ip]->vectors[1 + ind_var * n_dim_] * sin_phi +
            ptr_points_[ip]->vectors[1 + ind_var * n_dim_] * cos_phi;
        vector_rot[2] = ptr_points_[ip]->vectors[2 + ind_var * n_dim_];

        //
        // Rotate the points with Incl around the X axis
        //
        vector_tmp[0] = vector_rot[0] * cos_incl + vector_rot[2] * sin_incl;
        vector_tmp[1] = vector_rot[1];

        //
        // Rotate the points with Posang around the Z axis
        //
        vector_rot[0] = vector_tmp[0] * cos_posang + vector_tmp[1] * sin_posang;
        vector_rot[1] =
            -vector_tmp[0] * sin_posang + vector_tmp[1] * cos_posang;

        for (int ix = ix_min; ix < ix_max; ++ix) {
          for (int iy = iy_min; iy < iy_max; ++iy) {
            double dist = ((rot_x - pixel_x[ix]) * (rot_x - pixel_x[ix]) +
                           (rot_y - pixel_y[iy]) * (rot_y - pixel_y[iy]) +
                           (rot_z * rot_z));
            dist = std::sqrt(dist) / ptr_points_[ip]->scalars[ind_h];

            if (dist <= 2.0) {
              double kernel = ptr_smoothing_kernel_->W(dist);
#pragma omp atomic
              var_slice[0 + (ix + iy * n_pixel_x) * 2] +=
                  vector_rot[0] * weight * kernel;
#pragma omp atomic
              var_slice[1 + (ix + iy * n_pixel_x) * 2] +=
                  vector_rot[1] * weight * kernel;
            }
          }
        }
      }
    }
  }
}
