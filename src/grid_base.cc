// ============================================================================
// grid_base.cc
//
// This file is part of sphtool:
// Gridding and visualisation package for SPH simulations
//
// Copyright (C) 2018 Attila Juhasz
// Author: Attila Juhasz
//
// sphtool is free software; you can redistribute it and/or modify it under
// the terms of the GNU General Public License as published by the Free Software
// Foundation; either version 2 of the License, or (at your option) any later
// version.
//
// sphtool is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.
// ============================================================================

#include <string>
#include <stdexcept>
#include "../include/grid_base.h"

// ------------------------------------------------------------------------
//
// ------------------------------------------------------------------------
GridBase::GridBase() {
  crd_system_ = "cartesian";
  active_dim_.resize(3);
  for (int i = 0; i < 3; ++i)
    active_dim_[i] = false;
}
// ------------------------------------------------------------------------
//
// ------------------------------------------------------------------------
GridBase::~GridBase() {}
// ------------------------------------------------------------------------
//
// ------------------------------------------------------------------------
std::string GridBase::GetCrdSystem() { return crd_system_; }
// ------------------------------------------------------------------------
//
// ------------------------------------------------------------------------
void GridBase::SetCrdSystem(const std::string& crd_system) {
  if ((crd_system != "cartesian") & (crd_system != "spherical")) {
    throw std::runtime_error(
        "Unknown coordinate system : " + crd_system +
        ". Only cartesian and spherical coordinate systems are implemented.");
  }
  this->crd_system_ = crd_system;
}
// ------------------------------------------------------------------------
//
// ------------------------------------------------------------------------
bool GridBase::GetActiveDimension(const int& iaxis) {
  if ((iaxis < 0) | (iaxis > 2)) {
    throw std::runtime_error("Invalid dimension : " + std::to_string(iaxis) +
                             ". Valid dimension index is between 0 and 2.");
  }
  return this->active_dim_[iaxis];
}
// ------------------------------------------------------------------------
//
// ------------------------------------------------------------------------
int GridBase::GetNCell(const int& iaxis) {return 0;}
// ------------------------------------------------------------------------
//
// ------------------------------------------------------------------------
int GridBase::GetNCell() {return 0;}
// ------------------------------------------------------------------------
//
// ------------------------------------------------------------------------
void GridBase::GetCellCenter(const int& iaxis, double* vector_double_out,
                             const int& dim) {}
// ------------------------------------------------------------------------
//
// ------------------------------------------------------------------------
void GridBase::WriteToFile(const std::string& file_name,
                           const std::string& format) {}
// ------------------------------------------------------------------------
//
// ------------------------------------------------------------------------

void GridBase::ReadFromFile(const std::string& file_name,
                            const std::string& format) {}
// ------------------------------------------------------------------------
//
// ------------------------------------------------------------------------
void GridBase::ResetGrid() {}
