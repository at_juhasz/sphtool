.. _howtoregrid:
**************************
Regridding SPH simulations
**************************

    .. The fundamental capability of sphtool is to interpolate physical (vector or scalar) variables from Smoothed Particle Hydrodynamic (SPH) simulations to a regular spherical or cartesian grid or to an octree AMR mesh. Currently sphtool does not have file I/O capabilities for the file format of various SPH codes. Writing functions/scripts to read the variables from output files of SPH codes is currently the responsibility of the user. Since the primary goal of the development of sphtool was to produce an interface between SPH codes and `RADMC-3D <http://www.ita.uni-heidelberg.de/~dullemond/software/radmc-3d/>`_, currently the output file format of the gridded variables and the spatial grid is that of `RADMC-3D <http://www.ita.uni-heidelberg.de/~dullemond/software/radmc-3d/>`_.

.. _howtoregrid-method:

Regridding method
=================
Gridding to an AMR (Adaptive Mesh Refinement) grid or a regular grid is using the SPH kernel interpolation method to calculate the variables in the centers of 
the grid cell. Thus it works in a similar way for regular and AMR meshes. For the latter, however, there is an additional refinement method, which is explained
below, which can make the gridding slower compared to that of a regular grid of the same size.

The basic idea of interpolating the SPH particle based quantities to grid based quantities is to find all SPH particles that are within 2h distance from the cell
center (here 'h' is the smoothing length of that particle). Then use the SPH kernel interpolation scheme to find the scalar or vector variables in the cell center.
Currently only the cubic spline kernel is implemented in `sphtool`:

.. math::
    W(r, h) = \frac{\sigma}{h^\nu}\begin{cases} \mbox{1 - $\frac{3}{2}q^2$ + $\frac{3}{4}q^3$} & \mbox{if } 0\leq q<1 \\ 
                            \mbox{$\frac{1}{4}(1-q)^3$} & \mbox{if } 1\leq q<2\\
                            \mbox{$0$} & \mbox{if } 2\leq q\\
                            \end{cases}

where :math:`r` is the distance to the particle, :math:`h` is the smoothing length, :math:`\sigma` is a normalisation factor, :math:`\nu` is the 
number of spatial dimensions, and :math:`q=r/h`. 

`sphtool` uses an octree built over the SPH particles to find particles which are within 2h distance to the cell centers. The interpolation is parallelised
over the number of points to interpolate. 

.. _howtoregrid-method-regular-grid:

Regular grid
------------

Interpolation to a regular is implemented in two coordinate systems, regular or spherical. The coordinate order for cartesian grid is x, y, z. For spherical
coordinates :math:`\theta` is the poloidal angular coordinate, which is 0 at the north pole and :math:`\pi` at the south pole and :math:`\phi` is the azimuthal
angular coordinate going from 0 to :math:`2\pi`. The index order for spherical coordinates, :math:`r`, :math:`\theta`, :math:`\phi`.

Interpolation for regular grids does not require the cell sizes to be uniform, the cell center coordinates only need to be monotonic. This means, that for spherical
grid a logarithmic spacing of the radial coordinate is also possible. 

.. _howtoregrid-method-octree:

Octree 
------

Gridding to a regular grid is a single step. For a given set of grid cell coordinates the interpolator will find the scalar and vector variables at the grid
cell centers. In contrast, gridding to an octree mesh is a two/three step process. First the octree is built, in the next step the mesh might be further refined, 
and finally, the interpolator calculates the requested variables in the cell center coordinates. 

The octree starts from a single root node and the resolution of the cells is based on the SPH particles
contained by the cells. If a cell contains more than a user-defined number of SPH particles, the cell is resolved. The AMR grid building algorithm uses two parameters
to generate the mesh: the maximum number of SPH particles to be contained in leaf nodes and the total grid/tree depth, i.e. the highest resolution level. 
When the root node is resolved, the resolutions proceeds recursively for all child nodes, until either each leaf node contains no more SPH particles than the 
threshold value, or until the limiting tree depth is reached. This way the structure of the generated mesh closely follows the distribution of SPH particles, 
which in turn follows the mass, density. 

A mass/density dependent resolution is is not necesssarily a good choice for radiative transfer. In many cases a good choice of mesh is the one that samples 
the density gradients well, instead of sampling the highest density regions well. This is the case, for instance, when near-infrared scattered light images
are calculated in protoplanetary disk models. In this case photons are scattered in the uppermost layers of the disk, where SPH simulations have few particles
with large smoothing length. To get smoother images, it is important to sample the transition region between optically thin and thing regions well, in the upper
layers of the disk. To do so `sphtool` can refine the octree mesh, built over the particle distribution, based on the density variation, density gradient 
within the grid cells.
This is implemented in the following way: the density is calculated in each leaf-cell in a user-defined number of random points :math:`\rho_{\rm i}`. 
Then the density variation within the grid cell is calculated as :math:`(\max{\rho_{\rm i}} - \min{\rho_\rm{i}})/\max{\rho_{\rm i}}`. If the density 
variation is higher than a user-defined threshold value, and the cell depth is not higher than a pre-defined grid depth, the cell is resolved. 

In some cases the density gradient is the highest in the lowest density regions. E.g. protoplanetary disks have a Gaussian vertical density distribution, 
which has an increasing vertical density gradient with height. In such model the gradient based resolution algorithm would resolve most of the cells close to the 
highest limit above a certain vertical height of the x-y plane. We can prevent this by using a density floor in the grid building algorithm. The
density floor basically sets the lowest density above which the density gradient refinement is active. Note, that this density floor is used for the 
grid building only! The interpolation algorithm does not use floor or ceiling values for the variables. If one wishes to use floor/ceiling values for 
any variable, one is free to do so in either the gridded variable array, or in the file output. 

Finally there are two additional things to keep in mind when gridding to octree meshes. While the building of an octree mesh over the SPH particle cloud can be 
reasonably fast, the refinement can take some time, depending on the number AMR leaf cells, the highest allowed resolution level in the mesh and the number of 
random location one wishes to probe the density. Another thing to keep in mind is that since the octree building algorithm contains a stochastic element
(the gradient refinement algorithm is based on a random set of points), so will the generated mesh. In other words, two runs of the gridding / grid building
algorithm with gradient refinement on the same SPH simulation and the same parameters can result in slightly different grids. 

.. _howtoregrid-fileformat:

File format
===========

.. _howtoregrid-fileformat-sph:

SPH file input
-------------

Currently sphtool does not have dedicated file I/O capabilities for the file format of various SPH codes, i.e. there are no dedicated functions
to read the output of e.g. GADGET-2 or PHANTOM. Writing functions/scripts to read the variables from output files of SPH codes is currently the 
responsibility of the user (see examples below).


.. _howtoregrid-fileformat-gridded:

Grid/gridded data output
------------------------

`sphtool` can either return the gridded variable(s) as numpy arrays, or it can also write them to file. Currently two output file formats are supported, 
that of `RADMC-3D <http://www.ita.uni-heidelberg.de/~dullemond/software/radmc-3d/>`_, and the `Legacy VTK format <https://www.vtk.org/wp-content/uploads/2015/04/file-formats.pdf>`_. For `RADMC-3D <http://www.ita.uni-heidelberg.de/~dullemond/software/radmc-3d/>`_ format both formatted ASCII and binary files are supported. 
Regarding the output file format of sphtool see the `manual of RADMC-3D <http://www.ita.uni-heidelberg.de/~dullemond/software/radmc-3d/guide.html>`_.
To view Legacy VTK files one can use e.g. `ParaView <https://www.paraview.org>`_, or `VisIt <https://wci.llnl.gov/simulation/computer-codes/visit/>`_.


.. _howtoregrid-examples:

Examples
========

Below there are examples how to interpolate 3D sph simulations to a regular or an octree AMR mesh in cartesian coordinates. 
There are more examples in the downloaded package in the `examples` folder, including the ones below. The example SPH simulation
can be downloaded from the :ref:`downloads` area, accessible also from the menu on the left. 



.. _howtoregrid-examples-regular-grid:

Regrid to a regular grid
------------------------

The following example shows how to regrid sph simulations to a regular grid. 

First let us import SphTool, the fundamental base class of `sphtool`, and numpy::

    from sphtool import SphTool
    import numpy as np

The following function will read the SPH simulation and return the variables in a dictionary::

    def read_sph(fname=None):
        """
        Function to read the example SPH simulation
       
        Parameters
        ----------

            fname   - str
                      Name of the SPH snapshot file to be read
        Returns
        -------
            A dictionary with the following keys
                * npart - Number of particles
                * x     - Cartesian x coordinate of the particle
                * y     - Cartesian y coordinate of the particle
                * z     - Cartesian z coordinate of the particle
                * pmass - Particle mass
                * h     - Smoothing length
                * vx    - Cartesian x velocity compoent of the particle
                * vy    - Cartesian y velocity compoent of the particle
                * vz    - Cartesian z velocity compoent of the particle

            All of the dictionary keys contain a numpy ndarray with a length of npart
        """

        dum = np.genfromtxt(fname, skip_header=12)

        # Select the valid sph particles
        ii = dum[:,-1] == 1
        
        return {'npart': dum[ii,0].shape[0], 'x':dum[ii,0], 'y':dum[ii,1], 'z':dum[ii,2], 
                'pmass':dum[ii,3], 'h':dum[ii,4], 'rho':dum[ii,5], 'vx':dum[ii,6], 
                'vy':dum[ii,7], 'vz':dum[ii,8]}


Read the sph simulations::
    
    sph = read_sph(fname='snap_00000.ascii')


Create an instance of SphTool that uses maximum 4 parallel threads and contains as many points as sph particles we have::

    spt = SphTool(n_thread=4, n_point=sph['npart'])

Now set all coordinates and variables of each SPH particle. For N particles scalar variables should be set as ndarrays with N
elements, containing the scalar for each particle. Vector variables should be passed to the :meth:`SphTool.set_vector` as two
dimensional ndarrays with [N, M] where N is the number of SPH particles and M is number of vector component. 
Note, that there are four mandatory quantities to be set: crd, density, smoothing_length, particle_mass::

    spt.set_scalar("density", sph['rho'])
    spt.set_scalar("smoothing_length", sph['h'])
    spt.set_scalar("particle_mass", sph['pmass'])
    spt.set_vector("crd", sph['crd'])
    spt.set_vector("velocity", sph['v'])


Let us finalize the setup: the finalize method validates the sph data set, checks if all mandatory variables have been set 
and builds the tree used for the interpolation::

    spt.finalize()

Generate the cartesian spatial grid we wish to interpolate to. Note, that `sphtool` requires the cell interface coordinates, not
the that of the cell centers::

    xi = np.linspace(-25, 25, 200) 
    yi = np.linspace(-25, 25, 200) 
    zi = np.linspace(-25, 25, 200) 

Finally do the gridding/interpolation of the density and the velocity fields to the grid we just defined::

    spt.regrid_to_regular_grid(["density", "velocity"], crd_system="cartesian", xi=xi, yi=yi, zi=zi)

We can now write the grid and the gridded density to file if we wish to do so.

In RADMC-3D format::

    spt.write_grid("amr_grid.inp", format='radmc3d')
    spt.write_gridded_var(["density"], file_name="dust_density.inp", file_format="radmc3d", binary=False, scalar_floor=1e-90) 

In Legacy VTK format::

    spt.write_gridded_var(["density", "velocity"], file_name="model.vtk", file_format="vtk") 
        
If we open the written VTK file with ParaView resulting gridded density distribution should look like this:

.. image:: screenshot_regular_cartesian.png
    :align: center



.. _howtoregrid-examples-amr-grid:

Regrid to octree
----------------

The gridding of an SPH simulation to an octree AMR mesh is very similar to the gridding to a regular grid. Reading
the variables and passing them to an SphTool instance is the same as in the above example for the regular grid. Thus
it will not be repeated here. For an AMR grid we only need to pass arrays containing the corner coordinates of the 
root cell, i.e. min/max values in each spatial dimension. Then we need to set the maximum number of SPH particles, 
a leaf node can contain (let's set it to 30), the highest maximum resolution level (set it to 8) ::

    spt.regrid_to_amr_grid(["density", "velocity"], crd_system="cartesian", xi=[-25., 25.], 
                       yi=[-25., 25.], zi=[-25., 25.], max_particle_per_amr_grid_cell=30, 
                       max_amr_grid_depth=8.)


Now we can again write the gridded variable(s) to RAMDC-3D format::

    spt.write_gridded_var(["density"], file_name="dust_density.inp", file_format="radmc3d", binary=False, scalar_floor=density_floor) 
    spt.write_gridded_var(["density", "velocity"], file_name="model_no_refinement.vtk", file_format="vtk") 
    
or in Legacy VTK format::
    
    spt.write_gridded_var(["density", "velocity"], file_name="model_no_refinement.vtk", file_format="vtk") 


If we open the written VTK file with ParaView resulting gridded density distribution should look like this:

.. image:: screenshot_amr_cartesian_no_refinement.png
    :align: center

.. _howtoregrid-examples-amr-grid-gradient-refinement:

Octree gradient refinement
--------------------------

As mentioned above it is also possible to use further iterations to refine the mesh along density gradients. To do so 
we can use several futher keyword arguments at the call of :func:`~sphtool.SphTool.regrid_to_amr_grid`. 
First we are going to create a basic octree AMR mesh based on the particle distribution, whose root cell 
extends in the [-25., 25.] interval in all three dimensions (:code:`xi=[-25., 25.]`,  :code:`yi=[-25., 25.]`, :code:`zi=[-25., 25.]`), 
and whose leaf cells can contain at most 300 SPH particles (:code:`max_particle_per_amr_grid_cell=300`) and which has
a maximum grid depth of 4 (:code:`max_amr_grid_depth=4`).

With only these parameters we would get a fairly low resolution grid. To refine it along the gradients we need to add a few
more keywords to the call of :func:`~sphtool.SphTool.regrid_to_amr_grid`. With :code:`refine_gradient=True` the gradient refinement
is enabled, then one has to set the maximum grid depth after the refinement (:code:`max_refined_depth=8`), which cannot be smaller 
than the grid depth used for the particle based grid building. The algorithm determines the density variation in each leaf cell
via calculating the density at random location within the cell. We also need to set the number of random locations the density
will be probed at (:code:`n_trial_points=300`). Once the density is known at the trial points the quantity 
:math:`(\max{\rho_{\rm i}} - \min{\rho_\rm{i}})/\max{\rho_{\rm i}}` will be calculated, and if it is higher than a given threshold
the cell will be refined. This threshold should also be set (:code:`threshold=0.95`). Finally we need to set a floor value for the
density above which density gradient refinement will be enabled (:code:`density_floor=1e-9`)::

    spt.regrid_to_amr_grid(["density", "velocity"], crd_system="cartesian", xi=[-25., 25.], 
                       yi=[-25., 25.], zi=[-25., 25.], max_particle_per_amr_grid_cell=300, 
                       max_amr_grid_depth=4, refine_gradient=True, max_refined_depth=8,
                       n_trial_points=300, threshold=0.95, density_floor=1e-9)


Once the gridding is done we can again write the gridded variable(s) to RAMDC-3D format::

    spt.write_gridded_var(["density"], file_name="dust_density.inp", file_format="radmc3d", binary=False, scalar_floor=density_floor) 
    spt.write_gridded_var(["density", "velocity"], file_name="model_no_refinement.vtk", file_format="vtk") 
    
or in Legacy VTK format::
    
    spt.write_gridded_var(["density", "velocity"], file_name="model_no_refinement.vtk", file_format="vtk") 


If we open the written VTK file with ParaView resulting gridded density distribution should look like this:

.. image:: screenshot_amr_cartesian.png
    :align: center

