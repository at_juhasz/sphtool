.. _gettingstarted:
***************
Getting started
***************

.. _requirements:

Requirements
============
sphtool requires the following to be installed:

* gnu make
* gcc/g++ (tested with gcc 5 and 6)
* swig 3.0.8+
* Python 2 or 3 (tested with Python 2.7 and 3.6)
* numpy 
* matplotlib v1.5+


.. _installation:

Installation
============

Go to the sphtool root directory. The first thing is to edit the Makefile first three lines or four lines of the Makefile to set the C++ compiler, the path to the Python installation directory, the Python version to be used and possibly the optimisation flags. Once it's done one can follow the usual way of compiling and installing linux/unix software by issuing the command

$>make

in a shell (currently only bash is supported). This will compile the C++ codebase. Then you can use the provided python setup.py to install the
package:

$>python setup.py install

This will copy the corresponding files from the sphtool folder to the appropriate directory. The setup.py uses distutils, thus you can use the usual
command line arguments if you wish to install it to a custom directory: 

$>python setup.py install --prefix=target_directory

where `target_directory` is the path to the directory where one wishes to install sphtool. 

