%module sphtool
%{
    #define SWIG_FILE_WITH_INIT
    #include "../include/point_data.h"
    #include "../include/sph_data.h"
    #include "../include/gridded_data.h"
    #include "../include/sphtool.h"
%}
%include "std_vector.i"
%include "std_string.i"
%include "exception.i"
%include "numpy.i"

%feature("autodoc", "3");

namespace std {
    %template(StringVector) std::vector<string>;
};

%init %{
    import_array();
%}

%exception {
    try {
        $action
    } catch (std::runtime_error &e) {
        PyErr_SetString(PyExc_RuntimeError, const_cast<char*>(e.what()));
        return NULL;
    }
}


void SPHTool::SetParamInt(std::string var_name, int var_value);
int SPHTool::GetParamInt(std::string var_name);
std::vector<std::string> SPHTool::GetAllScalarNames();
std::vector<std::string> SPHTool::GetAllVectorNames();
void SPHTool::WriteVTK(std::string file_name, std::vector<std::string> var_name);

%apply (double* IN_ARRAY1, int DIM1) {(double* vector_double_in, int dim)}
%apply (double* ARGOUT_ARRAY1, int DIM1) {(double* vector_double_out, int dim_out)}
%apply (double* IN_ARRAY1, int DIM1) {(double* xi, int nxi), (double* yi, int nyi), (double* zi, int nzi)}
%apply (double* ARGOUT_ARRAY1, int DIM1) {(double* xc_out, int nx_out), (double* yc_out, int ny_out), (double* zc_out, int nz_out)}
%apply (int* ARGOUT_ARRAY1, int DIM1) {(int* vector_int_out, int dim_out)}

%include "../include/point_data.h"
%include "../include/sph_data.h"
%include "../include/gridded_data.h"
%include "../include/sphtool.h"

%template(RegularGriddedDataSet) GriddedDataSet<RegularGrid>;
%template(AMRGriddedDataSet) GriddedDataSet<AMRGrid>;

%pythoncode %{
import numpy as np
import matplotlib.pyplot as plt
from matplotlib.colors import Normalize, PowerNorm, LogNorm
from matplotlib.colors import LogNorm

class SphTool(SPHTool):
    """
    SphTool base class

    Parameters
    ----------
        n_point                         : int
                                          Number of SPH data points

        n_dim                           : int
                                          Number of spatial dimensions

        n_thread                        : int
                                          Number of parallel threads to be used

        max_particle_per_cell           : int
                                          Number of particles per cell in the tree

        max_tree_depth                  : int
                                          Maximum tree depth

        max_particle_per_amr_grid_cell  : int
                                          Maximum number of SPH particles per AMR grid cells
                                          when regridding to an AMR grid

        max_amr_grid_depth              : int
                                          Maximum depth of the octree AMR when regridding to an AMR grid
                                        
        ki_table_n_point                : int
                                          Number of steps to take when numerically evaluating the 1D integral
                                          through the SPH kernel

        ki_table_n_val                  : int
                                          Number of elements of the lookup table used to pre-calculate the 
                                          1D integral through the SPH kernel. The sampling is in the impact
                                          parameter / perpendicular distance between the center of the kernel
                                          and the line of integration

        output_ascii_precision          : int
                                          Output precision in formatted ASCII output (number of significant digits
                                          in exponential notation)
    
    """
    def __init__(self, n_point=0, n_dim=3, n_thread=1, max_particle_per_cell=10, max_tree_depth=15, 
            max_particle_per_amr_grid_cell=50, max_amr_grid_depth=5,
            ki_table_n_point=1000, ki_table_n_val=1000, output_ascii_precision=9):
        
        super(SphTool, self).__init__()
        
        self.set_param_int("n_thread", n_thread)
        self.set_param_int("max_particle_per_cell", max_particle_per_cell)
        self.set_param_int("max_tree_depth", max_tree_depth)
        self.set_param_int("max_particle_per_amr_grid_cell", max_particle_per_amr_grid_cell)
        self.set_param_int("max_amr_grid_depth", max_amr_grid_depth)
        self.set_param_int("ki_table_n_point", ki_table_n_point)
        self.set_param_int("ki_table_n_val", ki_table_n_val)
        self.set_param_int("output_ascii_precision", output_ascii_precision)

        if n_point > 0:
            self.set_size(n_point, n_dim)

    def set_scalar(self, var_name=None, arr=None):
        """
        Sets a scalar variable. 

        Parameters
        ----------

            var_name      : str
                            Name of the scalar variable

            arr           : ndarray
                            1D numpy ndarray containing the variable values for each SPH particle
        """
       
        self._set_scalar(var_name, np.ascontiguousarray(arr.flatten()))

    def get_scalar(self, var_name=None):
        """
        Returns a scalar variable in the SPH point based representation

        Parameters
        ----------

            var_name      : str
                            Name of the scalar variable
        Returns
        -------
            An ndarray with [Npoint] dimensions containing the scalar variable for each SPH particle.
        """
        
        npoint = self.get_npoint()
        arr = self._get_scalar(var_name, npoint)
        return arr 
    
    def set_vector(self, var_name=None, arr=None):
        """
        Sets a vector variable

        Parameters
        ----------

            var_name      : str
                            Name of the vector variable

            arr           : ndarray
                            2D numpy ndarray containing the variable values for each SPH particle. 
                            The first index is the particle index, the second index must be the vector component index.
        """
        
        ndim = arr.shape[1]
        self._set_vector(var_name, np.ascontiguousarray(arr.flatten()), ndim)

    def get_vector(self, var_name=None):
        """
        Returns a vector variable in the SPH point based representation

        Parameters
        ----------

            var_name       : str
                             Name of the vector variable
        Returns
        -------
            An ndarray with [Npoint, Ndim] dimensions, where Npoint is the number of SPH particles and Ndim is the number
            of spatial dimensions / vector components.
        """
        
        npoint = self.get_npoint()
        ndim = self.get_ndim()
        arr = self._get_vector(var_name, npoint*ndim, ndim)
        arr.reshape(npoint, ndim)
        
        return arr 


    def interpolate(self, var_name=None, x=None, y=None, z=None):
        """
        Performs SPH interpolation to find the variable(s) at given coordinates

        Parameters
        ----------
            var_name        : str
                              Name of the vector variable
           
            x               : ndarray, list, tuple
                              Cartesian x coordinate of the points to interpolate the variable to
            
            y               : ndarray, list, tuple
                              Cartesian y coordinate of the points to interpolate the variable to
            
            z               : ndarray, list, tuple
                              Cartesian z coordinate of the points to interpolate the variable to

        Returns
        -------
            An ndarray containing the variable interpolated at the requested coordinates.
        """
        
        #
        # Convert x, y, z to numpy ndarrays
        #
        if isinstance(x, np.ndarray):
            if len(x.shape) > 1:
                raise RuntimeError("x has an invalid shape (larger than one dimension).")
        else:
            if isinstance(x, list) | isinstance(x, tuple):
                x = np.array(x, dtype=np.float64)
            else:
                x = np.array([np.float64(x)])

        if isinstance(y, np.ndarray):
            if len(y.shape) > 1:
                raise RuntimeError("y has an invalid shape (larger than one dimension).")
        else:
            if isinstance(y, list) | isinstance(y, tuple):
                y = np.array(y, dtype=np.float64)
            else:
                y = np.array([np.float64(y)])

        if isinstance(z, np.ndarray):
            if len(z.shape) > 1:
                raise RuntimeError("z has an invalid shape (larger than one dimension).")
        else:
            if isinstance(z, list) | isinstance(z, tuple):
                z = np.array(z, dtype=np.float64)
            else:
                z = np.array([np.float64(z)])
   
        #
        # Check the x,y,z array sizes
        #
        npoint = np.array([x.shape[0], y.shape[0], z.shape[0]], dtype=int)
        imax = npoint.argmax()
        for i in range(3):
            if (npoint[i] != npoint[imax]) & (npoint[i] != 1):
                raise RuntimeError("Input coordinate array have wrong shapes. x, y, z arrays "\
                        + "need to be either of the same shape, or they should be single element "\
                        + "array or single float")
        
        if x.shape[0] == npoint[imax]:
            x_interp = x
        else:
            x_interp = np.zeros(npoint[imax], dtype=np.float64) + x[0]

        if y.shape[0] == npoint[imax]:
            y_interp = y
        else:
            y_interp = np.zeros(npoint[imax], dtype=np.float64) + y[0]

        if z.shape[0] == npoint[imax]:
            z_interp = z
        else:
            z_interp = np.zeros(npoint[imax], dtype=np.float64) + z[0]

        #
        # Check the variable names
        #
        if not isinstance(var_name, str):
            raise RuntimeError("var_name is not a string. Variable name must be a single string object.")
       
        var = self._interpolate(var_name, x_interp, y_interp, z_interp, np.int(npoint[imax]))
        
        return var

    def regrid_to_regular_grid(self, var_name=None, crd_system=None, xi=None, yi=None, zi=None):
        """
        Re-grids the SPH simulation to a regular grid

        Parameters
        ----------
            var_name        : str, list
                              Name(s) of the variables to be regridded

            crd_system      : {'cartesian', 'spherical'}
                              Coordinate system type

            xi              : ndarray
                              Cell interfaces in the first dimension (x - cartesian, r - spherical)
            
            yi              : ndarray
                              Cell interfaces in the second dimension (y - cartesian, theta - spherical)
            
            zi              : ndarray
                              Cell interfaces in the second dimension (z - cartesian, phi - spherical)
        """
        
        if isinstance(var_name, list) | isinstance(var_name, tuple):
            for ivar, var in enumerate(var_name):
                if not isinstance(var, str):
                    raise RuntimeError("Element " + str(ivar) + " in var_name is not a string. var_name "
                                        + " must either be string or a list/tuple of strings")
        else:
            if not isinstance(var_name, str):
                raise RuntimeError("var_name must either be string or a list/tuple of strings")
            
            var_name = [var_name]

        nx = xi.shape[0]
        ny = yi.shape[0]
        nz = zi.shape[0]
        nvar = len(var_name)
        self._set_grid("regular", xi, yi, zi, crd_system)
        self._regrid(var_name)


    def regrid_to_amr_grid(self, var_name=None, crd_system=None, xi=None, yi=None, zi=None, 
            max_particle_per_amr_grid_cell=None, max_amr_grid_depth=None, refine_gradient=False,
            n_trial_points=None, threshold=None, density_floor=None, max_refined_depth=None):
        """
        Re-grids the SPH simulation to an octree AMR grid

        Parameters
        ----------
            var_name        : str, list
                              Name(s) of the variables to be regridded

            crd_system      : {'cartesian'}
                              Coordinate system type (currently only cartesian coordinates are implemented)

            xi              : list, ndarray
                              Two element list/ndarray containing the min, max values of the root cell in the
                              first (x) coordinate
            
            yi              : list, ndarray
                              Two element list/ndarray containing the min, max values of the root cell in the
                              second (y) coordinate
            
            zi              : list, ndarray
                              Two element list/ndarray containing the min, max values of the root cell in the
                              third (z) coordinate

            max_particle_per_amr_grid_cell : float
                                             Number of SPH particles per AMR grid cell 

            max_amr_grid_depth             : float
                                             Maximum resolution level in the AMR octree

            refine_gradient                : bool
                                             Refine AMR grid based on density variation within the grid cells
                                             (True = yes, False = no)

            n_trial_points                 : int
                                             Number of trial points to estimate density variation within the
                                             grid cells

            threshold                      : float
                                             Threshold value for the quantity (rho_max - rho_min)/rho_max used to
                                             estimate the density variation for the cell. If it is higher than the
                                             threshold value the cell is split/refined, otherwise it is left unchanged.

            density_floor                  : float
                                             Floor value for the density 

            max_refined_depth              : int
                                             Maximum resolution level of the AMR octree after the refinement based
                                             on the density variation (should be higher than or equal to max_amr_grid_depth)
        """
        
        if isinstance(var_name, list) | isinstance(var_name, tuple):
            for ivar, var in enumerate(var_name):
                if not isinstance(var, str):
                    raise RuntimeError("Element " + str(ivar) + " in var_name is not a string. var_name "
                                        + " must either be string or a list/tuple of strings")
        else:
            if not isinstance(var_name, str):
                raise RuntimeError("var_name must either be string or a list/tuple of strings")
            
            var_name = [var_name]


        if xi[1] <= xi[0]:
            raise RuntimeError("Cell interface error for x : x[1] <= x[0]")
        if yi[1] <= yi[0]:
            raise RuntimeError("Cell interface error for y : y[1] <= y[0]")
        if zi[1] <= zi[0]:
            raise RuntimeError("Cell interface error for z : z[1] <= z[0]")

        if max_particle_per_amr_grid_cell is not None:
            self.set_param_int("max_particle_per_amr_grid_cell", max_particle_per_amr_grid_cell)

        if max_amr_grid_depth is not None:
            self.set_param_int("max_amr_grid_depth", max_amr_grid_depth)

        if isinstance(xi, list) | isinstance(xi, tuple):
            xi = np.array(xi, dtype=np.float64)
        
        if isinstance(yi, list) | isinstance(yi, tuple):
            yi = np.array(yi, dtype=np.float64)
        
        if isinstance(zi, list) | isinstance(zi, tuple):
            zi = np.array(zi, dtype=np.float64)

        nx = xi.shape[0]
        ny = yi.shape[0]
        nz = zi.shape[0]
        nvar = len(var_name)
        self._set_grid("amr", xi, yi, zi, crd_system)
        if refine_gradient:
            if n_trial_points is None:
                n_trial_points = 50

            if threshold is None:
                threshold = 0.99

            if density_floor is None:
                density_floor = 1e-8

            if max_refined_depth is None:
                max_refined_depth = max_amr_grid_depth
            else:
                if max_amr_grid_depth > max_refined_depth:
                    msg = "max_refined_depth (" + str(max_refined_depth) +\
                          ") cannot be smaller than max_amr_grid_depth (" + str(max_amr_grid_depth) + ")"
                    raise RuntimeError(msg)

            self._refine_amr_gradient(n_trial_points, threshold, density_floor, max_refined_depth)

        self._regrid(var_name)


    def get_gridded_var(self, var_name=None):
        """
        Returns the gridded variable

        Parameters
        ----------
            var_name        : str, list
                              Name(s) of the gridded variable(s)

        Returns
        -------
        An ndarray containing the gridded variable. The ndarray has three (scalars) or four (vectors) dimensions.
        For vectors, the first three indices are the spatial coordinate indices, while the fourth index is the vector component index. 
        """
        #
        # Check if all variables are gridded
        #
        if isinstance(var_name, list) | isinstance(var_name, tuple):
            for ivar, var in enumerate(var_name):
                if not isinstance(var, str):
                    raise RuntimeError("Element " + str(ivar) + " in var_name is not a string. var_name "
                                        + " must either be string or a list/tuple of strings")
        else:
            if not isinstance(var_name, str):
                raise RuntimeError("var_name must either be string or a list/tuple of strings")
            
            var_name = [var_name]

        nvar = len(var_name)
        ncell = np.zeros(3, dtype=int);
        for i in range(3):
            ncell[i] = self._get_grid_n_cell(i)
        
        var_size = ncell.sum() * nvar
        var = self._get_gridded_var(var_name, var_size)
        var = var.reshape(ncell[0], ncell[1], ncell[2], nvar)
        return var

    def write_gridded_var(self, var_name=None, file_name=None, file_format=None, binary=False, scalar_floor=1e-90):
        """
        Writes gridded variable(s) to file

        This function writes a single file, but multiple scalar variables might be written to a single file. This 
        is needed for e.g. RADMC-3D if multiple dust populations are to be used. 

        Parameters
        ----------
            var_name        : str, list
                              Name(s) of the gridded variable(s)
            
            file_name       : str
                              Name of the file to write the variable(s) to

            file_format     : {'radmc3d', 'vtk'}
                              Format of the output file

            binary          : bool, optional
                              For 'radmc3d' file format if binary = True the file will be written in 
                              C-style binary, for binary = True, the file will be written in formatted ASCII

            scalar_floor    : double, optional
                              Floor value to be used when outputting scalar variable(s) (default: 1e-90)
        """
        if isinstance(var_name, list) | isinstance(var_name, tuple):
            for ivar, var in enumerate(var_name):
                if not isinstance(var, str):
                    raise RuntimeError("Element " + str(ivar) + " in var_name is not a string. var_name "
                                        + " must either be string or a list/tuple of strings")

            if file_format == 'radmc3d':
                self._write_gridded_var_multicomp(file_name, file_format, var_name, binary, scalar_floor)

            elif file_format == 'vtk':
                self._write_vtk(file_name, var_name)

            else:
                raise RuntimeError("Unknown file format " + file_format + ". Allowed formats are 'vtk' or 'radmc3d'")

        else:
            if not isinstance(var_name, str):
                raise RuntimeError("var_name must either be string or a list/tuple of strings")
            
            if file_format == 'radmc3d':
                self._write_gridded_var(file_name, file_format, var_name, binary, scalar_floor)

            elif file_format == 'vtk':
                self._write_vtk(file_name, [var_name])
            
            else:
                raise RuntimeError("Unknown file format " + file_format + ". Allowed formats are 'vtk' or 'radmc3d'")


    def _plot_scalar_data(self, data=None, x=None, y=None, ax=None, cax=None, stretch='linear', gamma=None, vmin=None, vmax=None, 
                          cblabel=None, **kwargs):
        """
        Helper function to plot scalar data

        Parameters
        ----------
            data            : ndarray
                              Two dimensional numpy ndarray containing the image data to be plotted

            x               : ndarray
                              x-coordinate axis of the image data
            
            y               : ndarray
                              y-coordinate axis of the image data
    
            ax              : matplotlib.axes.Axes
                              Matplotlib axes onto which the image should be drawn

            cax             : matplotlib.axes.Axes
                              Matplotlib axes onto which the colorbar should be drawn

            stretch         : {'linear', 'power', 'log'}
                              Color stretch of the image

            gamma           : float
                              Power exponent of the color scale if stretch='power' is selected

            vmin            : float
                              Lower boundary for the pixel intensity color scale 

            vmax            : float
                              Upper boundary for the pixel intensity color scale 

            **kwargs        : keyword arguments 
                              Any further keyword arguments are passed to matplotlib.pcolormesh() 
        Returns
        -------
            A list with two elements: the first is a matplotlib.collections.QuadMesh returned by pcolormesh() used to
            display the projected scalar, while the second element is a matplotlib.colorbar.Colorbar instance             
        """
        if ax is None:
            ax = plt.gca()

        if vmin is None:
            vmin = data.min()

        if vmax is None:
            vmax = data.max()
       
        if stretch == 'linear':
            norm = Normalize(vmin, vmax)

        elif stretch == 'power':
            norm = PowerNorm(gamma, vmin, vmax)

        elif stretch == 'log':
            norm = LogNorm(vmin, vmax)

        else:
            raise RuntimeError("Unknown stretch : " + stretch + ". Allowed stretch values are 'linear', 'power' or 'log'")

        pcm = ax.pcolormesh(x, y, data, norm=norm, **kwargs)
        cbar = plt.colorbar(pcm, ax=ax, cax=cax) 
        
        if cblabel is not None:
            cbar.set_label(cblabel)        

        return pcm, cbar

    def get_coordinate_axis_boundaries(self, iaxis=None):
        """
        Returns the point min/max coordinates of the whole SPH point set in a given coordinate axis

        Parameters
        ----------
            iaxis           : int
                              Coordinate axis (0,1,2)

        Returns
        -------
        An ndarray with two elements containing the min and the max coordinate values of the whole SPH point set in a dimension
        """

        return self._get_coordinate_axis_boundaries(iaxis, 2)

    def plot_slice(self, var_name="", incl=0., posang=0., phi=0., xcent=None, ycent=None, zcent=None, image_size=None, 
            npix=None, ax=None, cax=None, stretch='linear', gamma=None, vmin=None, vmax=None, cblabel=None,
            qkeylength=None, qkeylabel=None, qkeypos=None, **kwargs):
        """
        Plots a scalar or a vector variable in a slice of the model
        
        Parameters
        ----------

            var_name        : str
                              Name of the variable in the SPH data set to be plotted

            incl            : float
                              Inclination angle (rotation around the y-axis in the SPH coordinate system, 2nd rotation)

            posang          : float
                              Position angle (rotation around the z-axis in the SPH coordinate system, 3rd rotation)

            phi             : float
                              Position angle (rotation around the z-axis in the SPH coordinate system, 1st rotation)

            xcent           : float
                              Center x-coordinate of the image in the SPH coordinate system
            
            ycent           : float
                              Center y-coordinate of the image in the SPH coordinate system
            
            zcent           : float
                              Center z-coordinate of the image in the SPH coordinate system
            
            image_size      : float
                              Full size of the image along the edge in unit of the SPH coordinates
            
            npix            : int
                              Number of pixels along the side of the image (currently only rectangular images are supported)

            ax              : matplotlib.axes.Axes
                              Matplotlib axes onto which the image should be drawn

            cax             : matplotlib.axes.Axes
                              Matplotlib axes onto which the colorbar should be drawn

            stretch         : {'linear', 'power', 'log'}
                              Color stretch of the image

            gamma           : float
                              Power exponent of the color scale if stretch='power' is selected

            vmin            : float
                              Lower boundary for the pixel intensity color scale 

            vmax            : float
                              Upper boundary for the pixel intensity color scale 
            
            cblabel         : str
                              Colorbar label
            
            qkeylength      : float
                              Length of the arrow in a legend in data units to be displayed 
                          
            qkeylabel       : str
                              Label of the arrow to be displayed in a legend
                          
            qkeypos         : list, tuple, ndarray
                              Two element array-like container with the position of the arrow legend in figure coordinates
                              first element is x position, second element is y posiiton (default=(0.9, 0.9))

            **kwargs        : keyword arguments 
                              Any further keyword arguments are passed to matplotlib.pcolormesh() if scalar is plotted or to 
                              matplotlib.quiver() if vectors are plotted
        
        Returns
        -------
            A list with two elements. 

            For scalars the first is a matplotlib.collections.QuadMesh returned by pcolormesh() used to
            display the projected scalar, while the second element is a matplotlib.colorbar.Colorbar instance             
            
            For vectors the first is a Quiver instance returned by quiver() used to display the vector field, 
            while the second element is a QuiverKey instance returned by quiverkey().
        """
        n_dim = 3
        #
        # Grab the active axes if ax is not set
        #
        if ax is None:
            ax = plt.gca()
        #
        # If the image size is not set, set it to be the largest extent of the SPH point set in any coordinate axis
        #
        if image_size is None:
            crd_bounds = []
            for idim in range(n_dim):
                crd_bounds.append(self.get_coordinate_axis_boundaries(idim))
                
            image_size = 0.
            for idim in range(n_dim):
                delta = (crd_bounds[idim][1] - crd_bounds[idim][0])
                if delta > image_size:
                    image_size = delta
       
        #
        # If the image center is not set, set it to be the origin (i.e. x=0, y=0, z=0)
        #
        if xcent is None:
            xcent = 0.0
        if ycent is None:
            ycent = 0.0
        if zcent is None:
            zcent = 0.0
   
        #
        # If the number of pixel is not set, use 500 pixels for scalars, and 50 pixels for vectors. 
        #  For vector variables 50 pixels is sufficient as we'll use quiver() to display the vector field
        #  and too densely packed arrows will not look that goo
        #
        if npix is None:
            if self.is_var_scalar(var_name):
                npix = 500
            else: 
                npix = 50

        if self.is_var_scalar(var_name):
            
            if cblabel is None:
                cblabel = var_name 
            
            if npix is None:
                npix = 500

            data = self._get_slice(var_name, xcent, ycent, zcent, image_size, npix, incl, posang, phi, npix*npix)
            data = data.reshape(npix, npix)
            x = np.linspace(xcent-image_size/2., xcent+image_size/2., npix)
            y = np.linspace(ycent-image_size/2., ycent+image_size/2., npix)
            pcm, cbar = self._plot_scalar_data(data, x, y, ax=ax, cax=cax, stretch=stretch, gamma=gamma, vmin=vmin, vmax=vmax, 
                                   cblabel=cblabel, **kwargs)

            return pcm, cbar
        
        else:
            
            if npix is None:
                npix = 50
            
            if qkeylabel is None:
                    qkeylabel = var_name 
               
            if qkeypos is None:
                qkeypos = (0.85, 0.9)
            else:
                if not isinstance(qkeypos, list):
                    if not isinstance(qkeypos, tuple):
                        if not isinstance(qkeypos, np.ndarray):
                            msg = 'qkeypos has the wrong data type. It should be a two element array-like object'\
                                 + '(i.e. list, tuple, ndarray)'
                            raise TypeError(msg)
                  
            if qkeylength is None:
                qkeylength = 1.0
                msg = 'Unkonwn qkeylength. The unit vector size in the legend is set to 1.0'  
                print(msg)


            data = self._get_slice(var_name, xcent, ycent, zcent, image_size, npix, incl, posang, phi, npix*npix*2)
            data = data.reshape(npix, npix, 2)
            x = np.linspace(xcent-image_size/2., xcent+image_size/2., npix)
            y = np.linspace(ycent-image_size/2., ycent+image_size/2., npix)
            xx, yy = np.meshgrid(x, y)
            qvr = ax.quiver(xx, yy, data[:, :, 0], data[:, :, 1], color='k', pivot='mid', scale=0.5*npix, **kwargs)

            vlength = np.sqrt(data[:, :, 0]**2 + data[:, :, 1]**2)
            qk = ax.quiverkey(qvr, qkeypos[0], qkeypos[1], qkeylength, qkeylabel, labelpos='E', coordinates='figure')
             
            return qvr, qk
        

    def plot_projected_scalar(self, var_name="", incl=0., posang=0., phi=0., xcent=None, ycent=None, zcent=None, image_size=None, 
            npix=None, ax=None, cax=None, stretch='linear', gamma=None, vmin=None, vmax=None, cblabel=None, **kwargs):
        """
        Projects scalars to a plane by integrating the scalars along the normal of the plane. 
        For density this results in column/surface density
        
        Parameters
        ----------

            var_name        : str
                              Name of the variable in the SPH data set to be plotted

            incl            : float
                              Inclination angle (rotation around the y-axis in the SPH coordinate system, 2nd rotation)

            posang          : float
                              Position angle (rotation around the z-axis in the SPH coordinate system, 3rd rotation)

            phi             : float
                              Position angle (rotation around the z-axis in the SPH coordinate system, 1st rotation)

            xcent           : float
                              Center x-coordinate of the image in the SPH coordinate system
            
            ycent           : float
                              Center y-coordinate of the image in the SPH coordinate system
            
            zcent           : float
                              Center z-coordinate of the image in the SPH coordinate system
            
            image_size      : float
                              Full size of the image along the edge in unit of the SPH coordinates
            
            npix            : int
                              Number of pixels along the side of the image (currently only rectangular images are supported)

            ax              : matplotlib.axes.Axes
                              Matplotlib axes onto which the image should be drawn

            cax             : matplotlib.axes.Axes
                              Matplotlib axes onto which the colorbar should be drawn

            stretch         : {'linear', 'power', 'log'}
                              Color stretch of the image

            gamma           : float
                              Power exponent of the color scale if stretch='power' is selected

            vmin            : float
                              Lower boundary for the pixel intensity color scale 

            vmax            : float
                              Upper boundary for the pixel intensity color scale 

            cblabel         : str
                              Colorbar label

            **kwargs        : keyword arguments 
                              Any further keyword arguments are passed to matplotlib.plot
        
        Returns
        -------
            A list with two elements: the first is a matplotlib.collections.QuadMesh returned by pcolormesh() used to
            display the projected scalar, while the second element is a matplotlib.colorbar.Colorbar instance             
        """
        n_dim = 3

        #
        # Grab the active axes if ax is not set
        #
        if ax is None:
            ax = plt.gca()
        #
        # If the image size is not set, set it to be the largest extent of the SPH point set in any coordinate axis
        #
        if image_size is None:
            crd_bounds = []
            for idim in range(n_dim):
                crd_bounds.append(self.get_coordinate_axis_boundaries(idim))
                
            image_size = 0.
            for idim in range(n_dim):
                delta = (crd_bounds[idim][1] - crd_bounds[idim][0])
                if delta > image_size:
                    image_size = delta
        
        #
        # If the image center is not set, set it to be the origin (i.e. x=0, y=0, z=0)
        #
        if xcent is None:
            xcent = 0.0
        if ycent is None:
            ycent = 0.0
        if zcent is None:
            zcent = 0.0
        
        #
        # Set the number of pixels to 500 if it's not specified
        #
        if npix is None:
            npix = 500
        
        if cblabel is None:
            cblabel = "projected " + var_name 

        if self.is_var_scalar(var_name):
            if npix is None:
                npix = 500
            data = self._project_scalar(var_name, xcent, ycent, image_size, npix, incl, posang, phi, npix*npix)
            data = data.reshape(npix, npix)
            x    = np.linspace(xcent-image_size/2., xcent+image_size/2., npix)
            y    = np.linspace(ycent-image_size/2., ycent+image_size/2., npix)
            pcm, cbar = self._plot_scalar_data(data, x, y, ax=ax, cax=cax, stretch=stretch, gamma=gamma, vmin=vmin, 
                                               vmax=vmax, cblabel=cblabel, **kwargs)
        else:
            raise RuntimeError("No scalar variable has been found with name " + var_name) 
       
        return pcm, cbar

%}


