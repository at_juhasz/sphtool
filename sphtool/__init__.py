# -*- coding: utf-8 -*-

__version__ = '0.2'
__author__ = 'Attila Juhasz (juhasz@ast.cam.ac.uk)'
__copyright__ = 'Copyright 2018 Attila Juhasz'

__all__ = ['SphTool']

from .sphtool import SphTool

