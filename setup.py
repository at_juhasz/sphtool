from distutils.core import setup
import sphtool
setup(name = 'sphtool',
      version = sphtool.__version__,
      author = "Attila Juhasz",
      author_email="juhasz@ast.cam.ac.uk",
      description = """Regridding and visualisation for SPH simulations""",
      packages=['sphtool'],
      package_data={'sphtool': ["_sphtool.so"]},
      license = "GPLv2")
