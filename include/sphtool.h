// ============================================================================
/// \file sphtool.h
/// \brief Python interface
//
// This file is part of sphtool:
// Gridding and visualisation package for SPH simulations
//
// Copyright (C) 2018 Attila Juhasz
// Author: Attila Juhasz
//
// sphtool is free software; you can redistribute it and/or modify it under
// the terms of the GNU General Public License as published by the Free Software
// Foundation; either version 2 of the License, or (at your option) any later
// version.
//
// sphtool is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.
// ============================================================================

#ifndef INCLUDE_SPHTOOL_H_
#define INCLUDE_SPHTOOL_H_

#include <vector>
#include <map>
#include <string>
#include "../include/point_data.h"
#include "../include/tree.h"
#include "../include/regular_grid.h"
#include "../include/amr_grid.h"
#include "../include/sph_data.h"
#include "../include/gridded_data.h"
///
/// \brief Python interface class
///
class SPHTool : public SPHData {
 public:
  SPHTool();
  virtual ~SPHTool();

  ///
  /// \brief Sets a parameter
  /// @param[in] param_name         Name of the parameter
  /// @param[in] param_value        Parameter value
  ///
  void set_param_int(const std::string& param_name, const int& param_value);
  ///
  /// \brief Returns a parameter value
  /// @param param_name             Name of the parameter
  /// @return A parameter value
  int get_param_int(const std::string& param_name);
  ///
  /// \brief Returns the list of parameter names
  ///
  std::vector<std::string> get_param_list();
  ///
  /// \brief Performs SPH interpolation of a scalar or vector variable to
  /// an array of points given by their coordinates
  ///
  /// @param[in] var_name             Name of the variables
  /// @param[in] xi                   Array of coordinates in the first
  ///                                 dimension to interpolate to
  /// @param[in] nx                   Size of the first coordinate array
  /// @param[in] yi                   Array of coordinates in the second
  ///                                 dimension to interpolate to
  /// @param[in] ny                   Size of the second coordinate array
  ///                                 (should be equal to nx)
  /// @param[in] zi                   Array of coordinates in the third
  ///                                 dimension to interpolate to
  /// @param[in] nz                   Size of the third coordinate array
  ///                                 (should be equal to nx)
  /// @param[out] vector_double_out   Array of interpolated variables
  /// @param[out] dim_out             Size of the interpolated array (equal to
  ///                                 nx)
  void _interpolate(const std::string& var_name, double* xi, int nxi,
                    double* yi, int nyi, double* zi, int nzi,
                    double* vector_double_out, int dim_out);
  ///
  /// \brief Sets the grid (for re-gridding SPH variables to a regular/AMR type
  ///        grid)
  ///
  /// @param[in] grid_type          Type of the grid : "amr" or "regular"
  /// @param[in] xi                 Cell interfaces in the first dimension for
  ///                               regular grids, for AMR grids it is the cell
  ///                               interface of the root node in the first
  ///                               dimension
  /// @param[in] nx                 Number of cells in the first dimension (i.e.
  ///                               length of xi-1)
  /// @param[in] yi                 Cell interfaces in the second dimension for
  ///                               regular grids, for AMR grids it is the cell
  ///                               interface of the root node in the second
  ///                               dimension
  /// @param[in] ny                 Number of cells in the second dimension
  ///                               (i.e. length of yi-1)
  /// @param[in] zi                 Cell interfaces in the third dimension for
  ///                               regular grids, for AMR grids it is the cell
  ///                               interface of the root node in the thid
  ///                               dimension
  /// @param[in] nz                 Number of cells in the third dimension (i.e.
  ///                               length of zi-1)
  /// @param[in] crd_system         Coordinate system ("cartesian", "spherical",
  ///                               or "cylindrical")
  ///
  void _set_grid(const std::string& grid_type, double* xi, int nxi,
                 double* yi, int nyi, double* zi, int nzi,
                 const std::string& crd_system);

  ///
  /// \brief Re-maps SPH-point based variables to a regular/AMR type grid
  ///
  /// @param[in] var_name           Name of the variable to be re-gridded
  void _regrid(const std::vector<std::string>& var_name);

  void _refine_amr_gradient(const int& n_trial_point, const double& threshold,
                            const double& density_floor,
                            const int& max_amr_grid_depth);
  ///
  /// \brief Returns a gridded variable
  ///
  /// @param[in] var_name           Name of the gridded variable
  /// @param[out] vector_double_out Output array containing the gridded variable
  /// @param[in] dim_out            Number of elements in vector_double_out
  ///
  void _get_gridded_var(const std::string& var_name, double* vector_double_out,
                        int dim_out);
  ///
  /// \brief Writes a gridded variable to file
  ///
  /// @param[in] file_name          Name of the variable to be written to file
  /// @param[in] format             File format (currently only "radmc3d" is
  ///                               supported)
  /// @param[in] var_name           Name of the variable
  /// @param[in] is_format_binary   Should the file be written as binary (True)
  ///                               or formatted ASCII (False)
  /// @param[in] scalar_floor       Floor value for scalar variables
  ///
  void _write_gridded_var(const std::string& file_name,
                          const std::string& format,
                          const std::string& var_name,
                          const bool& is_format_binary,
                          const double& scalar_floor);

  ///
  /// \brief Writes multiple (scalar) variables to file
  ///
  /// @param[in] file_name          Name of the variable to be written to file
  /// @param[in] format             File format (currently only "radmc3d" is
  ///                               supported)
  /// @param[in] var_name           Vector of variable names to be written to
  ///                               the same file
  /// @param[in] is_format_binary   Should the file be written as binary (True)
  ///                               or formatted ASCII (False)
  /// @param[in] scalar_floor       Floor value for scalar variables
  ///
  void _write_gridded_var_multicomp(const std::string& file_name,
                                    const std::string& format,
                                    const std::vector<std::string>& var_name,
                                    const bool& is_format_binary,
                                    const double& scalar_floor);
  ///
  /// \brief Returns the number of grid cells in a dimension
  ///
  /// For AMR grids the number of grid cells in a given dimension is not well
  /// defined, by constructions. Therefore the returned value is always the
  /// total number of leaf cells irrespectively of the axis/dimension selected!
  ///
  /// @param iaxis                  Coordinate axis/dimension
  /// @return the number of grid cells in a given axis/dimension
  ///
  int _get_grid_n_cell(const int& iaxis);

  ///
  /// \brief Returns the grid cell centers along a given axis/dimension
  ///
  /// For AMR grids the number of grid cells in a given dimension is not well
  /// defined, by constructions. Therefore, for AMR grids, the returned values
  /// are the coordinates of the leaf cells in the given dimension.
  ///
  /// @param[in] iaxis                Coordinate axis/dimension
  /// @param[out] vector_double_out   Array containing the cell center
  ///                                 coordinates
  /// @param[in] dim_out              Number of elements in vector_double_out
  void _get_grid_cell_centers(const int& iaxis, double* vector_double_out,
                          int dim_out);

  ///
  /// \brief Writes the spatial grid to file (in case re-gridding the SPH to a
  ///           regular or AMR grid)
  /// @param[in] file_name            Name of the file to write the grid into
  /// @param[in] format               Format of the file (currently only
  ///                                 "radmc3d" is accepted)
  void write_grid(const std::string& file_name, const std::string& format);
  ///
  /// \brief Calculates a projection of scalar variables (i.e. integrating the
  ///         variable along a 1D line)
  ///
  /// @param[in] var_name             Name of the variable to project
  /// @param[in] image_center_x       Cartesian x coordinate of the image center
  /// @param[in] image_center_y       Cartesian y coordinate of the image center
  /// @param[in] image_size           Size of the (square) image along the edge
  /// @param[in] npix                 Number of pixels along the edge of the
  ///                                 square image
  /// @param[in] incl_deg             Inclination angle of the slice in degrees
  ///                                 (Rotation around the cartesian y axis of
  ///                                 the model coordinate system)
  /// @param[in] posang_deg           Position angle in degrees
  ///                                 (Rotation around the normal of the image
  ///                                 plane)
  /// @param[in] phi_deg              Azimuth angle in degrees (Rotation around
  ///                                 the cartesian z axis of the model
  ///                                 coordinate system)
  /// @param[out] vector_double_out   Array containing the projected variable
  /// @param[in] dim_out              Size (number of elements) of
  ///                                 vector_double_out
  void _project_scalar(const std::string& var_name,
                       const double& image_center_x,
                       const double& image_center_y, const double& image_size,
                       const int& npix, const double& incl_deg,
                       const double& posang_deg, const double& phi_deg,
                       double* vector_double_out, int dim_out);
  ///
  /// \brief Calculates a 2D slice through the model
  ///
  /// @param[in] var_name             Name of the variable to calculate the
  ///                                 slice for
  /// @param[in] slice_xcent          Cartesian x coordinate of the slice center
  /// @param[in] slice_ycent          Cartesian y coordinate of the slice center
  /// @param[in] slice_zcent          Cartesian z coordinate of the slice center
  /// @param[in] slice_size           Total size of the slice along the edge
  ///                                 (the image will be square)
  /// @param[in] n_pixel              Number of pixels along the edge of the
  ///                                 (square) image
  /// @param[in] incl_deg             Inclination angle of the slice in degrees
  ///                                 (Rotation around the cartesian y axis of
  ///                                 the model coordinate system)
  /// @param[in] posang_deg           Position angle in degrees
  ///                                 (Rotation around the normal of the image
  ///                                 plane)
  /// @param[in] phi_deg              Azimuth angle in degrees (Rotation around
  ///                                 the cartesian z axis of the model
  ///                                 coordinate system)
  /// @param[out] vector_double_out   Array containing the sliced variable
  /// @param[in] dim_out              Size (number of elements) of
  ///                                 vector_double_out
  void _get_slice(const std::string& var_name, const double& slice_xcent,
                  const double& slice_ycent, const double& slice_zcent,
                  const double& slice_size, const int& n_pixel,
                  const double& incl_deg, const double& posang_deg,
                  const double& phi_deg, double* vector_double_out,
                  int dim_out);

  ///
  /// \brief Writes gridded variable(s) to a legacy VTK file
  ///
  /// @param[in] file_name              Name of the file
  /// @param[in] var_name               Name(s) of the variable(s) to be written
  ///                                   in the file
  ///
  void _write_vtk(const std::string& file_name,
                  const std::vector<std::string>& var_name);

  ///
  /// \brief Finalize the SPH data input (validates containers and builds tree)
  ///
  void finalize();

 private:
  // Parameters
  std::map<std::string, int> param_int_;
  // amr or regular grid
  std::string grid_type_;
  // Data stored on the regular grid
  GriddedDataSet<RegularGrid> regular_data_;
  // Data stored on the AMR grid
  GriddedDataSet<AMRGrid> amr_data_;
};

#endif /* INCLUDE_SPHTOOL_H_ */
