// ============================================================================
/// \file sph_data.h
/// \brief Containers for SPH particle sets
//
// This file is part of sphtool:
// Gridding and visualisation package for SPH simulations
//
// Copyright (C) 2018 Attila Juhasz
// Author: Attila Juhasz
//
// sphtool is free software; you can redistribute it and/or modify it under
// the terms of the GNU General Public License as published by the Free Software
// Foundation; either version 2 of the License, or (at your option) any later
// version.
//
// sphtool is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.
// ============================================================================

#ifndef INCLUDE_SPH_DATA_H_
#define INCLUDE_SPH_DATA_H_

#include <vector>
#include <string>
#include <map>
#include <stdexcept>
#include "../include/tree.h"
#include "../include/kernels.h"
#include "../include/point_data.h"
#include "../include/gridded_data.h"

///
/// \brief Container for an SPH particle set
///
class SPHData : public PointDataSet {
 public:
  /// \brief Constructor
  SPHData();
  /// \brief Constructor
  /// @param[in] n_point    Number of points in the set
  /// @param[in] n_dim      Number of spatial dimensions
  SPHData(const int& n_point, const int& n_dim);
  /// \brief Destructor
  virtual ~SPHData();
  /// \brief Validates the data set
  /// Currently it only checks if three mandatory scalars have already been
  /// defined in the data set: density, smoothing length and particle mass.
  /// These scalar variables are needed to perform SPH kernel based
  /// interpolation, therefore they all must be defined.
  void validate();

#ifndef TEST_BUILD

 protected:
#endif
  ///
  /// \brief Initializes a tree over the particle set
  /// @param[in] max_particle_per_cell
  /// @param[in] max_tree_depth
  void InitTree(const int& max_particle_per_cell, const int& max_tree_depth);
  ///
  /// \brief  Performs SPH interpolation on a single scalar or vector variable
  ///
  /// This interpolator does not uses the SPH tree, but rather splits up the
  /// model space to n_box_x, n_box_y, n_box_z number of bounding boxes. Then
  /// associates each SPH particle with the bounding box it falls into and
  /// calculates the effective size of the bounding box. The effective size is
  /// the region of space that is affected by the particles in the
  /// bounding box. This is calculated for each coordinate separately. I.e.
  /// for first coordinate the effective coordinates of the bounding box are
  /// min(x_i - h_i), max(x_i +  h_i) where x_i is the first coordinate of the
  /// ith particle in the bounding box, and h_i is the smoothing length of the
  /// ith particle in the bounding box.
  ///
  /// @param[in] var_name       Name of the variable to be interpolated
  /// @param[in] xi             Cell interfaces in the first coordinate of the
  ///                           points the variable need to be interpolated to
  /// @param[in] yi             Cell interfaces in the second coordinate of the
  ///                           points the variable need to be interpolated to
  /// @param[in] zi             Cell interfaces in the third coordinate of the
  ///                           points the variable need to be interpolated to
  /// @param[in] n_point_int    Number of points to be interpolated to (i.e.size
  ///                           of the xi/yi/zi arrays)
  /// @param[in] n_thread       Number of parallel threads to be used during the
  ///                           interpolation
  /// @param[out] var_out       Array of the output, interpolated variable. It
  ///                           will have the same size as xi/yi/zi.
  ///
  void InterpolateSingleVar(std::string var_name, double* xi, double* yi,
                            double* zi, const int& n_point_int,
                            const int& n_thread, double* var_out);

  ///
  /// \brief Re-grid particle data (single scalar or vector variable) to a
  /// regular or amr grid
  ///
  /// Since the regular grid might not be uniform (e.g. a spherical grid with
  /// logarithmic radial coordinate) this interpolator converts the regular grid
  /// to a point set and builds a tree over it. Then it loops over all SPH
  /// particles and uses the tree over the cell centers of the regular grid to
  /// search for all cells whose centers are within a radius of twice of the
  /// smoothing length of that SPH particle. Finally adds the contribution of
  /// the SPH particle to all these regular grid cells using the SPH kernel.
  ///
  /// @param[in] var_name                 Name(s) of the scalar or vector
  ///                                     variable(s) to be re-gridded
  /// @param[in] max_particle_per_cell    Maximum number of particles per tree
  ///                                     node
  /// @param[in] max_tree_depth           Maximum tree depth
  /// @param[in] n_threads                Number of threads used during the
  ///                                     gridding
  /// @param[in] grid_type                Grid type "regular" or "amr"
  /// @param[in, out] ptr_grid_set        Point data set generated from a
  ///                                     regular or amr grid, containing the
  ///                                     centers of the grid cells as particles
  ///
  void ReGrid(const std::vector<std::string>& var_name,
              const int& max_particle_per_cell, const int& max_tree_depth,
              const int& n_threads, const std::string& grid_type,
              PointDataSet* ptr_grid_set);

//  void ReGridOld(const std::vector<std::string>& var_name,
//              const int& max_particle_per_cell, const int& max_tree_depth,
//              const int& n_threads, const std::string& grid_type,
//              PointDataSet* ptr_grid_set);

  ///
  /// \brief  Performs a line-of-sight integration of a scalar variable to
  ///         compute e.g the surface density on a 2D image.
  ///
  /// This function performs a line-of-sight integration of a scalar variable
  /// along any arbitrary direction.
  ///
  /// @param[in] scalar_name          Name of the scalar variable
  /// @param[in] xcent                First coordinate of the image center
  /// @param[in] ycent                Second coordinate of the image center
  /// @param[in] incl_rad             Inclination angle in radian
  /// @param[in] posang_rad           Position angle in radian
  /// @param[in] phi_rad              Azimuth angle in radian
  /// @param[in] n_thread             Number of parallel threads to be used
  /// @param[out] projected_scalar    Array containing the projected variable
  /// @return Returns a 2D image with the projected scalar
  void ProjectScalarToPlane(const std::string& scalar_name,
                            const std::vector<double>& xcent,
                            const std::vector<double>& ycent,
                            const double& incl_rad, const double& posang_rad,
                            const double& phi_rad, const int& n_thread,
                            double* projected_scalar);
  ///
  /// \brief Calculates a 2D slice through the model
  ///
  /// @param[in] var_name              Name of the variable to calculate the
  ///                                  slice of
  /// @param[in] slice_xcent           Cartesian x coordinate of the center
  ///                                  of the slice
  /// @param[in] slice_ycent           Cartesian y coordinate of the center of
  ///                                  the slice
  /// @param[in] slice_zcent           Cartesian y coordinate of the center of
  ///                                  the slice
  /// @param[in] n_pixel_x             Number of horizontal pixels in the slice
  ///                                  image
  /// @param[in] n_pixel_y             Number of vertical pixels in the slice
  ///                                  image
  /// @param[in] delta_x               Horizontal pixel size in the slice
  /// @param[in] delta_y               Vertical pixels ize in the slice
  /// @param[in] incl_rad              Inclination angle of the slice in radian
  ///                                  (rotation around the cartesian y axis of
  ///                                  the SPH coordinate system)
  /// @param[in] posang_rad            Position angle of the slice in radian
  ///                                  (rotation around the normal of the slice
  ///                                  image)
  /// @param[in] phi_rad               Azimuth angle of the slice in radian
  ///                                  (rotation around the cartesian z axis of
  ///                                  the SPH coordinate system)
  /// @param[in] n_thread              Number of parallel threads to be used
  /// @param[out] var_slice            Output array containing the variable in
  ///                                  the slice
  void GetVarSlice(const std::string& var_name, const double& slice_xcent,
                   const double& slice_ycent, const double& slice_zcent,
                   const int& n_pixel_x, const int& n_pixel_y,
                   const double& delta_x, const double& delta_y,
                   const double& incl_rad, const double& posang_rad,
                   const double& phi_rad, const int& n_thread,
                   double* var_slice);

#ifdef TEST_BUILD
  PointData GetPointData(const int& ipoint);
#endif

 private:
  // Tree over the particles
  Tree<double> tree_;
  // Smoothing kernel
  KernelBase* ptr_smoothing_kernel_;
};

#endif /* INCLUDE_SPH_DATA_H_ */
