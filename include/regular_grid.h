// ============================================================================
/// \file regular_grid.h
/// \brief Containers for regular grid and data stored on a regular grid
//
// This file is part of sphtool:
// Gridding and visualisation package for SPH simulations
//
// Copyright (C) 2018 Attila Juhasz
// Author: Attila Juhasz
//
// sphtool is free software; you can redistribute it and/or modify it under
// the terms of the GNU General Public License as published by the Free Software
// Foundation; either version 2 of the License, or (at your option) any later
// version.
//
// sphtool is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.
// ============================================================================

#ifndef REGULAR_GRID_H_
#define REGULAR_GRID_H_

#include <vector>
#include <string>
#include <map>
#include "../include/grid_base.h"
#include "../include/point_data.h"

///
/// \brief Container for a regular grid
///
class RegularGrid : public GridBase {
 public:
  /// \brief Constructor
  RegularGrid();
  /// \brief Destructor
  virtual ~RegularGrid();
  ///
  /// \brief Generates a single coordinate axis
  ///
  /// Refinement of mesh resolution is allowed along individual axes. I.e. the
  /// number of grid cells along the axis can be set in an arbitrary number of
  /// segments separately
  ///
  /// @param[in] iaxis                    Axis index (Cartesian: x=0, y=1, z=2,
  ///                                     Spherical : r=0, theta=1, phi=2)
  /// @param[in] axis_segment_bounds      Segment boundaries. For N number of
  ///                                     segments N+1 number of segment
  ///                                     boundaries should be defined. The
  ///                                     Number of grid points/cells can then
  ///                                     be set for each segment separately.
  /// @param[in] n_bounds                 Size of axis_segment_bounds
  /// @param[in] n_cell_in_segment        Number of grid cells in each
  ///                                     individual segment. The i-th element
  ///                                     of n_cell_in_segment sets the number
  ///                                     of grid cells between the i-th and
  ///                                     i+1-th axis segment boundary
  /// @param[in] n_segment                Number of segments
  void GenerateAxis(const int& iaxis, double* axis_segment_bounds,
                    const int& n_bounds, int* n_cell_in_segment,
                    const int& n_segment);

  /// \brief Returns the number of grid points / cells in the ith axis
  /// @param iaxis                        Axis index
  /// @return Returns the number of grid points / cells in the ith axis
  int GetNCell(const int& iaxis);
  /// \brief Returns the total number of grid points / cells (nx*ny*nz)
  /// @param iaxis                        Axis index
  /// @return the total number of grid points / cells
  int GetNCell();
  /// \brief Returns the number of cell interfaces in the ith axis
  /// @param iaxis                        Axis index
  /// @return the number of cell interfaces in the ith axis
  int GetNInterface(const int& iaxis);
  ///
  /// \brief Sets a coordinate axis based on a vector of cell interfaces
  /// @param[in] iaxis                    Axis index (Cartesian: x=0, y=1, z=2,
  ///                                     Spherical: r=0, theta=1, phi=2)
  /// @param[in] vector_double_in         Cell interface array
  /// @param[in] dim                      Size of the cell interface array
  void SetCellInterface(const int& iaxis, double* vector_double_in,
                    const int& dim);
  ///
  /// \brief  Returns the cell interface array of a given coordinate axis
  /// @param[in] iaxis                    Axis index (Cartesian: x=0, y=1, z=2,
  ///                                     Spherical: r=0, theta=1, phi=2)
  /// @param[in] vector_double_in         Cell interface array
  /// @param[in] dim                      Size of the cell interface array
  void GetCellInterface(const int& iaxis, double* vector_double_out,
                    const int& dim);
  ///
  /// \brief Sets a coordinate axis based on a vector of cell centers
  /// @param[in] iaxis                    Axis index (Cartesian: x=0, y=1, z=2,
  ///                                     Spherical: r=0, theta=1, phi=2)
  /// @param[in] vector_double_in         Cell center array
  /// @param[in] dim                      Size of the cell center array
  void SetCellCenter(const int& iaxis, double* vector_double_in,
                    const int& dim);
  ///
  /// \brief Returns the cell center array of a given coordinate axis
  /// @param[in] iaxis                    Axis index (Cartesian: x=0, y=1, z=2,
  ///                                     Spherical: r=0, theta=1, phi=2)
  /// @param[in] vector_double_in         Cell center array
  /// @param[in] dim                      Size of the cell center array
  void GetCellCenter(const int& iaxis, double* vector_double_out,
                    const int& dim);

  ///
  /// \brief Returns the grid type ("amr" or "regular")
  /// @return the grid type ("amr" or "regular")
  std::string GetGridType();


  ///
  /// \brief Writes the grid to file
  ///
  /// @param[in] file_name                Name of the file to write the grid to
  /// @param[in] format                   Format of the file (currently only
  ///                                     "radmc3d" is supported)
  void WriteToFile(const std::string& file_name, const std::string& format);
  ///
  /// \brief Read the grid to from file
  ///
  /// @param[in] file_name                Name of the file to write the grid to
  /// @param[in] format                   Format of the file (currently only
  ///                                     "radmc3d" is supported)
  void ReadFromFile(const std::string& file_name, const std::string& format);
  ///
  /// \brief Sets the output precision for formatted ASCII output
  /// @param[in] precision                Number of significant digits in
  ///                                     exponential notation
  void SetOutputPrecision(const int& precision);
  ///
  /// \brief Returns the output precision (number of significant digits) in
  /// formatted ASCII output
  ///
  int GetOutputPrecision();
  ///
  /// \brief Resets the grid container
  ///
  void ResetGrid();

 private:
  // Number of cells in the first dimension
  int n_x_;
  // Number of cells in the second dimension
  int n_y_;
  // Number of cells in the third dimension
  int n_z_;

  // Cell centers in the first dimension
  std::vector<double> x_;
  // Cell centers in the second dimension
  std::vector<double> y_;
  // Cell centers in the third dimension
  std::vector<double> z_;
  // Cell interfaces in the first dimension
  std::vector<double> xi_;
  // Cell interfaces in the second dimension
  std::vector<double> yi_;
  // Cell interfaces in the third dimension
  std::vector<double> zi_;
  // Grid type
  static const char *grid_type_;
  // Output precision
  int output_ascii_precision_;
};

#endif /* REGULAR_GRID_H_ */
