// ============================================================================
/// \file kernels.h
/// \brief SPH interpolation kernels
//
// This file is part of sphtool:
// Gridding and visualisation package for SPH simulations
//
// Copyright (C) 2018 Attila Juhasz
// Author: Attila Juhasz
//
// sphtool is free software; you can redistribute it and/or modify it under
// the terms of the GNU General Public License as published by the Free Software
// Foundation; either version 2 of the License, or (at your option) any later
// version.
//
// sphtool is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.
// ============================================================================


#ifndef INCLUDE_KERNELS_H_
#define INCLUDE_KERNELS_H_

#include <cmath>
#include <algorithm>
#include <vector>
#include <string>

///
/// \brief Smoothing kernel base
///
class KernelBase {
 public:
  ///
  /// \brief KernelBase constructor
  ///
  KernelBase();
  ///
  /// \brief KernelBase destructor
  ///
  virtual ~KernelBase();
  ///
  /// \brief KernelBase weight function
  ///
  /// @param[in] q    Dimensionless distance (r / h)
  ///
  virtual double W(const double& q);
  /// \brief Generates a lookup table for integrals of SPH kernels
  /// When calculating line-of-sight integrated quantities (e.g. surface
  /// density) we need to calculate the integral along the line of sight of the
  /// smoothing kernel. Since the kernel is spherically symmetric what we need
  /// to calculate is integral_-inf^+inf K(r)dy, where r = sqrt(x^2 + y^2). To
  /// speed up the calculation these integrals are pre-calculated for a range of
  /// "x" values between 0 and 2. The upper limit is 2.0 since, for r>2.0 the
  /// value of the kernel is 0.0 everywhere for the currently implemented cubic
  /// spline kernel.
  ///
  /// @param[in] n_integral     Number of points to numerically calculate the
  ///                           integral
  /// @param[in] n_ki_table     Size of the lookup table (number of elements
  ///                           between 0 and 2)
  virtual void InitWIntegralTable(const int& n_integral, const int& n_ki_table);
  ///
  /// \brief Evaluates the 1d integral through the smoothing kernel
  /// @param x                  Impact parameter / perpendicular distance
  ///                           between the line of integration and the center
  ///                           of the kernel
  ///
  inline double GetWIntegral(const double& x);
  ///
  /// \brief Sets the number of spatial dimensions
  /// @param n_dim              Number of spatial dimensions
  ///
  void SetNDim(const int& n_dim);
  ///
  /// \brief Returns the number of spatial dimensions
  ///
  int GetNDim();

 protected:
  // Number of spatial dimensions
  int n_dim_;
  // Kernel normalisation factor
  double norm_;
  // Size (number of elements) of the kernel integral lookup table
  double n_ki_table_double_;
  // Number of steps to take when numerically evaluating the integral
  int n_integral_;
  // Size (number of elements) of the kernel integral lookup table
  int n_ki_table_;
  // Kernel integral table : impact parameter / perpendicular distance
  //  of the SPH point from the line of integration
  std::vector<double> ki_table_x_;
  // Kernel integral table : integrated smoothing kernel along a line
  // with a given impact parameter / perpendicular distance from the point
  std::vector<double> ki_table_fx_;
};
// ------------------------------------------------------------------------
//
// ------------------------------------------------------------------------
inline double KernelBase::GetWIntegral(const double& x) {
  int kit_index = std::lround(x / 2.0 * n_ki_table_double_) - 1;
  kit_index = std::max(kit_index, 0);
  return ki_table_fx_[kit_index];
}

///
/// \brief Cubic spline kernel
///
class CubicSplineKernel : public KernelBase {
 public:
  ///
  /// \brief CubicSpKernel constructor
  ///
  explicit CubicSplineKernel(const int& n_dim);
  ///
  /// \brief CubicSpKernel destructor
  ///
  virtual ~CubicSplineKernel();
  ///
  /// \brief KernelBase weight function
  ///
  /// @param[in] q    Dimensionless distance (r / h)
  ///
  double W(const double& q);
  ///
  /// \brief Generates a lookup table for integrals of SPH kernels
  /// When calculating line-of-sight integrated quantities (e.g. surface
  /// density) we need to calculate the integral along the line of sight of the
  /// smoothing kernel. Since the kernel is spherically symmetric what we need
  /// to calculate is integral_-inf^+inf K(r)dy, where r = sqrt(x^2 + y^2). To
  /// speed up the calculation these integrals are pre-calculated for a range of
  /// "x" values between 0 and 2. The upper limit is 2.0 since, for r>2.0 the
  /// value of the kernel is 0.0 everywhere for the currently implemented cubic
  /// spline kernel.
  ///
  /// @param[in] n_integral     Number of points to numerically calculate the
  ///                           integral
  /// @param[in] n_ki_table     Size of the lookup table (number of elements
  ///                           between 0 and 2)
  void InitWIntegralTable(const int& n_integral, const int& n_ki_table);
  ///
  /// \brief Evaluates the 1d integral through the smoothing kernel
  /// @param x                  Impact parameter / perpendicular distance
  /// between
  ///                           the line of integration and the center of the
  ///                           kernel
  ///
};
#endif /* INCLUDE_KERNELS_H_ */
