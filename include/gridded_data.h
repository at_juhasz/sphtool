// ============================================================================
/// \file gridded_data.h
/// \brief Container for data defined on a grid
//
// This file is part of sphtool:
// Gridding and visualisation package for SPH simulations
//
// Copyright (C) 2018 Attila Juhasz
// Author: Attila Juhasz
//
// sphtool is free software; you can redistribute it and/or modify it under
// the terms of the GNU General Public License as published by the Free Software
// Foundation; either version 2 of the License, or (at your option) any later
// version.
//
// sphtool is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.
// ============================================================================


#ifndef INCLUDE_GRIDDED_DATA_H_
#define INCLUDE_GRIDDED_DATA_H_

#include <utility>
#include <string>
#include <vector>
#include <algorithm>
#include <fstream>
#include <iomanip>
#include <cmath>
#include <stdexcept>
#include "../include/point_data.h"
#include "../include/regular_grid.h"
#include "../include/amr_grid.h"

///
/// \brief Container for data defined on a grid
///
template <class T>
class GriddedDataSet : public PointDataSet {
 public:
  /// \brief Constructor
  GriddedDataSet();
  /// \brief Constructor
  explicit GriddedDataSet(T* ptr_grid);
  /// \brief Destructor
  virtual ~GriddedDataSet();
  ///
  /// \brief Sets the regular grid the data is defined on
  /// @param[in] reg_grid          AMR grid the data is defined on
  void SetGrid(T* grid);
  ///  @return Returns the AMR grid the data is defined on
  T GetGrid();
  ///
  /// \brief Returns the number of cells in the grid
  /// @param[in] iaxis             Coordinate axis in which cells should be
  ///                              counted
  ///
  int GetNCell(int iaxis);
  ///
  /// \brief Returns the cell center coordinates in a given coordiante axis
  /// @param[in] iaxis                Coordinate axis
  /// @param[out] vector_double_out   Cell centers in the requested coordinate
  ///                                 axis
  /// @param[in] dim_out              Size of (number of elements in)
  ///                                 vector_double_out
  ///
  void GetCellCenter(const int& iaxis, double* vector_double_out,
                     const int& dim_out);
  ///
  /// \brief Returns the coordinate system the grid is defined in
  ///
  /// 0-99 : Cartesian, 100-199 - Spherical, 200-299 - Cylindrical
  ///
  std::string GetCrdSystem();
  ///
  /// \brief Sets the output precision for formatted ASCII output
  /// @param[in] precision                Number of significant digits in
  ///                                     exponential notation
  void SetOutputPrecision(const int& precision);
  ///
  /// \brief Returns the output precision (number of significant digits) in
  /// formatted ASCII output
  ///
  int GetOutputPrecision();
  ///
  /// \brief Writes data to file (multiple scalar variables to single file)
  ///
  /// This function writes scalar variables, which meant to have multiple
  /// components to a single file. An example of this is density of dust
  /// particles with different sizes.
  ///
  /// @param[in] file_name            Name of the file to write the variables to
  /// @param[in] format               File format (currently only "radmc3d" is
  ///                                 accepted)
  /// @param[in] var_name             Name(s) of the variable(s) to be written
  ///                                 in the file
  /// @param[in] is_format_binary     Should the output be binary (true) or
  ///                                 formatted ASCII (false)
  /// @param[in] scalar_floor         Floor value for all variables
  ///
  void WriteData(const std::string& file_name, const std::string& format,
                   const std::vector<std::string>& var_name,
                   const bool& is_format_binary,
                   const double& scalar_floor);
  ///
  /// \brief Writes data to file
  ///
  /// This function writes a single scalar or vector variable to a file
  ///
  /// @param[in] file_name            Name of the file to write the variables to
  /// @param[in] format               File format (currently only "radmc3d" is
  ///                                 accepted)
  /// @param[in] var_name             Name of the variable to be written
  ///                                 in the file
  /// @param[in] is_format_binary     Should the output be binary (true) or
  ///                                 formatted ASCII (false)
  /// @param[in] scalar_floor         Floor value of the variable (for scalars
  ///                                 only)
  ///
  void WriteData(const std::string& file_name, const std::string& format,
                   const std::string& var_name,
                   const bool& is_format_binary,
                   const double& scalar_floor);

  ///
  /// \brief Writes the grid to file
  ///
  /// @param[in] file_name                Name of the file to write the grid to
  /// @param[in] format                   Format of the file (currently only
  ///                                     "radmc3d" is supported)
  void WriteGrid(const std::string& file_name, const std::string& format);
  ///
  /// \brief Resets the container
  ///
  void Reset();
  ///
  /// \brief Writes grid and gridded variables to a legacy VTK format
  /// @param[in] file_name                Name of the file to write the grid to
  /// @param[in] var_name                 Name(s) of scalar and/or vector
  ///                                     variable(s) to be written to the file
  ///
  void WriteVTK(const std::string& file_name,
                const std::vector<std::string>& var_name);

 private:
  // Grid the data is defined on
  T grid_;
  int output_ascii_precision_;
};
// ------------------------------------------------------------------------
//
// ------------------------------------------------------------------------
template <class T>
GriddedDataSet<T>::GriddedDataSet() {
  this->output_ascii_precision_ = 9;
}
// ------------------------------------------------------------------------
//
// ------------------------------------------------------------------------
template <class T>
GriddedDataSet<T>::GriddedDataSet(T* ptr_grid) {
  this->output_ascii_precision_ = 9;
  this->SetGrid(ptr_grid);
}
// ------------------------------------------------------------------------
//
// ------------------------------------------------------------------------
template <class T>
GriddedDataSet<T>::~GriddedDataSet() {}
// ------------------------------------------------------------------------
//
// ------------------------------------------------------------------------
template <class T>
void GriddedDataSet<T>::SetOutputPrecision(const int& precision) {
  this->output_ascii_precision_ = precision;
  this->grid_.SetOutputPrecision(precision);
}
// ------------------------------------------------------------------------
//
// ------------------------------------------------------------------------
template <class T>
int GriddedDataSet<T>::GetOutputPrecision() {
  return this->output_ascii_precision_;
}
// ------------------------------------------------------------------------
//
// ------------------------------------------------------------------------
template <class T>
void GriddedDataSet<T>::Reset() {
  this->DeleteAllPoints();
  this->grid_.ResetGrid();
}
// ------------------------------------------------------------------------
//
// ------------------------------------------------------------------------
template <class T>
void GriddedDataSet<T>::SetGrid(T* grid) {
  this->grid_ = *grid;

  std::string grid_type = grid_.GetGridType();
  int n_point = 0;
  int n_dim = 3;
  if (grid_type == "regular") {
    n_point = grid_.GetNCell();

    this->set_size(n_point, n_dim);

    int n_x = grid_.GetNCell(0);
    int n_y = grid_.GetNCell(1);
    int n_z = grid_.GetNCell(2);
    std::vector<double> x(n_x, 0.0);
    std::vector<double> y(n_y, 0.0);
    std::vector<double> z(n_z, 0.0);

    grid_.GetCellCenter(0, x.data(), n_x);
    grid_.GetCellCenter(1, y.data(), n_y);
    grid_.GetCellCenter(2, z.data(), n_z);

    std::string crd_system = grid_.GetCrdSystem();
    //
    //  Coordinate system (0-99 Cartesian, 100-199 - Spherical,
    //  >200 Cylindrical)
    //
    if (crd_system == "cartesian") {
      int ip = 0;
      for (int iz = 0; iz < n_z; ++iz) {
        for (int iy = 0; iy < n_y; ++iy) {
          for (int ix = 0; ix < n_x; ++ix) {
            ptr_points_[ip]->crd[0] = x[ix];
            ptr_points_[ip]->crd[1] = y[iy];
            ptr_points_[ip]->crd[2] = z[iz];
            ip++;
          }
        }
      }
    } else if (crd_system == "spherical") {
      std::vector<double> sin_theta(n_y, 0.0);
      std::vector<double> cos_theta(n_y, 0.0);
      std::vector<double> sin_phi(n_z, 0.0);
      std::vector<double> cos_phi(n_z, 0.0);

      for (int iy = 0; iy < n_y; ++iy) {
        sin_theta[iy] = sin(y[iy]);
        cos_theta[iy] = cos(y[iy]);
      }

      for (int iz = 0; iz < n_z; ++iz) {
        sin_phi[iz] = sin(z[iz]);
        cos_phi[iz] = cos(z[iz]);
      }

      int ip = 0;
      for (int iz = 0; iz < n_z; ++iz) {
        for (int iy = 0; iy < n_y; ++iy) {
          for (int ix = 0; ix < n_x; ++ix) {
            ptr_points_[ip]->crd[0] = x[ix] * sin_theta[iy] * cos_phi[iz];
            ptr_points_[ip]->crd[1] = x[ix] * sin_theta[iy] * sin_phi[iz];
            ptr_points_[ip]->crd[2] = x[ix] * cos_theta[iy];
            ip++;
          }
        }
      }
    } else {
      throw std::runtime_error(
          "Unknown coordinate system : " + crd_system +
          ". Only cartesian and spherical coordinate systems are implemented.");
      //      std::vector<double> sin_phi(n_y, 0.0);
      //      std::vector<double> cos_phi(n_y, 0.0);
      //
      //      for (int iy = 0; iy < n_y; ++iy) {
      //        sin_phi[iy] = sin(y[iy]);
      //        cos_phi[iy] = cos(y[iy]);
      //      }
      //
      //      int ip = 0;
      //      for (int iz = 0; iz < n_z; ++iz) {
      //        for (int iy = 0; iy < n_y; ++iy) {
      //          for (int ix = 0; ix < n_x; ++ix) {
      //            ptr_points_[ip]->crd[0] = x[ix] * cos_phi[iy];
      //            ptr_points_[ip]->crd[1] = x[ix] * sin_phi[iy];
      //            ptr_points_[ip]->crd[2] = z[iz];
      //            ip++;
      //          }
      //        }
      //      }
    }
  } else if (grid_type == "amr") {
    n_point = grid_.GetNCell();
    this->set_size(n_point, n_dim);

    int n_cell = grid_.GetNCell();
    std::vector<double> x(n_cell, 0.0);
    std::vector<double> y(n_cell, 0.0);
    std::vector<double> z(n_cell, 0.0);

    grid_.GetCellCenter(0, x.data(), n_cell);
    grid_.GetCellCenter(1, y.data(), n_cell);
    grid_.GetCellCenter(2, z.data(), n_cell);

    std::string crd_system = grid_.GetCrdSystem();
    //
    //  Coordinate system (0-99 Cartesian, 100-199 - Spherical,
    //  >200 Cylindrical)
    //
    if (crd_system == "cartesian") {
      for (int icell = 0; icell < n_cell; ++icell) {
        ptr_points_[icell]->crd[0] = x[icell];
        ptr_points_[icell]->crd[1] = y[icell];
        ptr_points_[icell]->crd[2] = z[icell];
      }
    } else if (crd_system == "spherical") {
      for (int icell = 0; icell < n_cell; ++icell) {
        double sin_theta = sin(y[icell]);
        double cos_theta = cos(y[icell]);
        double sin_phi = sin(z[icell]);
        double cos_phi = cos(z[icell]);
        ptr_points_[icell]->crd[0] = x[icell] * sin_theta * cos_phi;
        ptr_points_[icell]->crd[1] = x[icell] * sin_theta * sin_phi;
        ptr_points_[icell]->crd[2] = x[icell] * cos_theta;
      }
    } else {
      throw std::runtime_error(
          "Unknown coordinate system : " + crd_system +
          ". Only cartesian and spherical coordinate systems are implemented.");
//      for (int icell = 0; icell < n_cell; ++icell) {
//        double sin_phi = sin(y[icell]);
//        double cos_phi = cos(y[icell]);
//        ptr_points_[icell]->crd[0] = x[icell] * cos_phi;
//        ptr_points_[icell]->crd[1] = x[icell] * sin_phi;
//        ptr_points_[icell]->crd[2] = z[icell];
//      }
    }
  }
}
// ------------------------------------------------------------------------
//
// ------------------------------------------------------------------------
template <class T>
T GriddedDataSet<T>::GetGrid() {
  return this->grid_;
}
// ------------------------------------------------------------------------
//
// ------------------------------------------------------------------------
template <class T>
std::string GriddedDataSet<T>::GetCrdSystem() {
  return this->grid_.GetCrdSystem();
}

// ------------------------------------------------------------------------
//
// ------------------------------------------------------------------------
template <class T>
void GriddedDataSet<T>::WriteData(const std::string& file_name,
                                  const std::string& format,
                                  const std::vector<std::string>& var_name,
                                  const bool& is_format_binary,
                                  const double& scalar_floor) {
  std::string grid_type = grid_.GetGridType();
  int n_spec = static_cast<int>(var_name.size());
  //
  //  Check if all variables are scalars
  //
  if (var_name.size() > 1) {
    for (int ivar = 0; ivar < n_spec; ++ivar) {
      if (!is_var_scalar(var_name[ivar])) {
        throw std::runtime_error("Variable " + var_name[ivar] +
                                 " is not scalar. Note that the radmc3d "
                                 "format only allows multiple scalar "
                                 "variables (e.g. density, temperature) of "
                                 "different dust species "
                                 "to be written to a single file. It is not "
                                 "possible to write multiple vector fields "
                                 "or mixed vector/scalar fields to the same "
                                 "file");
      }
    }
  }

  std::cout << "Writing " << file_name << "...";
  int64_t n_cell = static_cast<int64_t>(this->grid_.GetNCell());
  //
  // If the output format is binary
  //
  if (is_format_binary) {
    std::fstream output_stream;
    try {
      output_stream.open(file_name, std::ios::out | std::ios::binary);
    } catch (std::exception &e) {
      std::cout << e.what() << std::endl;
    }
    //
    // Write the header part
    //
    int64_t iformat = 1;
    int64_t precision = 8;
    output_stream.write(reinterpret_cast<char*>(&iformat), sizeof(int64_t));
    output_stream.write(reinterpret_cast<char*>(&precision), sizeof(int64_t));
    output_stream.write(reinterpret_cast<char*>(&n_cell), sizeof(int64_t));
    if (n_spec > 0) {
      int64_t dummy = static_cast<int64_t>(n_spec);
      output_stream.write(reinterpret_cast<char*>(&dummy), sizeof(int64_t));
    }

    //
    // Loop over all variables
    //
    for (int ivar = 0; ivar < n_spec; ++ivar) {
      if (is_var_scalar(var_name[ivar])) {
        //
        // If more than one species is written to a file write the number of
        // species written to this file
        //
        int ind_var = GetScalarIndex(var_name[ivar]);
        //
        // When SPH data are re-gridded the grid is converted to a point data
        // set and a tree is built over this point data set. Since the point
        // sequence changes during the tree building the points in the gridded
        // data container are not ordered by coordinate anymore. Thus we need
        // to fetch the index array that restores the original point /
        // coordinate sequence.
        //
        std::vector<int> point_index(n_cell, -1);
        GetOriginalIndexOrder(point_index.data(), n_cell);

        //
        // Now loop over all points and the variable to file
        //
        double dummy = 0.0;
        for (int ip = 0; ip < n_cell; ++ip) {
          dummy = std::max(ptr_points_[point_index[ip]]->scalars[ind_var],
                           scalar_floor);
          output_stream.write(reinterpret_cast<char*>(&dummy), sizeof(double));
        }
      }
    }
  //
  // Formatted ASCII output
  //
  } else {
    std::ofstream output_stream;
    try {
      output_stream.open(file_name,
                         std::ofstream::out | std::ofstream::trunc);
    } catch (std::exception& e) {
      std::cout << e.what() << std::endl;
    }

    //
    // Write the header part
    //
    int iformat = 1;
    output_stream << iformat << "\n";
    output_stream << n_cell << "\n";
    if (n_spec > 0) {
      output_stream << n_spec << "\n";
    }
    //
    // Loop over all variables
    //
    for (int ivar = 0; ivar < n_spec; ++ivar) {
      //
      // Get the scalar index
      //
      int ind_var = GetScalarIndex(var_name[ivar]);
      //
      // When SPH data are re-gridded the grid is converted to a point data
      // set and a tree is built over this point data set. Since the point
      // sequence changes during the tree building the points in the gridded
      // data container are not ordered by coordinate anymore. Thus we need
      // to fetch the index array that restores the original point /
      // coordinate sequence.
      //
      std::vector<int> point_index(n_cell, -1);
      GetOriginalIndexOrder(point_index.data(), n_cell);

      //
      // Now loop over all points and the variable to file
      //
      double dummy = 0.0;
      for (int ip = 0; ip < n_cell; ++ip) {
        dummy = std::max(ptr_points_[point_index[ip]]->scalars[ind_var],
                         scalar_floor);

        output_stream << std::fixed << std::scientific
                      << std::setprecision(this->output_ascii_precision_)
                      << dummy << "\n";
      }
    }
  }
  std::cout << "Done" << std::endl;
}

// ------------------------------------------------------------------------
//
// ------------------------------------------------------------------------
template <class T>
void GriddedDataSet<T>::WriteData(const std::string& file_name,
                                  const std::string& format,
                                  const std::string& var_name,
                                  const bool& is_format_binary,
                                  const double& scalar_floor) {
  std::string grid_type = grid_.GetGridType();
  //
  //  Check if we need to write a scalar or a vector variable
  //
  int ind_var = -1;
  bool is_scalar = is_var_scalar(var_name);
  if (is_scalar) {
    ind_var = GetScalarIndex(var_name);
  } else {
    ind_var = GetVectorIndex(var_name);
  }

  std::cout << "Writing " << file_name << "...";
  int64_t n_cell = static_cast<int64_t>(this->grid_.GetNCell());

  //
  // If the output format is binary
  //
  if (is_format_binary) {
    std::fstream output_stream;
    try {
      output_stream.open(file_name, std::ios::out | std::ios::binary);
    } catch (std::exception &e) {
      std::cout << e.what() << std::endl;
    }
    //
    // Write the header part
    //
    int64_t iformat = 1;
    int64_t precision = 8;
    output_stream.write(reinterpret_cast<char*>(&iformat), sizeof(int64_t));
    output_stream.write(reinterpret_cast<char*>(&precision), sizeof(int64_t));
    output_stream.write(reinterpret_cast<char*>(&n_cell), sizeof(int64_t));

    if (grid_type == "amr") {
      int64_t n_spec = 1;
      output_stream.write(reinterpret_cast<char*>(&n_spec), sizeof(int64_t));
    }

    //
    // When SPH data are re-gridded the grid is converted to a point data
    // set and a tree is built over this point data set. Since the point
    // sequence changes during the tree building the points in the gridded
    // data container are not ordered by coordinate anymore. Thus we need
    // to fetch the index array that restores the original point /
    // coordinate sequence.
    //
    std::vector<int> point_index(n_cell, -1);
    GetOriginalIndexOrder(point_index.data(), n_cell);

    //
    // Now loop over all points and the variable to file
    //

    if (is_scalar) {
      double dummy = 0.0;
      for (int ip = 0; ip < n_cell; ++ip) {
        dummy =
            std::max(ptr_points_[point_index[ip]]->scalars[ind_var],
                scalar_floor);
        output_stream.write(reinterpret_cast<char*>(&dummy),
                            sizeof(double));
      }
    } else {
      std::string crd_system = grid_.GetCrdSystem();
      if (crd_system == "cartesian") {
        for (int icell = 0; icell < n_cell; ++icell) {
          for (int idim = 0; idim < n_dim_; ++idim) {
            output_stream.write(reinterpret_cast<char*>(
                                    &ptr_points_[point_index[icell]]
                                         ->vectors[idim + n_dim_ * ind_var]),
                                sizeof(double));
          }
        }
      } else if (crd_system == "spherical") {
        int nx = this->grid_.GetNCell(0);
        int ny = this->grid_.GetNCell(1);
        int nz = this->grid_.GetNCell(2);
        //
        // Get the cell center coordinates
        //
        std::vector<double> x(nx, 0.0);
        std::vector<double> y(ny, 0.0);
        std::vector<double> z(nz, 0.0);
        this->grid_.GetCellCenter(0, x.data(), nx);
        this->grid_.GetCellCenter(1, y.data(), ny);
        this->grid_.GetCellCenter(2, z.data(), nz);

        //
        // Pre-compute some trigonometric functions
        //
        std::vector<double> cos_theta(ny, 0.0);
        std::vector<double> sin_theta(ny, 0.0);

        for (int iy = 0; iy < ny; ++iy) {
          cos_theta[iy] = cos(y[iy]);
          sin_theta[iy] = sin(y[iy]);

          if (std::abs(y[iy]) < 1e-15) {
            y[iy] = 0.0;
            sin_theta[iy] = 0.0;
            cos_theta[iy] = 1.0;
          }
          if (std::abs(y[iy] - (M_PI * 0.5)) < 1e-15) {
            y[iy] = 0.5 * M_PI;
            sin_theta[iy] = 1.0;
            cos_theta[iy] = 0.0;
          }

          if (std::abs(y[iy] - M_PI) < 1e-15) {
            y[iy] = M_PI;
            sin_theta[iy] = 0.0;
            cos_theta[iy] = -1.0;
          }
        }

        std::vector<double> cos_phi(nz, 0.0);
        std::vector<double> sin_phi(nz, 0.0);

        for (int iz = 0; iz < nz; ++iz) {
          cos_phi[iz] = cos(z[iz]);
          sin_phi[iz] = sin(z[iz]);

          if (std::abs(z[iz]) < 1e-15) {
            z[iz] = 0.0;
            sin_phi[iz] = 0.0;
            cos_phi[iz] = 1.0;
          }
          if (std::abs(z[iz] - (M_PI * 0.5)) < 1e-15) {
            z[iz] = 0.5 * M_PI;
            sin_phi[iz] = 1.0;
            cos_phi[iz] = 0.0;
          }
          if (std::abs(z[iz] - M_PI) < 1e-15) {
            z[iz] = M_PI;
            sin_phi[iz] = 0.0;
            cos_phi[iz] = -1.0;
          }
          if (std::abs(z[iz] - (M_PI * 1.5)) < 1e-15) {
            z[iz] = 1.5 * M_PI;
            sin_phi[iz] = -1.0;
            cos_phi[iz] = 0.0;
          }
          if (std::abs(z[iz] - (M_PI * 2.0)) < 1e-15) {
            z[iz] = 2.0 * M_PI;
            sin_phi[iz] = 0.0;
            cos_phi[iz] = 1.0;
          }
        }

        //
        // Loop through all cells
        //
        int icell = 0;
        std::vector<double> vec(3, 0.0);
        std::vector<double> crd_cart(3, 0.0);
        for (int iz = 0; iz < nz; ++iz) {
          for (int iy = 0; iy < ny; ++iy) {
            for (int ix = 0; ix < nx; ++ix) {
              crd_cart[0] = x[ix] * sin_theta[iy] * cos_phi[iz];
              crd_cart[1] = x[ix] * sin_theta[iy] * sin_phi[iz];
              crd_cart[2] = x[ix] * cos_theta[iy];
              double r_cyl = x[ix] * sin_theta[iy];
              for (int idim = 0; idim < 3; ++idim) {
                vec[0] += ptr_points_[point_index[icell]]
                              ->vectors[idim + n_dim_ * ind_var] *
                          crd_cart[0];
              }
              vec[0] /= x[ix];
              vec[1] =
                  (crd_cart[2] *
                       (crd_cart[0] * ptr_points_[point_index[icell]]
                                          ->vectors[0 + n_dim_ * ind_var] +
                        crd_cart[1] * ptr_points_[point_index[icell]]
                                          ->vectors[1 + n_dim_ * ind_var]) -
                   ptr_points_[point_index[icell]]
                           ->vectors[2 + n_dim_ * ind_var] *
                       x[ix] * x[ix]) /
                  (x[ix] * r_cyl);
              vec[2] = (crd_cart[0] * ptr_points_[point_index[icell]]
                                          ->vectors[1 + n_dim_ * ind_var] -
                        crd_cart[1] * ptr_points_[point_index[icell]]
                                          ->vectors[0 + n_dim_ * ind_var]) /
                       r_cyl;



              for (int idim = 0; idim < n_dim_; ++idim) {
                output_stream.write(reinterpret_cast<char*>(&vec[idim]),
                                    sizeof(double));
              }
            }
          }
        }
      }
    }

  } else {
    std::ofstream output_stream;
    try {
      output_stream.open(file_name,
                         std::ofstream::out | std::ofstream::trunc);
    } catch (std::exception& e) {
      std::cout << e.what() << std::endl;
    }

    //
    // Write the header part
    //
    int iformat = 1;
    output_stream << iformat << "\n";
    output_stream << n_cell << "\n";

    //
    // When SPH data are re-gridded the grid is converted to a point data
    // set and a tree is built over this point data set. Since the point
    // sequence changes during the tree building the points in the gridded
    // data container are not ordered by coordinate anymore. Thus we need
    // to fetch the index array that restores the original point /
    // coordinate sequence.
    //
    std::vector<int> point_index(n_cell, -1);
    GetOriginalIndexOrder(point_index.data(), n_cell);
    //
    // Now loop over all points and the variable to file
    //
    if (is_scalar) {
      double dummy = 0.0;
      for (int ip = 0; ip < n_cell; ++ip) {
        dummy =
            std::max(ptr_points_[point_index[ip]]->scalars[ind_var],
                scalar_floor);
        output_stream << std::fixed << std::scientific
                      << std::setprecision(this->output_ascii_precision_)
                      << dummy << "\n";
      }
      output_stream << "\n";
    } else {
      std::string crd_system = grid_.GetCrdSystem();
      if (crd_system == "cartesian") {
        for (int icell = 0; icell < n_cell; ++icell) {
          for (int idim = 0; idim < n_dim_; ++idim) {
            output_stream << std::fixed << std::scientific
                          << std::setprecision(this->output_ascii_precision_)
                          << ptr_points_[point_index[icell]]
                                 ->vectors[idim + n_dim_ * ind_var] << " ";
          }
          output_stream << "\n";
        }
      } else if (crd_system == "spherical") {
        int n_cell = this->grid_.GetNCell();

        //
        // Get the cell center coordinates
        //
        std::vector<double> r(n_cell, 0.0);
        std::vector<double> theta(n_cell, 0.0);
        std::vector<double> phi(n_cell, 0.0);
        this->grid_.GetCellCenter(0, r.data(), n_cell);
        this->grid_.GetCellCenter(1, theta.data(), n_cell);
        this->grid_.GetCellCenter(2, phi.data(), n_cell);

        //
        // Precompute trigonometric functions
        //
        for (int icell = 0; icell < n_cell; ++icell) {
          double cos_theta = cos(theta[icell]);
          double sin_theta = sin(theta[icell]);
          double cos_phi = cos(phi[icell]);
          double sin_phi = sin(phi[icell]);

          if (std::abs(theta[icell]) < 1e-15) {
            theta[icell] = 0.0;
            sin_theta = 0.0;
            cos_theta = 1.0;
          }
          if (std::abs(theta[icell] - (M_PI * 0.5)) < 1e-15) {
            theta[icell] = 0.5 * M_PI;
            sin_theta = 1.0;
            cos_theta = 0.0;
          }

          if (std::abs(theta[icell] - M_PI) < 1e-15) {
            theta[icell] = M_PI;
            sin_theta = 0.0;
            cos_theta = -1.0;
          }

          if (std::abs(phi[icell]) < 1e-15) {
            phi[icell] = 0.0;
            sin_phi = 0.0;
            cos_phi = 1.0;
          }
          if (std::abs(phi[icell] - (M_PI * 0.5)) < 1e-15) {
            phi[icell] = 0.5 * M_PI;
            sin_phi = 1.0;
            cos_phi = 0.0;
          }
          if (std::abs(phi[icell] - M_PI) < 1e-15) {
            phi[icell] = M_PI;
            sin_phi = 0.0;
            cos_phi = -1.0;
          }
          if (std::abs(phi[icell] - (M_PI * 1.5)) < 1e-15) {
            phi[icell] = 1.5 * M_PI;
            sin_phi = -1.0;
            cos_phi = 0.0;
          }
          if (std::abs(phi[icell] - (M_PI * 2.0)) < 1e-15) {
            phi[icell] = 2.0 * M_PI;
            sin_phi = 0.0;
            cos_phi = 1.0;
          }

          std::vector<double> vec(3, 0.0);
          std::vector<double> crd_cart(3, 0.0);
          crd_cart[0] = r[icell] * sin_theta * cos_phi;
          crd_cart[1] = r[icell] * sin_theta * sin_phi;
          crd_cart[2] = r[icell] * cos_theta;
          double r_cyl = r[icell] * sin_theta;

          for (int idim = 0; idim < 3; ++idim) {
            vec[0] += ptr_points_[point_index[icell]]
                          ->vectors[idim + n_dim_ * ind_var] *
                      crd_cart[0];
          }
          vec[0] /= r[icell];

          vec[1] =
              (crd_cart[2] *
                   (crd_cart[0] * ptr_points_[point_index[icell]]
                                      ->vectors[0 + n_dim_ * ind_var] +
                    crd_cart[1] * ptr_points_[point_index[icell]]
                                      ->vectors[1 + n_dim_ * ind_var]) -
               ptr_points_[point_index[icell]]->vectors[2 + n_dim_ * ind_var] *
                   r[icell] * r[icell]) / (r[icell] * r_cyl);

          vec[2] = (crd_cart[0] * ptr_points_[point_index[icell]]
                                      ->vectors[1 + n_dim_ * ind_var] -
                    crd_cart[1] * ptr_points_[point_index[icell]]
                                      ->vectors[0 + n_dim_ * ind_var]) /
                   r_cyl;

          for (int idim = 0; idim < n_dim_; ++idim) {
            output_stream << std::fixed << std::scientific
                          << std::setprecision(this->output_ascii_precision_)
                          << vec[idim] << " ";
          }
          output_stream << "\n";
        }
      }
    }
  }
  std::cout << "Done" << std::endl;
}
// ------------------------------------------------------------------------
//
// ------------------------------------------------------------------------
template <class T>
void GriddedDataSet<T>::WriteGrid(const std::string& file_name,
                                        const std::string& format) {
  this->grid_.WriteToFile(file_name, format);
}

// ------------------------------------------------------------------------
//
// ------------------------------------------------------------------------
template <class T>
int GriddedDataSet<T>::GetNCell(int iaxis) {
  return this->grid_.GetNCell(iaxis);
}
// ------------------------------------------------------------------------
//
// ------------------------------------------------------------------------
template <class T>
void GriddedDataSet<T>::GetCellCenter(const int& iaxis,
                                      double* vector_double_out,
                                      const int& dim_out) {
  int n_cell = this->grid_.GetNCell(iaxis);
  if (dim_out != n_cell) {
    throw std::runtime_error("Grid axis size error. Axis " +
                             std::to_string(iaxis) + " has " +
                             std::to_string(n_cell) +
                             " cells but GetGridAxis received a length for " +
                             std::to_string(dim_out));
  }

  this->grid_.GetCellCenter(iaxis, vector_double_out, dim_out);
}
// ------------------------------------------------------------------------
//
// ------------------------------------------------------------------------
template <class T>
void GriddedDataSet<T>::WriteVTK(const std::string& file_name,
                                 const std::vector<std::string>& var_name) {
  std::ofstream out_stream;
  // Open the stream
  try {
    out_stream.open(file_name, std::ofstream::out | std::ofstream::trunc);
  } catch (std::exception &e) {
    std::cout << e.what() << std::endl;
  }

  std::cout << "Writing " << file_name << "...";
  out_stream << "# vtk DataFile Version 3.0\n";
  out_stream << "SphTool Data\n";
  out_stream << "ASCII\n";
  out_stream << "\n";
  out_stream << "DATASET UNSTRUCTURED_GRID\n";

  if (this->grid_.GetGridType() == "regular") {
    int n_x = this->grid_.GetNCell(0);
    int n_y = this->grid_.GetNCell(1);
    int n_z = this->grid_.GetNCell(2);
    int n_xi = n_x + 1;
    int n_yi = n_y + 1;
    int n_zi = n_z + 1;
    std::string crd_system = this->grid_.GetCrdSystem();
    int n_dim = 3;
    int n_corner = 8;
    int n_points = 0;
    int n_cell   = 0;
    //
    // Get the cell interfaces
    //
    std::vector<double> xi(n_x + 1, 0.0);
    std::vector<double> yi(n_y + 1, 0.0);
    std::vector<double> zi(n_z + 1, 0.0);

    this->grid_.GetCellInterface(0, xi.data(), n_x + 1);
    this->grid_.GetCellInterface(1, yi.data(), n_y + 1);
    this->grid_.GetCellInterface(2, zi.data(), n_z + 1);
    //
    // Generate the coordinate index offset
    //
    std::vector<int> corner_ind_x(n_corner, 0);
    std::vector<int> corner_ind_y(n_corner, 0);
    std::vector<int> corner_ind_z(n_corner, 0);

    int icorner = 0;
    for (int ix = 0; ix < 2; ++ix) {
      for (int iy = 0; iy < 2; ++iy) {
        for (int iz = 0; iz < 2; ++iz) {
          corner_ind_x[icorner] = ix;
          corner_ind_y[icorner] = iy;
          corner_ind_z[icorner] = iz;
          icorner += 1;
        }
      }
    }

    if (crd_system == "cartesian") {
      //
      // Calculate the number of points (cell corners)
      //
      n_points = (n_x + 1) * (n_y + 1) * (n_z + 1);
      n_cell   = n_x * n_y * n_z;
      out_stream << "POINTS " << n_points << " double\n";

      //
      // Write the cell corners
      //
      for (int iz = 0; iz < n_z + 1; ++iz) {
        for (int iy = 0; iy < n_y + 1; ++iy) {
          for (int ix = 0; ix < n_x + 1; ++ix) {
            out_stream << std::fixed << std::scientific
                       << std::setprecision(this->output_ascii_precision_)
                       << xi[ix] << " " << yi[iy] << " " << zi[iz] << "\n";
          }
        }
      }
      //
      // Write the number of cell data point (number of cells * 9)
      // 9 is coming from having 8 corners + 1 number, which is the number
      // of corners
      //
      out_stream << "CELLS " << n_x * n_y * n_z << " " << n_x * n_y * n_z * 9
                 << "\n";
      //
      // Write out the indices of the cell interface mesh that define a
      // hexahedron (VTK cell type #12)
      //
      // The indexing of a hexahedron is as follows
      //
      //                  7________6
      //                 /|      / |
      //                / |     /  |
      //               4_------5   |             z ^   ^ y
      //               |  3____|___2               |  /
      //               | /     |  /                | /
      //               |/      | /                 |/
      //               0-------1                   0-----> x
      //
      for (int iz = 0; iz < n_z; ++iz) {
        for (int iy = 0; iy < n_y; ++iy) {
          for (int ix = 0; ix < n_x; ++ix) {
            int id1 = ix + iy * n_xi + iz * n_xi * n_yi;
            int id2 = (ix + 1) + iy * n_xi + iz * n_xi * n_yi;
            int id3 = (ix + 1) + (iy + 1) * n_xi + iz * n_xi * n_yi;
            int id4 = ix + (iy + 1) * n_xi + iz * n_xi * n_yi;
            int id5 = ix + iy * n_xi + (iz + 1) * n_xi * n_yi;
            int id6 = (ix + 1) + iy * n_xi + (iz + 1) * n_xi * n_yi;
            int id7 = (ix + 1) + (iy + 1) * n_xi + (iz + 1) * n_xi * n_yi;
            int id8 = ix + (iy + 1) * n_xi + (iz + 1) * n_xi * n_yi;
            out_stream << "8 " << id1 << " " << id2 << " " << id3 << " " << id4
                       << " " << id5 << " " << id6 << " " << id7 << " " << id8
                       << "\n";
          }
        }
      }
      out_stream << "CELL_TYPES " << n_x * n_y * n_z << "\n";
      for (int i = 0; i < n_x * n_y * n_z; ++i) {
        out_stream << "12\n";
      }
      //
      // Spherical coordinate system
      //
    } else if (crd_system == "spherical") {
      //
      // Indicator for periodic boundary in azimuth
      //
      bool is_boundary_periodic = false;
      //
      // Pre-compute some trigonometric functions
      //
      std::vector<double> cos_thetai(n_yi, 0.0);
      std::vector<double> sin_thetai(n_yi, 0.0);
      for (int i = 0; i < n_yi; ++i) {
        cos_thetai[i] = cos(yi[i]);
        sin_thetai[i] = sin(yi[i]);
        if (std::abs(yi[i]) < 1e-15) {
          yi[i] = 0.0;
          sin_thetai[i] = 0.0;
          cos_thetai[i] = 1.0;
        }
        if (std::abs(yi[i] - (M_PI*0.5)) < 1e-15) {
          yi[i] = 0.5 * M_PI;
          sin_thetai[i] = 1.0;
          cos_thetai[i] = 0.0;
        }

        if (std::abs(yi[i] - (M_PI)) < 1e-15) {
          yi[i] = M_PI;
          sin_thetai[i] = 0.0;
          cos_thetai[i] = -1.0;
        }
      }
      std::vector<double> cos_phii(n_zi, 0.0);
      std::vector<double> sin_phii(n_zi, 0.0);
      for (int i = 0; i < n_zi; ++i) {
        cos_phii[i] = cos(zi[i]);
        sin_phii[i] = sin(zi[i]);
        if (std::abs(zi[i]) < 1e-15) {
          zi[i] = 0.0;
          sin_phii[i] = 0.0;
          cos_phii[i] = 1.0;
        }
        if (std::abs(zi[i] - (M_PI*0.5)) < 1e-15) {
          zi[i] = 0.5 * M_PI;
          sin_phii[i] = 1.0;
          cos_phii[i] = 0.0;
        }
        if (std::abs(zi[i] - M_PI) < 1e-15) {
          zi[i] = M_PI;
          sin_phii[i] = 0.0;
          cos_phii[i] = -1.0;
        }
        if (std::abs(zi[i] - (M_PI*1.5)) < 1e-15) {
          zi[i] = 1.5 * M_PI;
          sin_phii[i] = -1.0;
          cos_phii[i] = 0.0;
        }
        if (std::abs(zi[i] - (M_PI*2.0)) < 1e-15) {
          zi[i] = 2.0 * M_PI;
          sin_phii[i] = 0.0;
          cos_phii[i] = 1.0;
        }
      }

      if ((zi[0] == 0.0) & (zi[n_zi - 1] == 2.0 * M_PI)) {
        is_boundary_periodic = true;
      }
      //
      // Calculate the number of points (cell corners)
      //
      n_points = (n_x + 1) * (n_y + 1) * (n_z + 1);
      if (is_boundary_periodic) {
        n_points = (n_x + 1) * (n_y + 1) * n_z;
      }
      n_cell   = n_x * n_y * n_z;
      out_stream << "POINTS " << n_points << " double\n";

      //
      // Write the cell corners
      //
      int iz_max = n_z + 1;
      if (is_boundary_periodic) {
        iz_max = n_z;
      }
      for (int iz = 0; iz < iz_max; ++iz) {
        for (int iy = 0; iy < n_y + 1; ++iy) {
          for (int ix = 0; ix < n_x + 1; ++ix) {
            out_stream << std::fixed << std::scientific
                       << std::setprecision(this->output_ascii_precision_)
                       << xi[ix] * sin_thetai[iy] * cos_phii[iz] << " "
                       << xi[ix] * sin_thetai[iy] * sin_phii[iz] << " "
                       << xi[ix] * cos_thetai[iy] << "\n";
          }
        }
      }
      //
      // Write the number of cell data point (number of cells * 9)
      // 9 is coming from having 8 corners + 1 number, which is the number
      // of corners
      //
      out_stream << "CELLS " << n_x * n_y * n_z << " "
                 << (n_x * (n_y - 2) * n_z * 9) + (n_x * n_z * 2) * 7 << "\n";
      //
      // Write out the indices of the cell interface mesh that define a
      // hexahedron (VTK cell type #12)
      //
      // The indexing of a hexahedron is as follows
      //
      //  Hexahedron:
      //
      //                  7________6
      //                 /|      / |
      //                / |     /  |
      //               4_------5   |             z ^   ^ y
      //               |  3____|___2               |  /
      //               | /     |  /                | /
      //               |/      | /                 |/
      //               0-------1                   0-----> x
      //
      //
      //  Wedge (Northern hemisphere):
      //
      //              Pole                       Pole
      //                3 ----- 5                 5 ----- 4
      //                |\     /|                 |\     /|
      //                | \   / |                 | \   / |
      //                |  \ /  |                 |  \ /  |
      //                |   4   |                 |   3   |
      //                0 --|-- 2                 2 --|-- 1
      //                 \  |  /                   \  |  /
      //                  \ | /                     \ | /
      //                   \|/                       \|/
      //                    1                         0
      //
      int n_xi = n_x + 1;
      int n_yi = n_y + 1;
      int n_zi = n_z + 1;

      int z_mod = n_zi;
      if (is_boundary_periodic) {
        z_mod = n_z;
      }

      for (int iz = 0; iz < n_z; ++iz) {
        //
        // Wedge cells (North pole)
        //
        for (int iy = 0; iy < 1; ++iy) {
          for (int ix = 0; ix < n_x; ++ix) {
            int id1 = ix + iy * n_xi + iz * n_xi * n_yi;
            int id2 = ix + (iy + 1) * n_xi + iz * n_xi * n_yi;
            int id3 = ix + (iy + 1) * n_xi + ((iz + 1) % z_mod) * n_xi * n_yi;
            int id4 = (ix + 1) + iy * n_xi + iz * n_xi * n_yi;
            int id5 = (ix + 1) + (iy + 1) * n_xi + iz * n_xi * n_yi;
            int id6 =
                (ix + 1) + (iy + 1) * n_xi + ((iz + 1) % z_mod) * n_xi * n_yi;
            out_stream << "6 " << id1 << " " << id2 << " " << id3 << " " << id4
                       << " " << id5 << " " << id6 << "\n";
          }
        }

        for (int iy = 1; iy < n_y-1; ++iy) {
          for (int ix = 0; ix < n_x; ++ix) {
            int id1 = ix + iy * n_xi + iz * n_xi * n_yi;
            int id2 = (ix + 1) + iy * n_xi + iz * n_xi * n_yi;
            int id3 = (ix + 1) + (iy + 1) * n_xi + iz * n_xi * n_yi;
            int id4 = ix + (iy + 1) * n_xi + iz * n_xi * n_yi;
            int id5 = ix + iy * n_xi + ((iz + 1)%z_mod) * n_xi * n_yi;
            int id6 = (ix + 1) + iy * n_xi + ((iz + 1)%z_mod) * n_xi * n_yi;
            int id7 =
                (ix + 1) + (iy + 1) * n_xi + ((iz + 1) % z_mod) * n_xi * n_yi;
            int id8 = ix + (iy + 1) * n_xi + ((iz + 1) % z_mod) * n_xi * n_yi;
            out_stream << "8 " << id1 << " " << id2 << " " << id3 << " " << id4
                       << " " << id5 << " " << id6 << " " << id7 << " " << id8
                       << "\n";
          }
        }
        //
        // Wedge cells (South pole)
        //
        for (int iy = n_y-1; iy < n_y; ++iy) {
          for (int ix = 0; ix < n_x; ++ix) {
            int id1 = ix + iy * n_xi + iz * n_xi * n_yi;
            int id2 = ix + iy * n_xi + ((iz + 1)%z_mod) * n_xi * n_yi;
            int id3 = ix + (iy + 1) * n_xi + iz * n_xi * n_yi;
            int id4 = (ix + 1) + iy * n_xi + iz * n_xi * n_yi;
            int id5 = (ix + 1) + iy * n_xi + ((iz + 1) % z_mod) * n_xi * n_yi;
            int id6 = (ix + 1) + (iy + 1) * n_xi + iz * n_xi * n_yi;
            out_stream << "6 " << id1 << " " << id2 << " " << id3 << " " << id4
                       << " " << id5 << " " << id6 << "\n";
          }
        }
      }
      out_stream << "CELL_TYPES " << n_x * n_y * n_z << "\n";
      for (int iz = 0; iz < n_z; ++iz) {
        //
        // Wedge cells (North pole)
        //
        for (int iy = 0; iy < 1; ++iy) {
          for (int ix = 0; ix < n_x; ++ix) {
            out_stream << "13\n";
          }
        }
        for (int iy = 1; iy < n_y-1; ++iy) {
          for (int ix = 0; ix < n_x; ++ix) {
            out_stream << "12\n";
          }
        }
        for (int iy = n_y-1; iy < n_y; ++iy) {
          for (int ix = 0; ix < n_x; ++ix) {
            out_stream << "13\n";
          }
        }
      }
    } else  {
      throw std::runtime_error(
          "VTK output is not yet implemented for cylindrical grid");
    }

    //
    // Write the scalar variables
    //
    out_stream << "CELL_DATA " << n_cell << "\n";
    for (size_t ivar = 0; ivar < var_name.size(); ++ivar) {
      if (this->is_var_scalar(var_name[ivar])) {
        out_stream << "SCALARS " << var_name[ivar] << " double \n";
        out_stream << "LOOKUP_TABLE default\n";

        std::vector<double> scalar(n_cell);
        this->_get_scalar(var_name[ivar], scalar.data(), n_cell);
        for (int i = 0; i < n_cell; ++i) {
          out_stream << std::fixed << std::scientific
                     << std::setprecision(this->output_ascii_precision_)
                     << scalar[i] << "\n";
        }
      }
    }
    //
    // Write the vector variables
    //
    for (size_t ivar = 0; ivar < var_name.size(); ++ivar) {
      if (!this->is_var_scalar(var_name[ivar])) {
        out_stream << "VECTORS " << var_name[ivar] << " double\n";

        std::vector<double> vector(n_cell * n_dim);
        this->_get_vector(var_name[ivar], vector.data(), n_cell * n_dim, n_dim);
        for (int i = 0; i < n_cell; ++i) {
          for (int idim = 0; idim < n_dim; ++idim) {
            out_stream << std::fixed << std::scientific
                       << std::setprecision(this->output_ascii_precision_)
                       << vector[idim + n_dim * i] << " ";
          }
          out_stream << "\n";
        }
      }
    }

  } else {
    int n_cell = this->grid_.GetNCell();
    std::string crd_system = this->grid_.GetCrdSystem();
    int n_dim = 3;
    int n_corner = 8;
    //
    // Get the cell interfaces
    //
    std::vector<double> xi(n_cell*2, 0.0);
    std::vector<double> yi(n_cell*2, 0.0);
    std::vector<double> zi(n_cell*2, 0.0);

    this->grid_.GetCellInterface(0, xi.data(), n_cell*2);
    this->grid_.GetCellInterface(1, yi.data(), n_cell*2);
    this->grid_.GetCellInterface(2, zi.data(), n_cell*2);

    if (crd_system == "cartesian") {
      //
      // Calculate the number of points (cell corners)
      //
      int n_points = n_corner * n_cell;
      out_stream << "POINTS " << n_points << " double\n";

      for (int icell = 0; icell < n_cell; ++icell) {
        for (int iz = 0; iz < 2; ++iz) {
          for (int iy = 0; iy < 2; ++iy) {
            for (int ix = 0; ix < 2; ++ix) {
              out_stream << std::fixed << std::scientific
                         << std::setprecision(this->output_ascii_precision_)
                         << xi[ix + 2 * icell] << " " << yi[iy + 2 * icell]
                         << " " << zi[iz + 2 * icell] << "\n";
            }
          }
        }
      }
      //
      // Write the number of cell data point (number of cells * 9)
      // 9 is coming from having 8 corners + 1 number, which is the number
      // of corners
      //
      out_stream << "CELLS " << n_cell << " " << n_cell * 9 << "\n";

      //
      // Write out the indices of the cell interface mesh that define a
      // hexahedron (VTK cell type #12)
      //
      // The indexing of a hexahedron is as follows
      //
      //                  7________6
      //                 /|      / |
      //                / |     /  |
      //               4_------5   |             z ^   ^ y
      //               |  3____|___2               |  /
      //               | /     |  /                | /
      //               |/      | /                 |/
      //               0-------1                   0-----> x
      //
      for (int icell = 0; icell < n_cell; ++icell) {
        int id1 = icell * n_corner + 0;
        int id2 = icell * n_corner + 1;
        int id3 = icell * n_corner + 3;
        int id4 = icell * n_corner + 2;
        int id5 = icell * n_corner + 4;
        int id6 = icell * n_corner + 5;
        int id7 = icell * n_corner + 7;
        int id8 = icell * n_corner + 6;
        out_stream << "8 " << id1 << " " << id2 << " " << id3 << " " << id4
                   << " " << id5 << " " << id6 << " " << id7 << " " << id8
                   << "\n";
      }
      out_stream << "CELL_TYPES " << n_cell << "\n";
      for (int i = 0; i < n_cell; ++i) {
        out_stream << "12\n";
      }

      //
      // Write the scalar variables
      //
      out_stream << "CELL_DATA " << n_cell << "\n";
      for (size_t ivar = 0; ivar < var_name.size(); ++ivar) {
        if (this->is_var_scalar(var_name[ivar])) {
          out_stream << "SCALARS " << var_name[ivar] << " double \n";
          out_stream << "LOOKUP_TABLE default\n";

          std::vector<double> scalar(n_cell);
          this->_get_scalar(var_name[ivar], scalar.data(), n_cell);
          for (int i = 0; i < n_cell; ++i) {
            out_stream << std::fixed << std::scientific
                       << std::setprecision(this->output_ascii_precision_)
                       << scalar[i] << "\n";
          }
        }
      }
      //
      // Write the vector variables
      //
      for (size_t ivar = 0; ivar < var_name.size(); ++ivar) {
        if (!this->is_var_scalar(var_name[ivar])) {
          out_stream << "VECTORS " << var_name[ivar] << " double\n";

          std::vector<double> vector(n_cell * n_dim);
          this->_get_vector(var_name[ivar], vector.data(), n_cell*n_dim, n_dim);
          for (int i = 0; i < n_cell; ++i) {
            for (int idim = 0; idim < n_dim; ++idim) {
              out_stream << std::fixed << std::scientific
                         << std::setprecision(this->output_ascii_precision_)
                         << vector[idim + n_dim * i] << " ";
            }
            out_stream << "\n";
          }
        }
      }


    } else if (crd_system == "spherical") {
      //
      // Pre-calculate some trigonometric functions and check cell types
      //
      std::vector<double> cos_thetai(n_cell * 2, 0.0);
      std::vector<double> sin_thetai(n_cell * 2, 0.0);
      std::vector<double> cos_phii(n_cell * 2, 0.0);
      std::vector<double> sin_phii(n_cell * 2, 0.0);
      std::vector<int> cell_type(n_cell, 12);

      for (int icell = 0; icell < n_cell; ++icell) {
        for (int i = 0; i < 2; ++i) {
          int curr_index = icell * 2 + i;
          cos_thetai[curr_index] = cos(yi[curr_index]);
          sin_thetai[curr_index] = sin(yi[curr_index]);
          cos_phii[curr_index] = cos(zi[curr_index]);
          sin_phii[curr_index] = sin(zi[curr_index]);

          if (std::abs(yi[curr_index]) < 1e-15) {
            yi[curr_index] = 0.0;
            sin_thetai[curr_index] = 0.0;
            cos_thetai[curr_index] = 1.0;
          }
          if (std::abs(yi[curr_index] - (M_PI * 0.5)) < 1e-15) {
            yi[curr_index] = 0.5 * M_PI;
            sin_thetai[curr_index] = 1.0;
            cos_thetai[curr_index] = 0.0;
          }

          if (std::abs(yi[curr_index] - M_PI) < 1e-15) {
            yi[curr_index] = M_PI;
            sin_thetai[curr_index] = 0.0;
            cos_thetai[curr_index] = -1.0;
          }

          if (std::abs(zi[curr_index]) < 1e-15) {
            zi[curr_index] = 0.0;
            sin_phii[curr_index] = 0.0;
            cos_phii[curr_index] = 1.0;
          }
          if (std::abs(zi[curr_index] - (M_PI * 0.5)) < 1e-15) {
            zi[curr_index] = 0.5 * M_PI;
            sin_phii[curr_index] = 1.0;
            cos_phii[curr_index] = 0.0;
          }
          if (std::abs(zi[curr_index] - M_PI) < 1e-15) {
            zi[curr_index] = M_PI;
            sin_phii[curr_index] = 0.0;
            cos_phii[curr_index] = -1.0;
          }
          if (std::abs(zi[curr_index] - (M_PI * 1.5)) < 1e-15) {
            zi[curr_index] = 1.5 * M_PI;
            sin_phii[curr_index] = -1.0;
            cos_phii[curr_index] = 0.0;
          }
          if (std::abs(zi[curr_index] - (M_PI * 2.0)) < 1e-15) {
            zi[curr_index] = 2.0 * M_PI;
            sin_phii[curr_index] = 0.0;
            cos_phii[curr_index] = 1.0;
          }
        }
        if ((yi[icell] == 0.0) | (yi[icell + 1] == M_PI)) {
          cell_type[icell] = 13;
        }
      }

      int n_wedge = 0;
      for (int icell = 0; icell < n_cell; ++icell) {
        if (cell_type[icell] == 13) n_wedge += 1;
      }
      int n_hexahedron = n_cell - n_wedge;
      //
      // Write the cell corners
      //
      out_stream << "POINTS " << n_cell * 8 << " double\n";
      for (int icell = 0; icell < n_cell; ++icell) {
        if (cell_type[icell] == 12) {
          for (int iz = 0; iz < 2; ++iz) {
            for (int iy = 0; iy < 2; ++iy) {
              for (int ix = 0; ix < 2; ++ix) {
                out_stream << std::fixed << std::scientific
                           << std::setprecision(this->output_ascii_precision_)
                           << xi[ix + 2 * icell] * sin_thetai[iy + 2 * icell] *
                                  cos_phii[iz + 2 * icell]
                           << " "
                           << xi[ix + 2 * icell] * sin_thetai[iy + 2 * icell] *
                                  sin_phii[iz + 2 * icell]
                           << " "
                           << xi[ix + 2 * icell] * cos_thetai[iy + 2 * icell]
                           << "\n";
              }
            }
          }
        } else {
          //
          // Wedge cells around the north pole
          //
          if (yi[2*icell] < M_PI*0.5) {
            for (int iz = 0; iz < 2; ++iz) {
              for (int iy = 0; iy < 2; ++iy) {
                for (int ix = 0; ix < 2; ++ix) {
                  out_stream
                      << std::fixed << std::scientific
                      << std::setprecision(this->output_ascii_precision_)
                      << xi[ix + 2 * icell] * sin_thetai[iy + 2 * icell] *
                             cos_phii[iz + 2 * icell]
                      << " "
                      << xi[ix + 2 * icell] * sin_thetai[iy + 2 * icell] *
                             sin_phii[iz + 2 * icell]
                      << " " << xi[ix + 2 * icell] * cos_thetai[iy + 2 * icell]
                      << "\n";
                }
              }
            }
          //
          // Wedge cells around the south pole
          //
          } else {
            for (int iz = 0; iz < 2; ++iz) {
              for (int iy = 0; iy < 2; ++iy) {
                for (int ix = 0; ix < 2; ++ix) {
                  out_stream
                      << std::fixed << std::scientific
                      << std::setprecision(this->output_ascii_precision_)
                      << xi[ix + 2 * icell] * sin_thetai[iy + 2 * icell] *
                             cos_phii[iz + 2 * icell]
                      << " "
                      << xi[ix + 2 * icell] * sin_thetai[iy + 2 * icell] *
                             sin_phii[iz + 2 * icell]
                      << " " << xi[ix + 2 * icell] * cos_thetai[iy + 2 * icell]
                      << "\n";
                }
              }
            }
          }
        }
      }
      //
      // Write the number of cell data point (number of cells * 9)
      // 9 is coming from having 8 corners + 1 number, which is the number
      // of corners
      //
      out_stream << "CELLS " << n_cell << " " << n_hexahedron * 9 + n_wedge * 7
                 << "\n";
      // Write out the indices of the cell interface mesh that define a
      // hexahedron (VTK cell type #12)
      //
      // The indexing of a hexahedron is as follows
      //
      //  Hexahedron:
      //
      //                  7________6
      //                 /|      / |
      //                / |     /  |
      //               4_------5   |             z ^   ^ y
      //               |  3____|___2               |  /
      //               | /     |  /                | /
      //               |/      | /                 |/
      //               0-------1                   0-----> x
      //
      // Cell indexing:
      //                  1------- 5
      //                 /|      / |
      //                / |     /  |
      //               0-------4   |             z ^   ^ y
      //               |  3----|---7               |  /
      //               | /     |  /                | /
      //               |/      | /                 |/
      //               2-------6                   0-----> x
      //
      //  Wedge (Northern hemisphere):
      //
      //              Pole                       Pole
      //                3 ----- 5                 5 ----- 4
      //                |\     /|                 |\     /|
      //                | \   / |                 | \   / |
      //                |  \ /  |                 |  \ /  |
      //                |   4   |                 |   3   |
      //                0 --|-- 2                 2 --|-- 1
      //                 \  |  /                   \  |  /
      //                  \ | /                     \ | /
      //                   \|/                       \|/
      //                    1                         0
      //
      for (int icell = 0; icell < n_cell; ++icell) {
        std::vector<int> id(8, 0);
        int nc = 0;
        if (cell_type[icell] == 12) {
          nc = 8;
            id[0] = 0;
            id[1] = 2;
            id[2] = 6;
            id[3] = 4;
            id[4] = 1;
            id[5] = 3;
            id[6] = 7;
            id[7] = 5;
        } else {
          nc = 6;
          //
          // Wedge cells around the north pole
          //
          if (yi[2*icell] < M_PI*0.5) {
            id[0] = 0;
            id[1] = 2;
            id[2] = 6;
            id[3] = 1;
            id[4] = 3;
            id[5] = 7;
          } else {
            id[0] = 0;
            id[1] = 2;
            id[2] = 4;
            id[3] = 1;
            id[4] = 3;
            id[5] = 5;
          }
        }
        out_stream << nc << " ";
        for (int ic = 0; ic < nc; ++ic) {
          out_stream << id[ic] + icell * 8 << " ";
        }
        out_stream << "\n";
      }

      out_stream << "CELL_TYPES " << n_cell << "\n";
      for (int icell = 0; icell < n_cell; ++icell) {
        out_stream << cell_type[icell] << "\n";
      }

      //
      // Write the scalar variables
      //
      out_stream << "CELL_DATA " << n_cell << "\n";
      for (size_t ivar = 0; ivar < var_name.size(); ++ivar) {
        if (this->is_var_scalar(var_name[ivar])) {
          out_stream << "SCALARS " << var_name[ivar] << " double \n";
          out_stream << "LOOKUP_TABLE default\n";

          std::vector<double> scalar(n_cell);
          this->_get_scalar(var_name[ivar], scalar.data(), n_cell);
          for (int i = 0; i < n_cell; ++i) {
            out_stream << std::fixed << std::scientific
                       << std::setprecision(this->output_ascii_precision_)
                       << scalar[i] << "\n";
          }
        }
      }
      //
      // Write the vector variables
      //
      for (size_t ivar = 0; ivar < var_name.size(); ++ivar) {
        if (!this->is_var_scalar(var_name[ivar])) {
          out_stream << "VECTORS " << var_name[ivar] << " double\n";

          std::vector<double> vector(n_cell * n_dim);
          this->_get_vector(var_name[ivar], vector.data(), n_cell * n_dim,
                            n_dim);
          for (int i = 0; i < n_cell; ++i) {
            for (int idim = 0; idim < n_dim; ++idim) {
              out_stream << std::fixed << std::scientific
                         << std::setprecision(this->output_ascii_precision_)
                         << vector[idim + n_dim * i] << " ";
            }
            out_stream << "\n";
          }
        }
      }
    } else  {
      throw std::runtime_error(
          "VTK output is not yet implemented for cylindrical grid");
    }
  }

  out_stream.close();
  std::cout << "Done" << std::endl;
}

#endif /* INCLUDE_GRIDDED_DATA_H_ */
