// ============================================================================
/// \file amr_grid.h
/// \brief Grid with adaptive mesh refinement
//
// This file is part of sphtool:
// Gridding and visualisation package for SPH simulations
//
// Copyright (C) 2018 Attila Juhasz
// Author: Attila Juhasz
//
// sphtool is free software; you can redistribute it and/or modify it under
// the terms of the GNU General Public License as published by the Free Software
// Foundation; either version 2 of the License, or (at your option) any later
// version.
//
// sphtool is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.
// ============================================================================

#ifndef INCLUDE_AMR_GRID_H_
#define INCLUDE_AMR_GRID_H_

#include <vector>
#include <string>
#include <fstream>
#include <iostream>
#include "../include/grid_base.h"
#include "../include/tree.h"
//
//
//
class AMRGrid : public Tree<double>, public GridBase {
 public:
  AMRGrid();
  explicit AMRGrid(PointDataSet* ptr_point_data_set);
  AMRGrid(PointDataSet* ptr_point_data_set, const std::string& crd_system);
  virtual ~AMRGrid();
  AMRGrid(const AMRGrid& obj);
  AMRGrid& operator=(const AMRGrid& obj);
  AMRGrid(AMRGrid&& obj);
  AMRGrid& operator=(AMRGrid&& obj);

  void BuildGrid(const int& max_n_point_per_cell, const int& max_tree_depth,
                 const std::vector<double>& root_crd_min,
                 const std::vector<double>& root_crd_max,
                 const std::string& crd_system);
  /// \brief Returns the number of grid points / cells in the ith axis
  /// @param iaxis                        Axis index
  /// @return Returns the number of grid points / cells in the ith axis
  int GetNCell(const int& iaxis);
  ///
  /// \brief Returns the number of leaf cells
  /// @return the number of leaf cells
  int GetNCell();
  ///
  /// \brief Returns the grid type ("amr" or "regular")
  /// @return the grid type
  std::string GetGridType();
  ///
  /// \brief Returns the cell center array of the leaf cells
  /// @param[in] iaxis                    Axis index (Cartesian: x=0, y=1, z=2,
  ///                                     Spherical: r=0, theta=1, phi=2)
  /// @param[in] vector_double_out        Cell center array
  /// @param[in] dim                      Size of the cell center array
  ///
  void GetCellCenter(const int& iaxis, double* vector_double_out,
                     const int& dim);
  ///
  /// \brief Set the cell center array of the leaf cells
  /// @param[in] iaxis                    Axis index (Cartesian: x=0, y=1, z=2,
  ///                                     Spherical: r=0, theta=1, phi=2)
  /// @param[in] vector_double_in         Cell center array
  /// @param[in] dim                      Size of the cell center array
  ///
  void SetCellCenter(const int& iaxis, double* vector_double_in, const int&dim);
  ///
  /// \brief Returns the cell interface array of the leaf cells
  ///
  /// The number of elements in the output array is twice the number of points,
  /// and the indexing is [iboundary + 2*i_point]
  ///
  /// @param[in] iaxis                    Axis index (Cartesian: x=0, y=1, z=2,
  ///                                     Spherical: r=0, theta=1, phi=2)
  /// @param[in] vector_double_in         Cell center array
  /// @param[in] dim                      Size of the cell center array
  ///
  void GetCellInterface(const int& iaxis, double* vector_double_out,
                        const int& dim);
  ///
  /// \brief Writes the grid to file
  ///
  /// @param[in] file_name                Name of the file to write the grid to
  /// @param[in] format                   Format of the file (currently only
  ///                                     "radmc3d" is supported)
  void WriteToFile(const std::string& file_name, const std::string& format);
  ///
  /// \brief Read the grid to from file
  ///
  /// @param[in] file_name                Name of the file to write the grid to
  /// @param[in] format                   Format of the file (currently only
  ///                                     "radmc3d" is supported)
  void ReadFromFile(const std::string& file_name, const std::string& format);
  ///
  /// \brief Sets the output precision for formatted ASCII output
  /// @param[in] precision                Number of significant digits in
  ///                                     exponential notation
  void SetOutputPrecision(const int& precision);
  ///
  /// \brief Returns the output precision (number of significant digits) in
  /// formatted ASCII output
  ///
  int GetOutputPrecision();
  ///
  /// \brief Resets the AMR tree
  ///
  void ResetGrid();
  ///
  /// \brief Returns a vector containing pointers to each leaf node
  ///
  std::vector<TreeNode<double>*> GetPointerToLeafNodes();
  ///
  /// \brief Splits/resolves a cell adding child nodes to it
  ///
  /// SplitCell splits / resolves a node adding child nodes in a similar way to
  /// ResolveNode, but SplitCell does not changes/updates the point_id_first,
  /// point_id_last members and does not care about point_data_sets.
  ///
  /// @param[in] ptr_node                 Pointer to a node to be resolved
  ///
  void SplitCell(TreeNode<double>* ptr_node);
  ///
  /// \brief Recursive function to verify the grid
  ///
  /// It checks the consistency between the grid depth and the cell size
  ///
  /// @param ptr_node                     Pointer to a node
  ///
  void VerifyGrid(TreeNode<double>* ptr_node);

 private:
  ///
  /// \brief Recursive function to child nodes (sub-tree)
  /// @param[in] node_from         Node/cell whose children to be copied
  /// @param[out] ptr_node_to      Node/cell to copy to
  void CopyChildNodes(const TreeNode<double>& node_from,
                      TreeNode<double>* ptr_node_to);

  ///
  /// \brief Recursive function to gather the cell center coordinates
  /// @param[in] iaxis                Coordinate axis whose coordinate is to be
  ///                                 collected
  /// @param[in] node                 Node
  /// @param[in,out] cell_center_arr  Array to gather the cell centers
  ///                                 coordinates to
  /// @param[in,out] idx              Running index of cell_center_arr
  void CollectCellCenters(const int& iaxis, const TreeNode<double>& node,
                          double* cell_center_arr, int* ptr_idx);
  ///
  /// \brief Recursive function to set the cell center coordinates
  /// @param[in] iaxis                Coordinate axis whose coordinate is to be
  ///                                 set
  /// @param[in] node                 Node
  /// @param[in,out] cell_center_arr  Array to get the cell centers
  ///                                 coordinates from
  /// @param[in,out] idx              Running index of cell_center_arr
  void DistributeCellCenters(const int& iaxis, TreeNode<double>* ptr_node,
                          double* cell_center_arr, int* ptr_idx);
  ///
  /// \brief Recursive function to gather the cell interface coordinates
  /// @param[in] iaxis                Coordinate axis whose coordinate is to be
  ///                                 collected
  /// @param[in] node                 Node
  /// @param[in,out] cell_interface_arr  Array to gather the cell centers
  ///                                 coordinates to
  /// @param[in,out] idx              Running index of cell_center_arr
  void CollectCellIntefaces(const int& iaxis, const TreeNode<double>& node,
                          double* cell_interface_arr, int* ptr_idx);

  ///
  /// \brief Collects pointers to leaf nodes in a linear container
  ///
  /// @param[in] node                     Tree node
  /// @param[in,out] ptr_to_tree_nodes    Vector of Tree node pointers
  /// @param[in,out] ptr_idx              Running index of ptr_to_tree_nodes
  ///
  void CollectPointersToLeafNodes(
      TreeNode<double>* node, std::vector<TreeNode<double>*>* ptr_to_tree_nodes,
      int* ptr_idx);

  ///
  /// \brief Recursive function to write grid cells to file
  ///
  /// @param[in] ptr_out_stream           Pointer to an output file stream to
  ///                                     write to
  /// @param[in] node                     A node to be written to file
  void WriteCellToFile(std::ofstream* ptr_out_stream,
                       const TreeNode<double>& node);
  ///
  /// \brief Recursive function to read grid cells from file
  ///
  /// @param[in] input_stream             An input stream to read from
  /// @param[in,out] ptr_node             Pointer to a node read into
  ///
  void ReadCellFromFile(std::fstream* input_stream, TreeNode<double>* ptr_node);
  // Grid type ("amr" or "regular")
  static const char *grid_type_;
  // Output ascii precision
  int output_ascii_precision_;
};

#endif /* INCLUDE_AMR_GRID_H_ */
