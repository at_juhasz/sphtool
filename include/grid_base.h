// ============================================================================
/// \file grid_base.h
/// \brief Spatial grid base class
//
// This file is part of sphtool:
// Gridding and visualisation package for SPH simulations
//
// Copyright (C) 2018 Attila Juhasz
// Author: Attila Juhasz
//
// sphtool is free software; you can redistribute it and/or modify it under
// the terms of the GNU General Public License as published by the Free Software
// Foundation; either version 2 of the License, or (at your option) any later
// version.
//
// sphtool is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.
// ============================================================================

#ifndef INCLUDE_GRID_BASE_H_
#define INCLUDE_GRID_BASE_H_

#include <string>
#include <vector>
//
//
//
class GridBase {
 public:
  GridBase();
  virtual ~GridBase();

  /// \brief Returns the coordinate system type (0-99 Cartesian, 100-199
  ///        Spherical, 200-299 Cylindrical)
  /// @return the coordinate system type
  std::string GetCrdSystem();
  ///
  /// \brief Sets the coordinate system type (0-99 Cartesian, 100-199
  ///        Spherical, 200-299 Cylindrical)
  /// @param[in] crd_system               Coordinate system type
  void SetCrdSystem(const std::string& crd_system);
  /// \brief Returns true if the dimension active, false if not
  /// @param iaxis                        Axis index
  /// @return whether or not the dimension is active
  bool GetActiveDimension(const int& iaxis);

  /// \brief Returns the number of grid points / cells in the ith axis
  /// @param iaxis                        Axis index
  /// @return Returns the number of grid points / cells in the ith axis
  virtual int GetNCell(const int& iaxis);
  /// \brief Returns the total number of grid points / cells (nx*ny*nz)
  /// @param iaxis                        Axis index
  /// @return the total number of grid points / cells
  virtual int GetNCell();
  ///
  /// \brief Returns the cell center array of the leaf cells
  /// @param[in] iaxis                    Axis index (Cartesian: x=0, y=1, z=2,
  ///                                     Spherical: r=0, theta=1, phi=2)
  /// @param[in] vector_double_in         Cell center array
  /// @param[in] dim                      Size of the cell center array
  ///
  virtual void GetCellCenter(const int& iaxis, double* vector_double_out,
                     const int& dim);
  ///
  /// \brief Writes the grid to file
  ///
  /// @param[in] file_name                Name of the file to write the grid to
  /// @param[in] format                   Format of the file (currently only
  ///                                     "radmc3d" is supported)
  virtual void WriteToFile(const std::string& file_name,
                           const std::string& format);
  ///
  /// \brief Read the grid to from file
  ///
  /// @param[in] file_name                Name of the file to write the grid to
  /// @param[in] format                   Format of the file (currently only
  ///                                     "radmc3d" is supported)
  virtual void ReadFromFile(const std::string& file_name,
                            const std::string& format);
  ///
  /// \brief Resets the grid
  ///
  virtual void ResetGrid();

 protected:
  // Coordinate system (0-99 Cartesian, 100-199 - Spherical)
  std::string crd_system_;
  // Active dimension
  std::vector<bool> active_dim_;


};

#endif /* INCLUDE_GRID_BASE_H_ */
