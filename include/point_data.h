// ============================================================================
/// \file point_data.h
/// \brief Particle based data containers
//
// This file is part of sphtool:
// Gridding and visualisation package for SPH simulations
//
// Copyright (C) 2018 Attila Juhasz
// Author: Attila Juhasz
//
// sphtool is free software; you can redistribute it and/or modify it under
// the terms of the GNU General Public License as published by the Free Software
// Foundation; either version 2 of the License, or (at your option) any later
// version.
//
// sphtool is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.
// ============================================================================

#ifndef INCLUDE_POINT_DATA_H_
#define INCLUDE_POINT_DATA_H_

#include <iostream>
#include <vector>
#include <map>
#include <string>

///
/// \brief Data container for a single particle
///
class PointData {
 public:
  /// \brief Constructor
  PointData();
  /// \brief Destructor
  virtual ~PointData();
  /// \brief Coordinate array
  std::vector<double> crd;
  /// \brief Point/particle ID
  int pid;
  /// \brief Vector containing the scalar variables
  std::vector<double> scalars;
  /// \brief Vector containing the vector variables
  std::vector<double> vectors;
};

///
/// \brief Container for all SPH particles
///
class PointDataSet {
  template<class T>
  friend class TreeNode;

  template<class T>
  friend class Tree;

  friend class SPHData;

 public:
  PointDataSet();
  ///
  /// \brief Point data set constructor
  /// @param[in] n_dim      Number of dimensions
  /// @param[in] n_point    Number of points in the data set
  /// @param[in] n_scalar   Number of scalars (apart from density)
  /// @param[in] n_vector   Number of vectors
  PointDataSet(const int& n_point, const int& n_dim);
  ///
  /// \brief Destructor
  virtual ~PointDataSet();

  /// \Copy constructor
  PointDataSet(const PointDataSet& obj);
  /// \Copy assignment operator
  PointDataSet& operator=(const PointDataSet& obj);
  ///
  /// @return The number of dimensions in the data set
  int get_ndim();
  ///
  /// @return The number of points in the data set
  int get_npoint();
  ///
  /// @return The number of scalars in the data set
  int get_nscalar();
  ///
  /// @return The number of vectors in the data set
  int get_nvector();
  /// @return The name of all scalars
  std::vector<std::string> get_all_scalar_names();
  /// @return The name of all vectors
  std::vector<std::string> get_all_vector_names();
  ///
  /// \brief Sets a scalar variable
  /// @param[in] var_name           Name of the scalar
  /// @param[in] vector_double_in   Array containing the scalar variable
  /// @param[in] dim                Length / size of the scalar array
  void _set_scalar(const std::string& var_name, double* vector_double_in,
                 int dim);
  ///
  /// \brief Sets a vector variable
  /// @param[in] var_name           Name of the vector
  /// @param[in] vector_double_in   Array containing the vector variable
  /// @param[in] dim1               Number of points/coordinates at which the
  ///                               vector is defined
  /// @param[in] dim2               Number of spatial dimensions in the vector
  void _set_vector(const std::string& var_name, double* vector_double_in,
                 int dim, int dim2);

  ///
  /// \brief Returns a scalar variable
  /// @param[in] var_name           Name of the scalar
  /// @param[in] vector_double_out  Array containing the scalar variable
  /// @param[in] dim                Length / size of the scalar array
  void _get_scalar(const std::string& var_name, double* vector_double_out,
                 int dim_out);
  ///
  /// \brief Returns a vector variable
  /// @param[in] var_name           Name of the vector
  /// @param[in] vector_double_out  Array containing the vector variable
  /// @param[in] dim1               Number of points/coordinates at which the
  ///                               vector is defined
  /// @param[in] dim2               Number of spatial dimensions in the vector
  void _get_vector(const std::string& var_name, double* vector_double_out,
                 int dim_out, int dim2);

  ///  \brief Erases a scalar variable from the dataset
  ///  @param[in] var_name          Name of the scalar variable to be erased
  void delete_scalar(const std::string& var_name);

  ///  \brief Erases a vector variable from the dataset
  ///  @param[in] var_name          Name of the vector variable to be erased
  void delete_vector(const std::string& var_name);

  ///
  /// \brief Checks if a variable is scalar
  /// Throws a runtime_error if no variable is defined with name var_name
  ///
  /// @param[in] var_name           Name of the variable
  /// @return True if if the variable is a scalar and false if it is a vector
  bool is_var_scalar(const std::string& var_name);
  ///
  /// \brief Checks if all necessary variables have been set
  ///
  virtual void validate();
  ///
  /// \brief Sets the number of scalar variables
  /// @param[in] n_scalar       Number of scalars
  /// @param[in] scalar_name    Name of the scalar variables
  void _set_nscalar(const int& n_scalar,
                  const std::vector<std::string>& scalar_name);
  ///
  /// \brief Sets the number of vector variables
  /// @param[in] n_vector       Number of vectors
  /// @param[in] vector_name    Name of the vector variables
  void _set_nvector(const int& n_vector,
                  const std::vector<std::string>& vector_name);
  ///
  /// \brief Resets the point set to a new size (number of points)
  /// @param[in] n_point    Number of points in the point set
  ///
  void set_size(const int& n_point, const int& n_dim);
  ///
  /// \brief Returns the coordinate boundaries of the whole point set
  /// for a given dimension
  ///
  /// @param[in] iaxis      Coordinate axis index
  /// @param[out] vector_double_out  Upper and lower coordinate boundaries
  /// @param[out] dim_out            Size of vector_double_out (should be 2)
  void _get_coordinate_axis_boundaries(const int& iaxis,
                                       double* vector_double_out, int dim_out);
  ///
  /// \brief Returns the coordinate system of the point data set
  ///
  std::string _get_crd_system();

  ///
  /// \brief Sets the coordinate system of the point data set
  ///
  /// @param[in] crd_system       Coordinate system ("cartesian" or "spherical")
  void _set_crd_system(const std::string& crd_system);
  ///
  /// \brief Converts data point coordinates to a given coordinate system
  ///
  /// @param crd_system    Name of the coordinate system to convert to
  ///                      ("cartesian", "spherical" or "cylindrical")
  void _transform_to_crd_system(const std::string& crd_system);


#ifndef TEST_BUILD

 protected:
#endif
  ///
  /// \brief Adds point data to the data set
  /// @param[in] point_data   Data container of an SPH particle
  void AddPointData(const PointData& point_data);
  ///
  /// \brief Deletes all dynamically allocated memory for point data
  ///
  void DeleteAllPoints();
  ///
  /// \brief Returns the coordinate boundaries of the whole point set
  /// for each dimension
  ///
  /// @param[out] crd_min   Lower coordinate boundary in each dimension
  /// @param[out] crd_max   Upper coordinate boundary in each dimension
  void GetCoordinateBoundaries(double* crd_min, double* crd_max);

  ///
  /// \brief Sets the number of dimensions in the data set
  /// @param[in] n_dim    Number of dimensions
  ///
  void SetNDim(const int& n_dim);
  ///
  /// \brief Sets the number of points in the data set
  /// @param[in] n_point  Number of points
  ///
  void SetNPoint(const int& n_point);
  ///
  /// @param[in] scalar_name  Name of the scalar
  /// @return The index of the scalar with a given name
  ///
  int GetScalarIndex(const std::string& scalar_name);
  ///
  /// @param[in] vector_name  Name of the vector
  /// @return The index of the vector with a given name
  ///
  int GetVectorIndex(const std::string& vector_name);
  ///
  /// @param[in] scalar_index  Index of the scalar
  /// @return The name of the scalar with a given name
  std::string GetScalarName(const int& scalar_index);
  ///
  /// @param[in] vector_index  Index of the vector
  /// @return The name of the vector with a given name
  std::string GetVectorName(const int& vector_index);
  ///
  /// \brief Returns the index array that restores the original point data
  /// sequence
  ///
  /// When a tree is built over a PointDataSet the sequence of PointData
  /// containers may change during the tree building, but the original indices
  /// are stored. This function returns the index vector that restores the
  /// original PointData container sequence.
  ///
  /// @param vector_int_out         Array for the point indices
  /// @param dim_out                Size of of vector_int_out (should be equal
  ///                               to the the number of points in the dataset).
  void GetOriginalIndexOrder(int* vector_int_out, const int& dim_out);

  /// \brief Number of dimensions
  int n_dim_;
  /// \brief Number of points in the set
  int n_point_;
  /// \brief Number of scalars (apart from density)
  int n_scalar_;
  /// \brief Number of vectors
  int n_vector_;
  /// \brief coordinate system the points are stored in
  std::string crd_system_;
  ///
  /// Connects the name of a scalar variable to an index in the array
  ///
  std::map<std::string, int> scalar_index_;
  ///
  /// Connects the name of a vector variable to an index in the array
  ///
  std::map<std::string, int> vector_index_;

  /// \brief Vector containing the data for each point in the set
  std::vector<PointData*> ptr_points_;
};

#endif /* INCLUDE_POINT_DATA_H_ */
