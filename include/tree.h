// ============================================================================
/// \file tree.h
/// \brief Templates for binary-, quad- or octree.
//
// This file is part of sphtool:
// Gridding and visualisation package for SPH simulations
//
// Copyright (C) 2018 Attila Juhasz
// Author: Attila Juhasz
//
// sphtool is free software; you can redistribute it and/or modify it under
// the terms of the GNU General Public License as published by the Free Software
// Foundation; either version 2 of the License, or (at your option) any later
// version.
//
// sphtool is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.
// ============================================================================

#ifndef TREE_H_
#define TREE_H_

#include <vector>
#include <utility>
#include <cmath>
#include <stdexcept>
#include "../include/point_data.h"
///
/// \brief TreeNode class
///
template<class T>
class TreeNode {
 public:
  explicit TreeNode(const int& n_dim);
  virtual ~TreeNode();

  ///
  /// \brief Splits the node to 2^NDIM equal parts
  ///
  /// Checks if the node needs to be resolved by counting how many points there
  /// are in the current node/cell. If more than a threshold
  /// (max_n_point_per_cell) the node will be resolved.
  ///
  /// @param[in] ptr_point_data_set         Pointer to the point data set
  /// @param[in] max_n_point_per_cell       Max number of points per cell/node.
  ///                                       If there are more points than this
  ///                                       number in the current node/cell the
  ///                                       node/cell will be resolved.
  /// @param[in] max_depth                  Max tree depth
  /// @param[in] ptr_tree_depth             Current tree depth (highest
  ///                                       resolution level)
  /// @param[in] ptr_node_id                Running index of tree nodes
  void ResolveNode(PointDataSet* ptr_point_data_set,
                   const int& max_n_point_per_cell, const int& max_depth,
                   int* ptr_tree_depth, int* ptr_node_id);
  ///
  /// \brief Recursive functio nto delete all child nodes
  ///
  void DeleteChildNodes();

  ///
  /// \brief Print the coordinates of the nodes (cell corners) of all leaf nodes
  ///
  void PrintLeafNodes();
  ///
  /// \brief Returns the coordinates (cell corners) of all leaf nodes
  ///
  /// The indexing of the 1D arrays is inode*Ndim + idim, where inode is the
  /// node index and idim is the dimension index, while Ndim is the number of
  /// dimensions
  ///
  /// @param[out] crd_min   Lower boundary of the cell in all dimensions
  /// @param[out] crd_max   Upper boundary of the cell in all dimensions
  ///
  void GetLeafNodeCoordinates(std::vector<T>* ptr_crd_min,
                              std::vector<T>* ptr_crd_max);
  ///
  /// \brief Returns a pointer to the cell that contains a given point
  ///
  /// @param[in] point_crd  Coordinates of the point whose container cell
  ///                       is to be determined
  /// @return
  TreeNode<T>* GetContainerNode(const std::vector<T>& point_crd);
  ///
  /// \brief Returns the indices of all points for which
  /// |x_p,i - x_c,i| < d, where x_p,i is the point coordinate in the ith
  /// dimension, x_c,i is the coordinate of a given point in space, while
  /// d is a given distance
  ///
  /// @param[in] point_crd              Coordinates of the point
  /// @param[in] distance               Distance threshold
  /// @param[out] point_id              Point indices
  void GetPointIDsWithinDistance(const T* point_crd, const T& distance,
                                 std::vector<int>* ptr_point_id);

  /// \brief A boolean variable being True if the node is a leaf node and False
  /// if it is a branch
  bool is_leaf;
  /// \brief Node depth
  int node_depth;
  /// \brief Coordinates of the node/cell center
  std::vector<T> center_crd;
  /// \brief Width of the cell in each dimension
  std::vector<T> cell_half_width;
  /// \brief Child nodes (if is_leaf = false)
  std::vector<TreeNode<T>*> ptr_child_nodes;
  /// \brief Index of the first point in the point set contained by this node;
  int point_id_first;
  /// \brief Index of the last point in the point set contained by this node;
  int point_id_last;
  /// \brief Lower coordinate boundary of the bounding box (region of influence)
  ///        of the particles in the cell/node
  std::vector<T> bbox_crd_min;
  /// \brief Upper coordinate boundary of the bounding box (region of influence)
  ///        of the particles in the cell/node
  std::vector<T> bbox_crd_max;
  /// \brief ID number of the tree node
  int node_id;
};

///
/// \brief Tree class
///
template<class T>
class Tree : public TreeNode<T> {
 public:
  ///
  /// \brief Tree constructor
  ///
  Tree();
  ///
  /// \brief Tree constructor
  /// @param ptr_point_data_set         Point data set over which the tree
  ///                                   should be built
  ///
  explicit Tree(PointDataSet* ptr_point_data_set);
  ///
  /// \brief Tree destructor
  ///
  virtual ~Tree();
  ///
  /// \brief Builds a tree over the contained point set
  ///
  /// @param[in] max_n_point_per_cell
  /// @param[in] max_tree_depth
  void BuildTree(const int& max_n_point_per_cell,
                 const int& max_tree_depth);
  ///
  /// \brief Builds a tree over the contained point set
  ///
  /// @param[in] max_n_point_per_cell   Maximum number of points per node
  /// @param[in] max_tree_depth         Maximum number of levels
  /// @param[in] root_crd_min           Lower coordinate boundaries of the root
  ///                                   cell (cell wall coordinates)
  /// @param[in] root_crd_max           Upper coordinate boundaries of the root
  ///                                   cell (cell wall coordinates)
  ///
  void BuildTree(const int& max_n_point_per_cell,
                 const int& max_tree_depth,
                 const std::vector<double>& root_crd_min,
                 const std::vector<double>& root_crd_max);
  ///
  /// \brief Sets a pointer to the point data set container
  /// @param point_data_set   Pointer to the data set
  ///
  void SetPointData(PointDataSet* ptr_point_data_set);

  ///
  /// \brief Returns a pointer to the point data set container
  ///
  PointDataSet* GetPointData();
  ///
  /// \brief Resets the tree
  ///
  void Reset();
  ///
  /// \brief Resets the tree
  ///
  /// @param[in] ptr_node               Pointer to a TreeNode
  void VerifyTree(TreeNode<T>* ptr_node);
  ///
  /// \brief Calculate the effective boundaries of the OcTree cells
  ///
  /// The effective boundary of a cell in the i-th dimension is
  /// min_j(crd_cell_i, crd_part_j,i + 2*h_j), max_j(crd_cell_i, crd_part_j,i +
  /// 2*h_j), i.e. it defines of the region of influence of the particles
  /// in the tree cell
  ///
  /// @param[in] ptr_node                Pointer to a TreeNode
  /// @param[in] ind_h                   Scalar index of the smoothing length
  ///
  void CalculateBBoxCoordinates(TreeNode<T>* ptr_node, const int& ind_h);
  ///
  /// \brief Returns the particle indices whose volume contains a given point
  ///
  /// @param[in] crd                     Cartesian coordiante of the point
  /// @param[in] ptr_node                Pointer to a TreeNode
  /// @param[out] pid                    IDs of SPH particles that overlap with
  ///                                    the point
  ///
  void GetOverlappingParticleIndices(const std::vector<double>& crd,
                                     TreeNode<T>* ptr_node,
                                     std::vector<int>* ptr_pid);

  ///
  /// \brief Returns the IDs of the leaf nodes
  ///
  std::vector<int> GetLeafNodeIDs();
  ///
  /// \brief Returns the number of leaf cells
  /// @return the number of leaf cells
  int GetNLeaf();
  /// \brief Returns the total number of nodes in the AMR mesh
  /// @return the total number of nodes in the mesh
  int GetNNode();
  ///
  /// \brief Recursive function to count leaf cells
  /// @param[in] node              Node whose children to be counted
  void CountLeafs(const TreeNode<double>& node);
  ///
  /// \brief Recursive function to count all nodes/cells
  /// @param[in] node              Node whose children to be counted
  void CountNodes(const TreeNode<double>& node);
  ///
  /// \brief Set the maximum number of points in a given node cell (node
  /// resolution criterion)
  ///
  /// @param[in] max_n_point_per_cell   Max. number of points per node cell
  void SetMaxNPointPerCell(const int& max_n_point_per_cell);
  ///
  /// \brief Returns the maximum number of points in a given node cell (node
  /// resolution criterion)
  ///
  int GetMaxNPointPerCell();
  ///
  /// \brief Sets the maximum tree depth
  /// @param[in] max_tree_depth           Maximum tree depth
  ///
  void SetMaxTreeDepth(const int& max_tree_depth);
  ///
  /// \brief Returns the maximum tree depth
  ///
  int GetMaxTreeDepth();
  ///
  /// \brief Returns the actual tree depth
  ///
  int GetTreeDepth();
  ///
  /// \brief Returns the lower boundary of the root node in each coordinate
  ///
  std::vector<T> GetRootCrdMin();
  ///
  /// \brief Returns the upper boundary of the root node in each coordinate
  ///
  std::vector<T> GetRootCrdMax();
#ifndef TEST_BUILD

 protected:
#endif
  // Pointer to the point data set over which the tree is built
  PointDataSet* ptr_point_data_set_;
  // Maximum number of point per tree node
  int max_n_point_per_cell_;
  // Maximum tree depth
  int max_tree_depth_;
  // Actual tree depth
  int tree_depth_;
  // Lower coordinate boundary of the tree
  std::vector<T> crd_min_;
  // Upper coordinate boundary of the tree
  std::vector<T> crd_max_;
  // Number of leaf nodes
  int n_leaf_;
  // Number of all nodes
  int n_node_;

  ///
  /// \brief Collects the IDs of the leaf nodes
  ///
  /// @param[in] ptr_node                Pointer to a TreeNode
  /// @param[out] ptr_cell_node_id       Array to collect the leaf node ids in
  /// @param[in,out] ind                 Running index of the leaf node id array
  ///
  void CollectLeafNodeIDs(TreeNode<T>* ptr_node,
                          std::vector<int>* ptr_cell_node_id, int* ind);
};

// ----------------------------------------------------------------------
//
// ----------------------------------------------------------------------
template<class T>
TreeNode<T>::TreeNode(const int& n_dim) {
  is_leaf = true;
  center_crd.resize(n_dim);
  cell_half_width.resize(n_dim);
  for (int i = 0; i < n_dim; ++i) {
    center_crd[i] = 0.0;
    cell_half_width[i] = 0.0;
  }
  point_id_first = 0;
  point_id_last = 0;
  node_depth = 0;
  node_id = 0;
}

// ----------------------------------------------------------------------
//
// ----------------------------------------------------------------------
template<class T>
TreeNode<T>::~TreeNode() {
  if (!is_leaf) {
    for (size_t i = 0; i < ptr_child_nodes.size(); ++i) {
      delete ptr_child_nodes[i];
      ptr_child_nodes[i] = nullptr;
    }
    is_leaf = true;
  }
}

// ----------------------------------------------------------------------
//
// ----------------------------------------------------------------------
template<class T>
void TreeNode<T>::DeleteChildNodes() {
  if (!is_leaf) {
    for (size_t i = 0; i < ptr_child_nodes.size(); ++i) {
      ptr_child_nodes[i]->DeleteChildNodes();
      delete ptr_child_nodes[i];
    }
    ptr_child_nodes.resize(0);
    is_leaf = true;
  }
}

// ----------------------------------------------------------------------
//
// ----------------------------------------------------------------------
template<class T>
void TreeNode<T>::PrintLeafNodes() {
  if (!is_leaf) {
    for (size_t i = 0; i < ptr_child_nodes.size(); ++i) {
      ptr_child_nodes[i]->PrintLeafNodes();
    }
  } else {
    for (size_t idim = 0; idim < center_crd.size(); ++idim) {
      std::cout << center_crd[idim] << " ";
    }
    std::cout << node_depth << std::endl;
  }
}

// ----------------------------------------------------------------------
//
// ----------------------------------------------------------------------
template <class T>
void TreeNode<T>::GetPointIDsWithinDistance(const T* point_crd,
                                            const T& distance,
                                            std::vector<int>* ptr_point_id) {
  if (!is_leaf) {
    for (size_t i = 0; i < ptr_child_nodes.size(); ++i) {
      ptr_child_nodes[i]->GetPointIDsWithinDistance(
          point_crd, distance, ptr_point_id);
    }
  } else {
    int n_dim = static_cast<int>(center_crd.size());
    if (n_dim == 1) {
      if (std::abs(center_crd[0] - point_crd[0]) <=
          (cell_half_width[0] + distance)) {
        for (int ip = point_id_first; ip < point_id_last; ++ip) {
          (*ptr_point_id).push_back(ip);
        }
      }
    } else if (n_dim == 2) {
      if ((std::abs(center_crd[0] - point_crd[0]) <=
           (cell_half_width[0] + distance)) &
          (std::abs(center_crd[1] - point_crd[2]) <=
           (cell_half_width[1] + distance))) {
        for (int ip = point_id_first; ip < point_id_last; ++ip) {
          (*ptr_point_id).push_back(ip);
        }
      }
    } else if (n_dim == 3) {
      if ((std::abs(center_crd[0] - point_crd[0]) <=
           (cell_half_width[0] + distance)) &
          (std::abs(center_crd[1] - point_crd[1]) <=
           (cell_half_width[1] + distance)) &
          (std::abs(center_crd[2] - point_crd[2]) <=
           (cell_half_width[2] + distance))) {
        for (int ip = point_id_first; ip < point_id_last; ++ip) {
          (*ptr_point_id).push_back(ip);
        }
      }
    }
  }
}

// ----------------------------------------------------------------------
//
// ----------------------------------------------------------------------
template <class T>
void TreeNode<T>::GetLeafNodeCoordinates(std::vector<T>* ptr_crd_min,
                                         std::vector<T>* ptr_crd_max) {
  if (!is_leaf) {
    for (size_t i = 0; i < ptr_child_nodes.size(); ++i) {
      ptr_child_nodes[i]->GetLeafNodeCoordinates(ptr_crd_min, ptr_crd_max);
    }
  } else {
    for (size_t idim = 0; idim < center_crd.size(); ++idim) {
      (*ptr_crd_min).push_back(center_crd[idim] - cell_half_width[idim]);
      (*ptr_crd_max).push_back(center_crd[idim] + cell_half_width[idim]);
    }
  }
}

// ----------------------------------------------------------------------
//
// ----------------------------------------------------------------------
template <class T>
TreeNode<T>* TreeNode<T>::GetContainerNode(
    const std::vector<T>& point_crd) {

  TreeNode<T>* container_node = nullptr;

  if (!is_leaf) {
    for (size_t i = 0; i < ptr_child_nodes.size(); ++i) {
      container_node = ptr_child_nodes[i]->GetContainerNode(point_crd);
      if (container_node != nullptr) return container_node;
    }
  } else {
    int n_dim = static_cast<int>(center_crd.size());
    if (n_dim == 1) {
      if ((point_crd[0] >= center_crd[0] - cell_half_width[0]) &
         (point_crd[0] < center_crd[0] + cell_half_width[0])) {
        return this;
      } else {
        return nullptr;
      }
    } else if (n_dim == 2) {
      if ((point_crd[0] >= center_crd[0] - cell_half_width[0]) &
         (point_crd[0] < center_crd[0] + cell_half_width[0]) &
         (point_crd[1] >= center_crd[1] - cell_half_width[1]) &
         (point_crd[1] < center_crd[1] + cell_half_width[1])) {
        return this;
      } else {
        return nullptr;
      }
    } else if (n_dim == 3) {
      if ((point_crd[0] >= center_crd[0] - cell_half_width[0]) &
         (point_crd[0] < center_crd[0] + cell_half_width[0]) &
         (point_crd[1] >= center_crd[1] - cell_half_width[1]) &
         (point_crd[1] < center_crd[1] + cell_half_width[1]) &
         (point_crd[2] >= center_crd[2] - cell_half_width[2]) &
         (point_crd[2] < center_crd[2] + cell_half_width[2])) {
        return this;
      } else {
        return nullptr;
      }
    }
  }
  return container_node;
}


// ----------------------------------------------------------------------
//
// ----------------------------------------------------------------------
template <class T>
void TreeNode<T>::ResolveNode(PointDataSet* ptr_point_data_set,
                              const int& max_n_point_per_cell,
                              const int& max_depth, int* ptr_tree_depth,
                              int* ptr_node_id) {
  int n_point = point_id_last - point_id_first;
  int n_dim = static_cast<int>(center_crd.size());
  //
  // If there are more points than the threshold and the maximum
  // resolution level has not been reached yet split the cell
  //
  if ((n_point > max_n_point_per_cell) & (this->node_depth < max_depth)) {
    int index_first;
    int index_last;
    is_leaf = false;
    if (*ptr_tree_depth < (this->node_depth + 1)) {
      *ptr_tree_depth = this->node_depth + 1;
    }
    //
    // 1D case
    //
    if (n_dim == 1) {
      //
      // Add child nodes
      //
      ptr_child_nodes.resize(2);
      for (int ichild = 0; ichild < 2; ++ichild) {
        ptr_child_nodes[ichild] = new TreeNode<T>(n_dim);
        ptr_child_nodes[ichild]->node_depth = node_depth + 1;
        for (int idim = 0; idim < n_dim; ++idim) {
          ptr_child_nodes[ichild]->cell_half_width[idim] =
              0.5 * cell_half_width[idim];
        }

        ptr_child_nodes[ichild]->node_id = *ptr_node_id;
        *ptr_node_id += 1;
      }

      ptr_child_nodes[0]->center_crd[0] =
          center_crd[0] - 0.5 * cell_half_width[0];
      ptr_child_nodes[1]->center_crd[0] =
          center_crd[0] + 0.5 * cell_half_width[0];
      //
      // Now loop over the particles distribute them among the cells
      //
      index_first = point_id_first;
      index_last = point_id_first;
      for (int ichild = 0; ichild < 2; ++ichild) {
        for (int ip = index_first; ip < point_id_last; ++ip) {
          if ((ptr_point_data_set->ptr_points_[ip]->crd[0] >=
               ptr_child_nodes[ichild]->center_crd[0] -
                   ptr_child_nodes[ichild]->cell_half_width[0]) &
              (ptr_point_data_set->ptr_points_[ip]->crd[0] <
               ptr_child_nodes[ichild]->center_crd[0] +
                   ptr_child_nodes[ichild]->cell_half_width[0])) {
            std::swap(ptr_point_data_set->ptr_points_[index_last],
                      ptr_point_data_set->ptr_points_[ip]);
            index_last += 1;
          }
        }
        ptr_child_nodes[ichild]->point_id_first = index_first;
        ptr_child_nodes[ichild]->point_id_last = index_last;

        index_first = index_last;
        ptr_child_nodes[ichild]->ResolveNode(ptr_point_data_set,
                                             max_n_point_per_cell, max_depth,
                                             ptr_tree_depth, ptr_node_id);
      }
      //
      // 2D case
      //
    } else if (n_dim == 2) {
      ptr_child_nodes.resize(4);
      for (int ichild = 0; ichild < 4; ++ichild) {
        ptr_child_nodes[ichild] = new TreeNode<T>(n_dim);
        ptr_child_nodes[ichild]->node_depth = node_depth + 1;
        for (int idim = 0; idim < n_dim; ++idim) {
          ptr_child_nodes[ichild]->cell_half_width[idim] =
              0.5 * cell_half_width[idim];
        }
        ptr_child_nodes[ichild]->node_id = *ptr_node_id;
        *ptr_node_id += 1;
      }

      ptr_child_nodes[0]->center_crd[0] =
          center_crd[0] - 0.5 * cell_half_width[0];
      ptr_child_nodes[0]->center_crd[1] =
          center_crd[1] - 0.5 * cell_half_width[1];

      ptr_child_nodes[1]->center_crd[0] =
          center_crd[0] + 0.5 * cell_half_width[0];
      ptr_child_nodes[1]->center_crd[1] =
          center_crd[1] - 0.5 * cell_half_width[1];

      ptr_child_nodes[2]->center_crd[0] =
          center_crd[0] - 0.5 * cell_half_width[0];
      ptr_child_nodes[2]->center_crd[1] =
          center_crd[1] + 0.5 * cell_half_width[1];

      ptr_child_nodes[3]->center_crd[0] =
          center_crd[0] + 0.5 * cell_half_width[0];
      ptr_child_nodes[3]->center_crd[1] =
          center_crd[1] + 0.5 * cell_half_width[1];

      //
      // Now loop over the particles distribute them among the cells
      //
      index_first = point_id_first;
      index_last = point_id_first;
      for (int ichild = 0; ichild < 4; ++ichild) {
        for (int ip = index_first; ip < point_id_last; ++ip) {
          if ((ptr_point_data_set->ptr_points_[ip]->crd[0] >=
               ptr_child_nodes[ichild]->center_crd[0] -
                   ptr_child_nodes[ichild]->cell_half_width[0]) &
              (ptr_point_data_set->ptr_points_[ip]->crd[0] <
               ptr_child_nodes[ichild]->center_crd[0] +
                   ptr_child_nodes[ichild]->cell_half_width[0]) &
              (ptr_point_data_set->ptr_points_[ip]->crd[1] >=
               ptr_child_nodes[ichild]->center_crd[1] -
                   ptr_child_nodes[ichild]->cell_half_width[1]) &
              (ptr_point_data_set->ptr_points_[ip]->crd[1] <
               ptr_child_nodes[ichild]->center_crd[1] +
                   ptr_child_nodes[ichild]->cell_half_width[1])) {
            std::swap(ptr_point_data_set->ptr_points_[index_last],
                      ptr_point_data_set->ptr_points_[ip]);
            index_last += 1;
          }
        }
        ptr_child_nodes[ichild]->point_id_first = index_first;
        ptr_child_nodes[ichild]->point_id_last = index_last;
        index_first = index_last;
        ptr_child_nodes[ichild]->ResolveNode(ptr_point_data_set,
                                             max_n_point_per_cell, max_depth,
                                             ptr_tree_depth, ptr_node_id);
      }

      //
      // 3D case
      //
    } else if (n_dim == 3) {
      ptr_child_nodes.resize(8);
      for (int ichild = 0; ichild < 8; ++ichild) {
        ptr_child_nodes[ichild] = new TreeNode<T>(n_dim);
        ptr_child_nodes[ichild]->node_depth = node_depth + 1;
        for (int idim = 0; idim < n_dim; ++idim) {
          ptr_child_nodes[ichild]->cell_half_width[idim] =
              0.5 * cell_half_width[idim];
        }
        ptr_child_nodes[ichild]->node_id = *ptr_node_id;
        *ptr_node_id += 1;
      }
      ptr_child_nodes[0]->center_crd[0] =
          center_crd[0] - 0.5 * cell_half_width[0];
      ptr_child_nodes[0]->center_crd[1] =
          center_crd[1] - 0.5 * cell_half_width[1];
      ptr_child_nodes[0]->center_crd[2] =
          center_crd[2] - 0.5 * cell_half_width[2];

      ptr_child_nodes[1]->center_crd[0] =
          center_crd[0] + 0.5 * cell_half_width[0];
      ptr_child_nodes[1]->center_crd[1] =
          center_crd[1] - 0.5 * cell_half_width[1];
      ptr_child_nodes[1]->center_crd[2] =
          center_crd[2] - 0.5 * cell_half_width[2];

      ptr_child_nodes[2]->center_crd[0] =
          center_crd[0] - 0.5 * cell_half_width[0];
      ptr_child_nodes[2]->center_crd[1] =
          center_crd[1] + 0.5 * cell_half_width[1];
      ptr_child_nodes[2]->center_crd[2] =
          center_crd[2] - 0.5 * cell_half_width[2];

      ptr_child_nodes[3]->center_crd[0] =
          center_crd[0] + 0.5 * cell_half_width[0];
      ptr_child_nodes[3]->center_crd[1] =
          center_crd[1] + 0.5 * cell_half_width[1];
      ptr_child_nodes[3]->center_crd[2] =
          center_crd[2] - 0.5 * cell_half_width[2];

      ptr_child_nodes[4]->center_crd[0] =
          center_crd[0] - 0.5 * cell_half_width[0];
      ptr_child_nodes[4]->center_crd[1] =
          center_crd[1] - 0.5 * cell_half_width[1];
      ptr_child_nodes[4]->center_crd[2] =
          center_crd[2] + 0.5 * cell_half_width[2];

      ptr_child_nodes[5]->center_crd[0] =
          center_crd[0] + 0.5 * cell_half_width[0];
      ptr_child_nodes[5]->center_crd[1] =
          center_crd[1] - 0.5 * cell_half_width[1];
      ptr_child_nodes[5]->center_crd[2] =
          center_crd[2] + 0.5 * cell_half_width[2];

      ptr_child_nodes[6]->center_crd[0] =
          center_crd[0] - 0.5 * cell_half_width[0];
      ptr_child_nodes[6]->center_crd[1] =
          center_crd[1] + 0.5 * cell_half_width[1];
      ptr_child_nodes[6]->center_crd[2] =
          center_crd[2] + 0.5 * cell_half_width[2];

      ptr_child_nodes[7]->center_crd[0] =
          center_crd[0] + 0.5 * cell_half_width[0];
      ptr_child_nodes[7]->center_crd[1] =
          center_crd[1] + 0.5 * cell_half_width[1];
      ptr_child_nodes[7]->center_crd[2] =
          center_crd[2] + 0.5 * cell_half_width[2];

      //
      // Now loop over the particles distribute them among the cells
      //
      index_first = point_id_first;
      index_last = point_id_first;

      for (int ichild = 0; ichild < 8; ++ichild) {
        for (int ip = index_first; ip < point_id_last; ++ip) {
          if ((ptr_point_data_set->ptr_points_[ip]->crd[0] >=
               (ptr_child_nodes[ichild]->center_crd[0] -
                ptr_child_nodes[ichild]->cell_half_width[0])) &
              (ptr_point_data_set->ptr_points_[ip]->crd[0] <
               (ptr_child_nodes[ichild]->center_crd[0] +
                ptr_child_nodes[ichild]->cell_half_width[0])) &
              (ptr_point_data_set->ptr_points_[ip]->crd[1] >=
               (ptr_child_nodes[ichild]->center_crd[1] -
                ptr_child_nodes[ichild]->cell_half_width[1])) &
              (ptr_point_data_set->ptr_points_[ip]->crd[1] <
               (ptr_child_nodes[ichild]->center_crd[1] +
                ptr_child_nodes[ichild]->cell_half_width[1])) &
              (ptr_point_data_set->ptr_points_[ip]->crd[2] >=
               (ptr_child_nodes[ichild]->center_crd[2] -
                ptr_child_nodes[ichild]->cell_half_width[2])) &
              (ptr_point_data_set->ptr_points_[ip]->crd[2] <
               (ptr_child_nodes[ichild]->center_crd[2] +
                ptr_child_nodes[ichild]->cell_half_width[2]))) {
            std::swap(ptr_point_data_set->ptr_points_[index_last],
                      ptr_point_data_set->ptr_points_[ip]);
            index_last += 1;
          }
        }
        ptr_child_nodes[ichild]->point_id_first = index_first;
        ptr_child_nodes[ichild]->point_id_last = index_last;
        index_first = index_last;
        ptr_child_nodes[ichild]->ResolveNode(ptr_point_data_set,
                                             max_n_point_per_cell, max_depth,
                                             ptr_tree_depth, ptr_node_id);
      }
    }
  }
}
// ----------------------------------------------------------------------
//
// ----------------------------------------------------------------------
template <class T>
Tree<T>::Tree(PointDataSet* ptr_point_data_set)
    : TreeNode<T>::TreeNode(ptr_point_data_set->get_ndim()) {
  int n_dim = ptr_point_data_set->get_ndim();

  crd_min_.resize(n_dim);
  crd_max_.resize(n_dim);
  for (int idim = 0; idim < n_dim; ++idim) {
    crd_min_[idim] = 0.0;
    crd_max_[idim] = 0.0;
  }
  max_n_point_per_cell_ = 0;
  max_tree_depth_ = 0;
  tree_depth_ = 0;
  this->ptr_point_data_set_ = ptr_point_data_set;
  n_leaf_ = 0;
  n_node_ = 0;
}
// ----------------------------------------------------------------------
//
// ----------------------------------------------------------------------
template <class T>
Tree<T>::~Tree() {}

// ----------------------------------------------------------------------
//
// ----------------------------------------------------------------------
template <class T>
Tree<T>::Tree() : TreeNode<T>::TreeNode(0) {
  crd_min_.resize(0);
  crd_max_.resize(0);
  max_n_point_per_cell_ = 0;
  max_tree_depth_ = 0;
  tree_depth_ = 0;
  this->ptr_point_data_set_ = nullptr;
  n_leaf_ = 0;
  n_node_ = 0;
}
// ----------------------------------------------------------------------
//
// ----------------------------------------------------------------------
template <class T>
void Tree<T>::Reset() {
  //
  // Reset the tree
  //
  if (!this->is_leaf) {
    for (size_t i=0; i < this->ptr_child_nodes.size(); ++i) {
      this->ptr_child_nodes[i]->DeleteChildNodes();
      delete this->ptr_child_nodes[i];
    }
    this->ptr_child_nodes.resize(0);
  }
  this->is_leaf = true;

  //
  // Reset the tree with 0 dimension
  //
  this->center_crd.resize(0);
  this->cell_half_width.resize(0);
  crd_min_.resize(0);
  crd_max_.resize(0);
  this->point_id_first = 0;
  this->point_id_last = 0;
  this->node_depth = 0;
  max_n_point_per_cell_ = 0;
  max_tree_depth_ = 0;
}
// ----------------------------------------------------------------------
//
// ----------------------------------------------------------------------
template <class T>
void Tree<T>::SetPointData(PointDataSet* ptr_point_data_set) {
  Reset();
  //
  // Reset the tree with the new dimensions
  //
  int n_dim = ptr_point_data_set->get_ndim();
  this->center_crd.resize(n_dim);
  this->cell_half_width.resize(n_dim);
  crd_min_.resize(n_dim);
  crd_max_.resize(n_dim);
  for (int idim = 0; idim < n_dim; ++idim) {
    this->center_crd[idim] = 0.0;
    this->cell_half_width[idim] = 0.0;
    crd_min_[idim] = 0.0;
    crd_max_[idim] = 0.0;
  }
  this->ptr_point_data_set_ = ptr_point_data_set;
}
// ----------------------------------------------------------------------
//
// ----------------------------------------------------------------------
template <class T>
PointDataSet* Tree<T>::GetPointData() {
  return this->ptr_point_data_set_;
}
// ----------------------------------------------------------------------
//
// ----------------------------------------------------------------------
template <class T>
void Tree<T>::BuildTree(const int& max_n_point_per_cell,
    const int& max_tree_depth) {

  this->max_n_point_per_cell_ = max_n_point_per_cell;
  this->max_tree_depth_ = max_tree_depth;
  ptr_point_data_set_->GetCoordinateBoundaries(crd_min_.data(),
                                               crd_max_.data());

  for (size_t idim = 0; idim < this->center_crd.size(); ++idim) {
    this->center_crd[idim] = 0.5 * (crd_min_[idim] + crd_max_[idim]);
    this->cell_half_width[idim] = 0.5001 * (crd_max_[idim] - crd_min_[idim]);

    if (this->cell_half_width[idim] == 0.0) {
      this->cell_half_width[idim] = 1.0;
    }
  }

  this->is_leaf = true;
  this->point_id_first = 0;
  this->point_id_last = ptr_point_data_set_->n_point_;
  this->node_depth = 0;
  this->node_id = 0;
  int node_id = 1;
  this->ResolveNode(ptr_point_data_set_, max_n_point_per_cell, max_tree_depth,
                    &tree_depth_, &node_id);
  this->CountLeafs(*this);
  this->CountNodes(*this);
}

// ----------------------------------------------------------------------
//
// ----------------------------------------------------------------------
template <class T>
void Tree<T>::BuildTree(const int& max_n_point_per_cell,
                        const int& max_tree_depth,
                        const std::vector<double>& root_crd_min,
                        const std::vector<double>& root_crd_max) {
  this->max_n_point_per_cell_ = max_n_point_per_cell;
  this->max_tree_depth_ = max_tree_depth;

  crd_min_ = root_crd_min;
  crd_max_ = root_crd_max;
  for (size_t idim = 0; idim < this->center_crd.size(); ++idim) {
    this->center_crd[idim] = 0.5 * (crd_min_[idim] + crd_max_[idim]);
    this->cell_half_width[idim] = 0.5 * (crd_max_[idim] - crd_min_[idim]);
    if (this->cell_half_width[idim] == 0.0) {
      this->cell_half_width[idim] = 1.0;
    }
  }
  this->is_leaf = true;
  this->point_id_first = 0;
  this->point_id_last = ptr_point_data_set_->n_point_;
  this->node_depth = 0;
  this->node_id = 0;
  int node_id = 1;
  this->ResolveNode(ptr_point_data_set_, max_n_point_per_cell, max_tree_depth,
                    &tree_depth_, &node_id);
  this->CountLeafs(*this);
  this->CountNodes(*this);
}

// ----------------------------------------------------------------------
//
// ----------------------------------------------------------------------
template<class T>
void Tree<T>::VerifyTree(TreeNode<T>* ptr_node) {
  if (!ptr_node->is_leaf) {
    for (size_t i = 0; i < ptr_node->ptr_child_nodes.size(); ++i) {
      VerifyTree(ptr_node->ptr_child_nodes[i]);
    }
  } else {
    int n_dim = static_cast<int>(ptr_node->center_crd.size());
    for (int idim = 0; idim < n_dim; ++idim) {
      for (int ip = ptr_node->point_id_first; ip < ptr_node->point_id_last;
           ++ip) {
        if ((ptr_point_data_set_->ptr_points_[ip]->crd[idim] <
             (ptr_node->center_crd[idim] - ptr_node->cell_half_width[idim])) |
            (ptr_point_data_set_->ptr_points_[ip]->crd[idim] >=
             (ptr_node->center_crd[idim] + ptr_node->cell_half_width[idim]))) {
          throw std::runtime_error(
              "Point in the tree outside of the node cell with ID : " +
              std::to_string(ip) + "\n" + " coordinate axis : " +
              std::to_string(idim) + "\n " + "coordinate : " +
              std::to_string(ptr_point_data_set_->ptr_points_[ip]->crd[idim]) +
              " cell boundaries : " +
              std::to_string(ptr_node->center_crd[idim] -
                             ptr_node->cell_half_width[idim]) +
              ", " +
              std::to_string(ptr_node->center_crd[idim] +
                             ptr_node->cell_half_width[idim]));
        }
      }
    }
  }
}

// ----------------------------------------------------------------------
//
// ----------------------------------------------------------------------
template <class T>
void Tree<T>::CalculateBBoxCoordinates(TreeNode<T>* ptr_node,
                                       const int& ind_h) {
  int n_dim = static_cast<int>(ptr_node->center_crd.size());
  ptr_node->bbox_crd_min.resize(n_dim, 1e99);
  ptr_node->bbox_crd_max.resize(n_dim, -1e99);

  for (int idim = 0; idim < n_dim; ++idim) {
    ptr_node->bbox_crd_min[idim] =
        ptr_node->center_crd[idim] - ptr_node->cell_half_width[idim];
    ptr_node->bbox_crd_max[idim] =
        ptr_node->center_crd[idim] + ptr_node->cell_half_width[idim];
    for (int ip = ptr_node->point_id_first; ip < ptr_node->point_id_last;
         ++ip) {
      if ((ptr_point_data_set_->ptr_points_[ip]->crd[idim] +
           ptr_point_data_set_->ptr_points_[ip]->scalars[ind_h]) >
          ptr_node->bbox_crd_max[idim]) {
        ptr_node->bbox_crd_max[idim] =
            ptr_point_data_set_->ptr_points_[ip]->crd[idim] +
            ptr_point_data_set_->ptr_points_[ip]->scalars[ind_h];
      }
      if ((ptr_point_data_set_->ptr_points_[ip]->crd[idim] -
           ptr_point_data_set_->ptr_points_[ip]->scalars[ind_h]) <
          ptr_node->bbox_crd_min[idim]) {
        ptr_node->bbox_crd_min[idim] =
            ptr_point_data_set_->ptr_points_[ip]->crd[idim] -
            ptr_point_data_set_->ptr_points_[ip]->scalars[ind_h];
      }
    }
  }
  if (!ptr_node->is_leaf) {
    for (size_t i = 0; i < ptr_node->ptr_child_nodes.size(); ++i) {
      this->CalculateBBoxCoordinates(ptr_node->ptr_child_nodes[i], ind_h);
    }
  }
}

// ----------------------------------------------------------------------
//
// ----------------------------------------------------------------------
template <class T>
std::vector<int> Tree<T>::GetLeafNodeIDs() {
  std::vector<int> leaf_node_id(n_leaf_, -1);
  int ind = 0;

  CollectLeafNodeIDs(this, &leaf_node_id, &ind);
  return leaf_node_id;
}
// ----------------------------------------------------------------------
//
// ----------------------------------------------------------------------
template <class T>
void Tree<T>::CollectLeafNodeIDs(TreeNode<T>* ptr_node,
                             std::vector<int>* ptr_cell_node_id, int* ind) {
  if (ptr_node->is_leaf) {
    (*ptr_cell_node_id)[*ind] = ptr_node->node_id;
    (*ind) += 1;
  } else {
    for (size_t i = 0; i < ptr_node->ptr_child_nodes.size(); ++i) {
      CollectLeafNodeIDs(ptr_node->ptr_child_nodes[i], ptr_cell_node_id, ind);
    }
  }
}

// ----------------------------------------------------------------------
//
// ----------------------------------------------------------------------
template <class T>
void Tree<T>::GetOverlappingParticleIndices(const std::vector<double>& crd,
                                            TreeNode<T>* ptr_node,
                                            std::vector<int>* ptr_pid) {
  //
  // Check if the bounding box of the node overlaps with the point
  //
  bool overlap = true;
  for (size_t idim=0; idim < ptr_node->center_crd.size(); ++idim) {
    overlap = (overlap & ((crd[idim] >= ptr_node->bbox_crd_min[idim]) &
                          (crd[idim] <= ptr_node->bbox_crd_max[idim])));
  }

  if (overlap) {
    if (ptr_node->is_leaf) {
      int old_size = static_cast<int>((*ptr_pid).size());
      int new_size =
          old_size + (ptr_node->point_id_last - ptr_node->point_id_first);
      (*ptr_pid).resize(new_size, -1);
      for (int ip = ptr_node->point_id_first; ip < ptr_node->point_id_last;
           ++ip) {
        (*ptr_pid)[old_size + (ip - ptr_node->point_id_first)] = ip;
      }
    } else {
      for (size_t i = 0; i < ptr_node->ptr_child_nodes.size(); ++i) {
        this->GetOverlappingParticleIndices(crd, ptr_node->ptr_child_nodes[i],
                                            ptr_pid);
      }
    }
  }
}
// ------------------------------------------------------------------------
//
// ------------------------------------------------------------------------
template <class T>
int Tree<T>::GetNLeaf() {
  return n_leaf_;
}
// ------------------------------------------------------------------------
//
// ------------------------------------------------------------------------
template <class T>
int Tree<T>::GetNNode() {
  return n_node_;
}
// ------------------------------------------------------------------------
//
// ------------------------------------------------------------------------
template <class T>
void Tree<T>::CountLeafs(const TreeNode<double>& node) {
  if (node.is_leaf) {
    n_leaf_ += 1;
  } else {
    for (size_t ichild = 0; ichild < node.ptr_child_nodes.size(); ++ichild) {
      this->CountLeafs(*node.ptr_child_nodes[ichild]);
    }
  }
}
// ------------------------------------------------------------------------
//
// ------------------------------------------------------------------------
template <class T>
void Tree<T>::CountNodes(const TreeNode<double>& node) {
  n_node_ += 1;
  if (!node.is_leaf) {
    for (size_t i = 0; i < node.ptr_child_nodes.size(); ++i) {
      this->CountNodes(*node.ptr_child_nodes[i]);
    }
  }
}
// ------------------------------------------------------------------------
//
// ------------------------------------------------------------------------
template <class T>
void Tree<T>::SetMaxNPointPerCell(const int& max_n_point_per_cell) {
  max_n_point_per_cell_ = max_n_point_per_cell;
}
// ------------------------------------------------------------------------
//
// ------------------------------------------------------------------------
template <class T>
int Tree<T>::GetMaxNPointPerCell() {
  return max_n_point_per_cell_;
}
// ------------------------------------------------------------------------
//
// ------------------------------------------------------------------------
template <class T>
void Tree<T>::SetMaxTreeDepth(const int& max_tree_depth) {
  max_tree_depth_ = max_tree_depth;
}
// ------------------------------------------------------------------------
//
// ------------------------------------------------------------------------
template <class T>
int Tree<T>::GetMaxTreeDepth() {
  return max_tree_depth_;
}
// ------------------------------------------------------------------------
//
// ------------------------------------------------------------------------
template <class T>
int Tree<T>::GetTreeDepth() {
  return tree_depth_;
}
// ------------------------------------------------------------------------
//
// ------------------------------------------------------------------------
template <class T>
std::vector<T> Tree<T>::GetRootCrdMin() {
  return crd_min_;
}
// ------------------------------------------------------------------------
//
// ------------------------------------------------------------------------
template <class T>
std::vector<T> Tree<T>::GetRootCrdMax() {
  return crd_max_;
}

#endif /* TREE_H_ */
