#
# This example is demonstrating how to use sphtool to do a basic visualisation of SPH
# simulations by plotting vertical slices of scalar and vector variables as well as 
# plotting scalar variables projected to an arbitrary plane (i.e. integrating along
# the normal of the plane)
#
#######################################################################################
from sphtool import SphTool
import numpy as np
import matplotlib.pyplot as plt
#
# This example is demonstrating how to re-grid an SPH simulation to a regular grid
# 
#
# -------------------------------------------------------------------------------------
# 
# -------------------------------------------------------------------------------------
def read_sph(fname=None):
    """
    Function to read the example SPH simulation
   
    Parameters
    ----------

        fname   - str
                  Name of the SPH snapshot file to be read
    Returns
    -------
        A dictionary with the following keys
            * npart - Number of particles
            * x     - Cartesian x coordinate of the particle
            * y     - Cartesian y coordinate of the particle
            * z     - Cartesian z coordinate of the particle
            * pmass - Particle mass
            * h     - Smoothing length
            * vx    - Cartesian x velocity compoent of the particle
            * vy    - Cartesian y velocity compoent of the particle
            * vz    - Cartesian z velocity compoent of the particle

        All of the dictionary keys contain a numpy ndarray with a length of npart
    """

    dum = np.genfromtxt(fname, skip_header=12)

    # Select the valid sph particles
    ii = dum[:,-1] == 1

    return {'npart': dum[ii,0].shape[0], 'crd':dum[ii,:3], 
            'pmass':dum[ii,3], 'h':dum[ii,4], 'rho':dum[ii,5], 'v':dum[ii,6:9]}

# -------------------------------------------------------------------------------------
# 
# -------------------------------------------------------------------------------------
print('Reading sph simulation..')
sph = read_sph(fname='snap_00000.ascii')
print('Done')

#
# Create an SphTool instance that uses maximum 4 parallel threads and contains as many
#  points as sph particles we have
#
spt = SphTool(n_thread=4, n_point=sph['npart'])
#
# Now set all coordinates and variables of each SPH particle. Note, that there are 
# four mandatory quantities to be set: crd, density, smoothing_length, particle_mass. 
#
spt.set_scalar("density", sph['rho'])
spt.set_scalar("smoothing_length", sph['h'])
spt.set_scalar("particle_mass", sph['pmass'])
spt.set_vector("crd", sph['crd'])
spt.set_vector("velocity", sph['v'])
#
# Now finalize the setup: the finalize method validates the sph data set, checks if
#  all mandatory variables have been set and builds the tree used for the interpolation
#
spt.finalize()

#
# Plot the volumne density slice in the xy plane
#
fig = plt.figure()
spt.plot_slice(var_name="density", incl=0., cmap=plt.cm.gist_heat, stretch='log', vmin=1e-8, image_size=50., cblabel="density")
plt.xlabel('x')
plt.ylabel('y')

#
# Plot the volumne density slice in the xy plane and overplot the velocity field
#
fig = plt.figure()
spt.plot_slice(var_name="density", incl=0., cmap=plt.cm.gist_heat, stretch='log', vmin=1e-8, image_size=50., cblabel="density")
spt.plot_slice(var_name="velocity", incl=0., posang=0., phi=0., image_size=50., qkeylabel="unit velocity")
plt.xlabel('x')
plt.ylabel('y')

#
# Plot the projected density to the xy plane, i.e. the z-integrated column density
#
fig = plt.figure()
spt.plot_projected_scalar(var_name="density", incl=0., posang=0., phi=0., image_size=50., cmap=plt.cm.gist_heat,
        stretch='log', vmin=1e-6, cblabel="surface density")
plt.xlabel('x')
plt.ylabel('y')


dum = input()

