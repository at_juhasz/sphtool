#
# This example is demonstrating how to re-grid an SPH simulation to a regular grid
# in cartesian coordinates
#
#######################################################################################


from sphtool import SphTool
import numpy as np
import matplotlib.pyplot as plt
#
# -------------------------------------------------------------------------------------
# 
# -------------------------------------------------------------------------------------
def read_sph(fname=None):
    """
    Function to read the example SPH simulation
   
    Parameters
    ----------

        fname   - str
                  Name of the SPH snapshot file to be read
    Returns
    -------
        A dictionary with the following keys
            * npart - Number of particles
            * x     - Cartesian x coordinate of the particle
            * y     - Cartesian y coordinate of the particle
            * z     - Cartesian z coordinate of the particle
            * pmass - Particle mass
            * h     - Smoothing length
            * vx    - Cartesian x velocity compoent of the particle
            * vy    - Cartesian y velocity compoent of the particle
            * vz    - Cartesian z velocity compoent of the particle

        All of the dictionary keys contain a numpy ndarray with a length of npart
    """

    dum = np.genfromtxt(fname, skip_header=12)

    # Select the valid sph particles
    ii = dum[:,-1] == 1

    return {'npart': dum[ii,0].shape[0], 'crd':dum[ii,:3], 
            'pmass':dum[ii,3], 'h':dum[ii,4], 'rho':dum[ii,5], 'v':dum[ii,6:9]}

# -------------------------------------------------------------------------------------
# 
# -------------------------------------------------------------------------------------
print('Reading sph simulation..')
sph = read_sph(fname='snap_00000.ascii')
print('Done')
#
# Create an SphTool instance that uses maximum 4 parallel threads and contains as many
#  points as sph particles we have
#
spt = SphTool(n_thread=4, n_point=sph['npart'])
#
# Now set all coordinates and variables of each SPH particle. Note, that there are 
# four mandatory quantities to be set: crd, density, smoothing_length, particle_mass. 
#
spt.set_scalar("density", sph['rho'])
spt.set_scalar("smoothing_length", sph['h'])
spt.set_scalar("particle_mass", sph['pmass'])
spt.set_vector("crd", sph['crd'])
spt.set_vector("velocity", sph['v'])
#
# Now finalize the setup: the finalize method validates the sph data set, checks if
#  all mandatory variables have been set and builds the tree used for the interpolation
#
spt.finalize()

#
# Set up the cartesian grid
#
#
# Number of cells in each direction
#
nx = 200
ny = 200
nz = 200
#
# Boundaries of the mesh
#
xmin = -25.
xmax = 25.
ymin = -25.
ymax = 25.
zmin = -25.
zmax = 25.
#
# Generate the cell interfaces
#
xi = np.linspace(xmin, xmax, nx+1)
yi = np.linspace(ymin, ymax, ny+1)
zi = np.linspace(zmin, zmax, nz+1)
#
# Now do the gridding
#
spt.regrid_to_regular_grid(["density", "velocity"], crd_system="cartesian", xi=xi, yi=yi, zi=zi)
#
# Write the grid to file
#
spt.write_grid("amr_grid.inp", format='radmc3d')
#
# Write the gridded density to file
#
spt.write_gridded_var(["density"], file_name="dust_density.inp", file_format="radmc3d", binary=False, scalar_floor=1e-90) 
#
# Write the gridded variable to a legacy vtk file format
#
spt.write_gridded_var(["density", "velocity"], file_name="model.vtk", file_format="vtk") 


