#
# This example is demonstrating how to re-grid an SPH simulation to an octree AMR grid
# in spherical coordinates. In this example first a basic octree grid is generated which
# is following the SPH particle distribution, then the grid will be refined along 
# denisty gradients
#
#######################################################################################
from sphtool import SphTool
import numpy as np
import matplotlib.pyplot as plt
#
# This example is demonstrating how to re-grid an SPH simulation to a regular grid
# 
#
# -------------------------------------------------------------------------------------
# 
# -------------------------------------------------------------------------------------
def read_sph(fname=None):
    """
    Function to read the example SPH simulation
   
    Parameters
    ----------

        fname   - str
                  Name of the SPH snapshot file to be read
    Returns
    -------
        A dictionary with the following keys
            * npart - Number of particles
            * x     - Cartesian x coordinate of the particle
            * y     - Cartesian y coordinate of the particle
            * z     - Cartesian z coordinate of the particle
            * pmass - Particle mass
            * h     - Smoothing length
            * vx    - Cartesian x velocity compoent of the particle
            * vy    - Cartesian y velocity compoent of the particle
            * vz    - Cartesian z velocity compoent of the particle

        All of the dictionary keys contain a numpy ndarray with a length of npart
    """

    dum = np.genfromtxt(fname, skip_header=12)

    # Select the valid sph particles
    ii = dum[:,-1] == 1

    return {'npart': dum[ii,0].shape[0], 'crd':dum[ii,:3], 
            'pmass':dum[ii,3], 'h':dum[ii,4], 'rho':dum[ii,5], 'v':dum[ii,6:9]}

# -------------------------------------------------------------------------------------
# 
# -------------------------------------------------------------------------------------
print('Reading sph simulation..')
sph = read_sph(fname='snap_00000.ascii')
print('Done')


#
# Create an SphTool instance that uses maximum 4 parallel threads and contains as many
#  points as sph particles we have
#
spt = SphTool(n_thread=4, n_point=sph['npart'])
#
# Now set all coordinates and variables of each SPH particle. Note, that there are 
# four mandatory quantities to be set: crd, density, smoothing_length, particle_mass. 
#
spt.set_scalar("density", sph['rho'])
spt.set_scalar("smoothing_length", sph['h'])
spt.set_scalar("particle_mass", sph['pmass'])
spt.set_vector("crd", sph['crd'])
spt.set_vector("velocity", sph['v'])
#
# Now finalize the setup: the finalize method validates the sph data set, checks if
#  all mandatory variables have been set and builds the tree used for the interpolation
#
spt.finalize()

#
# Set up the spatial grid
#

#
# Boundaries of the root cell
#
xmin = 0.5
xmax = 25.
ymin = 0.
ymax = np.pi
zmin = 0.0
zmax = 2.0*np.pi
#
# Maximum number of particles in an AMR grid cell (cell resolution criterion)
#
max_particle_per_amr_grid_cell = 300
#
# Maximum refinement level in the octree
#
max_amr_grid_depth = 4
#
# Switch on the mesh refinement based on the density gradient 
#
refine_gradient = True
#
# Maximum refinement level in the octree after density gradient refinement
#
max_refined_depth = 8
#
# Number of trial points to probe the density in the leaf cells
#
n_trial_points = 300
#
# Threshold value on the value of (rho_max - rho_min)/rho_max within the cells
# If this quantity is higher than a given threshold the cell will be refined. 
#
threshold = 0.95
#
# Floor value for the density
#
density_floor = 1e-9

#
# Now do the gridding
#
spt.regrid_to_amr_grid(["density", "velocity"], crd_system="spherical", xi=[xmin, xmax], 
                       yi=[ymin, ymax], zi=[zmin, zmax], 
                       max_particle_per_amr_grid_cell=max_particle_per_amr_grid_cell, 
                       max_amr_grid_depth=max_amr_grid_depth, refine_gradient=refine_gradient, 
                       max_refined_depth=max_refined_depth, n_trial_points=n_trial_points,
                       threshold=threshold, density_floor=density_floor)

#
# Write the grid to file
#
spt.write_grid("amr_grid.inp", format='radmc3d')
#
# Write the gridded density to file
#
spt.write_gridded_var(["density"], file_name="dust_density.inp", file_format="radmc3d", binary=False, scalar_floor=density_floor) 
#
# Write the gridded variable to a legacy vtk file format
#
spt.write_gridded_var(["density", "velocity"], file_name="model.vtk", file_format="vtk") 



