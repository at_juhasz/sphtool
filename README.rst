sphtool - Gridding and basic visualisation for SPH simulations
==============================================================

sphtool is a package for remapping SPH simulations to a 3D regular or an octree mesh. It is written in C++ with
a user interface in Python. This project started as an auxiliary package of radmc3dPy,
the Python interface for the 3D radiative transfer code `RADMC-3D <http://www.ita.uni-heidelberg.de/~dullemond/software/radmc-3d/>`_, 
to be able postprocess SPH simulations with RADMC-3D.


**Capabilities (as of v0.2)**

* regrid scalar and vector variables from SPH simulations to a regular or octree AMR mesh in cartesian or spherical coordinates
* Calculate/display 2D crossections of scalar or vector variables in SPH simulations
* Calculate/display 2D projetion of scalar variables (i.e. line-of-sight integrated scalars) in SPH simulations 
* Write the gridded scalar and vector variables to a legacy VTK format
* Currently sphtool can only process 3D SPH simulations and can only handle 3D spatial grids

Requirements
============
sphtool requires the following to be installed:

* gnu make
* gcc/g++ v5.0+
* swig 3.0.8+
* Python 2.7 or 3 
* numpy 1.13+
* matplotlib v1.5+

Documentation
=============

You can read more about sphtool `here <https://www.ast.cam.ac.uk/~juhasz/sphtoolDoc/index.html>`_ or 
in the doc folder in the sphtool directory. 

Contact
=======

Attila Juhasz `juhasz@ast.cam.ac.uk <mailto:juhasz@ast.cam.ac.uk>`_
